clingoLP="clingo clingoLP.py"
clingoLPParams=(-c nstrict=0 -c epsilon="(1,2)" -c accuracy=10)
clingoLPParams="${clingoLPParams[@]}"
minSupport="--heuristic Domain --enum-mode domRec "
clingoParams="--stats=2"

# ------------------------------- Carlson Without thermo---------------------------

echo "== Go to equilibrator to find Delta f G0'.  =="
python script/equilibrator_recup_data.py -i data/carlson/Correspondances.txt -o data/carlson/deltaGo_carlson.txt
echo "> written in deltaGo_carlson.txt"

echo "== Remove solutions ASPEFM Carlson Without Thermodynamics =="
$clingoLP $clingoLPParams $clingoParams $minSupport solveLP.lp4 -c debug=1 data/carlson/carlson_2004.lp4 -n 0 > outputs/Carlson/carlson_2004_without_Thermo.txt
echo "= Ecriture dans Output pour carlson without_Thermo="

# # parse output
echo "== parse EFMs of carlson into a csv file =="
python parse_output.py outputs/Carlson/carlson_2004_without_Thermo.txt --csv outputs/Carlson/carlson_2004_without_Thermo.csv 
echo "> written in carlson.csv"


# # ------------------------------- Carlson with thermo bornes using Sabine excel file FIXE, default = 10E-5 ---------------------------

echo "== Remove solutions using thermodynamics Carlson Test de Base =="
$clingoLP $clingoLPParams $clingoParams $minSupport solveLP.lp4 -c debug=1 extensions/thermo_dg_checker.py -c sbml=\"data/carlson/carlson_2004.xml\" -c concentrations=\"data/carlson/concentration_files/concentration_files_fixe/concentration_Carlson_time_modifie_ext_fixe.txt\" -c deltaG=\"data/carlson/deltaGo_carlson.txt\" data/carlson/carlson_2004.lp4 -n 0 > outputs/Carlson/carlson_2004_deltaG0_bornes_sabine_fixes.txt
echo "= Ecriture dans Output pour carlson DeltaGo="

# parse output
echo "== parse EFMs of carlson into a csv file =="
python parse_output.py outputs/Carlson/carlson_2004_deltaG0_bornes_sabine_fixes.txt --csv outputs/Carlson/carlson_2004_deltaG0_bornes_sabine_fixes.csv 
echo "> written in carlson.csv"

# # ------------------------------- Carlson with thermo bornes using Sabine excel file BOUCLE for pour les pourcentages ---------------------------

for i in 10 20 50 75 85 90 95 99
do
    echo "== Remove solutions using thermodynamics Carlson Test de Base =="
    $clingoLP $clingoLPParams $clingoParams $minSupport solveLP.lp4 -c debug=1 extensions/thermo_dg_checker.py -c sbml=\"data/carlson/carlson_2004.xml\" -c concentrations=\"data/carlson/concentration_files/concentration_files_variable_${i}/concentration_Carlson_time_modifie_ext_variable_${i}.txt\" -c deltaG=\"data/carlson/deltaGo_carlson.txt\" data/carlson/carlson_2004.lp4 -n 0 > outputs/Carlson/carlson_2004_deltaG0_bornes_sabine_variable_${i}.txt
    echo "= Ecriture dans Output pour carlson DeltaGo="

    # parse output
    echo "== parse EFMs of carlson into a csv file =="
    python parse_output.py outputs/Carlson/carlson_2004_deltaG0_bornes_sabine_variable_${i}.txt --csv outputs/Carlson/carlson_2004_deltaG0_bornes_sabine_variable_${i}.csv
    echo "> written in carlson.csv"
done


# # ------------------------------- Carlson with thermo bornes using Sabine excel file FIXE, avec par défaut 10-7 / 1 au lieu de 10-5  / 1 ---------------------------

echo "== Remove solutions using thermodynamics Carlson Test de Base =="
$clingoLP $clingoLPParams $clingoParams $minSupport solveLP.lp4 -c debug=1 extensions/thermo_dg_checker.py -c sbml=\"data/carlson/carlson_2004.xml\" -c concentrations=\"data/carlson/concentration_files/concentration_files_fixe/concentration_Carlson_time_modifie_ext_fixe_pourcentage_range_10E7_1_default.txt\" -c deltaG=\"data/carlson/deltaGo_carlson.txt\" data/carlson/carlson_2004.lp4 -n 0 > outputs/Carlson/carlson_2004_deltaG0_bornes_sabine_fixe_pourcentage_range_10E7_1_default.txt
echo "= Ecriture dans Output pour carlson DeltaGo="

# parse output
echo "== parse EFMs of carlson into a csv file =="
python parse_output.py outputs/Carlson/carlson_2004_deltaG0_bornes_sabine_fixe_pourcentage_range_10E7_1_default.txt --csv outputs/Carlson/carlson_2004_deltaG0_bornes_sabine_fixe_pourcentage_range_10E7_1_default.csv
echo "> written in carlson.csv"

# # ------------------------------- Carlson with thermo bornes using Sabine excel file BOUCLE for pour les pourcentages, avec par défaut 10-7 / 1 au lieu de 10-5  ---------------------------

for i in 10 20 50 75 85 90 95 99
do
    echo "== Remove solutions using thermodynamics Carlson Test de Base =="
    $clingoLP $clingoLPParams $clingoParams $minSupport solveLP.lp4 -c debug=1 extensions/thermo_dg_checker.py -c sbml=\"data/carlson/carlson_2004.xml\" -c concentrations=\"data/carlson/concentration_files/concentration_files_variable_${i}/concentration_Carlson_time_modifie_ext_variable_${i}_pourcentage_range_10E7_1_default.txt\" -c deltaG=\"data/carlson/deltaGo_carlson.txt\" data/carlson/carlson_2004.lp4 -n 0 > outputs/Carlson/carlson_2004_deltaG0_bornes_sabine_variable_${i}_pourcentage_range_10E7_1_default.txt
    echo "= Ecriture dans Output pour carlson DeltaGo="

    # parse output
    echo "== parse EFMs of carlson into a csv file =="
    python parse_output.py outputs/Carlson/carlson_2004_deltaG0_bornes_sabine_variable_${i}_pourcentage_range_10E7_1_default.txt --csv outputs/Carlson/carlson_2004_deltaG0_bornes_sabine_variable_${i}_pourcentage_range_10E7_1_default.csv
    echo "> written in carlson.csv"
done
