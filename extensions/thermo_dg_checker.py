#const sbml=nofile.
#const concentrations=nofile.
#const deltaG=nofile.

#script (python)

### Ligne de Commande type ppour lancer l'analyse thermodynamique ###
#clingo [...] thermo_dg_checker.py -c debug=1 -c sbml=data.sbml -c concentrations=data.cc -c deltaG=data.dG

#                                                                                      I. Library Imports
from types import SimpleNamespace
from extensions.clingoLP_extension import clingoLPExtension
import extensions.parsehelper as ph
from extensions.support_propagator import SupportPropagator

# Manipulation of SBML files
import cobra
from cobra.io import read_sbml_model
import xml.etree.ElementTree as ET

import random
import sys
import getopt
import csv
import re

#Construction of the CPLEX model
import docplex.mp.model as mpM
from docplex.mp.model import Model


import numpy as np
import time, warnings

# Function to remove flux("...") from reaction names (for the literals)
defluxer = lambda x: x[len('flux("'):-len('")')]

#                                                                                      II. ThermoDGChecker Class

class ThermoDGChecker():
    
    def __init__(self, sbml, concentrations, deltaG): # MODIFY
        """
        ThermoDGChecker class constructor

        Params:
            sbml (str): input file name
            concentrations (str): input file name
            deltaG (str): input file name
        """
        print("Début de la thermo")
        
        # Oppening files :

        with open(sbml, 'r') as f: # SBML
            self.model   = read_sbml_model(f)
            
        with open(concentrations, 'r') as f: # Concentrations Min et Max
            self.concentration_min_max = f.readlines()
    
        with open(deltaG, 'r') as f: # DeltaG
            self.deltaG0 = f.readlines()
    
        # Manipulation of data from file :
        def remove_react_default_cobra_deep_copy(reaction_id):
            ''' Par défaut cobra rajoute des réactions commencant par EX_ qui sont les réactions de transport de métabolite, souvent l'utilisateur a déjà ces réactions dans son modèle, donc on les enlève les nom des réactions commencant par EX.            input : 
                reaction_id (str) : id de la réaction
            output : 
                reaction_id (str) : id de la réaction sans les réaction commencait par EX_
            '''
            list_reaction_id = reaction_id.copy()
            for react in reaction_id:
                if react[0:3] == "EX_":
                    list_reaction_id.remove(react)
            return list_reaction_id

        # SBML
        self.nb_metabolites_total = len(self.model.metabolites) 
        self.nb_reactions_total   = len(self.model.reactions)
        
        self.id_metabolites       = self.model.metabolites.list_attr("id") # ['13dpg_c', '2pg_c', '3pg_c', '6pgc_c', ...] ATTENTION : M_ MANQUANT !!!!!!!!!
        self.id_reactions_potentielles = self.model.reactions.list_attr("id")   # ['ACALD', 'ACALDt', 'ACKr', 'ACONTa', ...]
        self.id_reactions = remove_react_default_cobra_deep_copy(self.id_reactions_potentielles)
        self.matrix_stoichiometry = cobra.util.array.create_stoichiometric_matrix(self.model, array_type = 'DataFrame') # panda matrix. To find the stochiometric number : stoichiometry.loc["atp_c", "ACALD"]

        # Model constants
        self.R = 8.314; #Perfect gas constant
        self.T = 298.15; # Temperature in Kelvin
        self.Co = 1; # Standard concentration 1M
        self.Cj_min = 1e-7; # Minimum concentration 1e-7M 
        self.Cj_max = 1; # Maximum concentration 1M
        self.lnCj_Co_min = np.log(self.Cj_min/self.Co)
        self.lnCj_Co_max = np.log(self.Cj_max/self.Co)
        
#                                                                                      II. ThermoDGChecker Class

        # CONCENTRATIONS
        
        # -- Specificity of deltaG0 input file --
        # The deltaG0 input file is a text file with the following columns:
        # id_metabolite;id_kegg;concentration_min;concentration_max
        # Attention : il faut absolument avoir un métabolite de biomasse dans le fichier de concentrationssi on veut negliger les contraintes de thermo sur les réactions utilisant la biomasse
        # Example:
        # BIOMASS;C00000;0.0001;0.01
        # atp_c;C00002;0.0001;0.0002
        # adp_c;C00008;0.0001;0.0002
        # ----------------------------------------
        
        # Dictionary building functions
        
        def get_dico_by_id_concentration_min_max(concentration_min_max, type_id):
            """Get concentrations min et max in a dict 
            Input : 
            concentration_min_max (str)   id_metabolite;id_kegg;concentration_min;concentration_max
            type_id (str) : 'id_metabolite' or 'id_kegg', par defaut 'id_metabolite'
            Output : dico_by_id_concentration_min_max (dict) {'type_id' : [concentration_min, concentration_max], ...}
            Attention on enlève si besoin le M_ devant les id_metabolites
            """
            dico_by_id_concentration_min_max = {}

            for line in concentration_min_max:
                line = line.rstrip('\n')
                line = line.split(";")
                if type_id == 'id_kegg':
                    id_metabolite = line[1]
                else:
                    id_metabolite = line[0]
                concentration_min = line[2]
                concentration_max = line[3]
                
                # Remove M_ in front of metabolite_id if metabolites begin with M_.
                if id_metabolite[0:2] == "M_":
                    id_metabolite = id_metabolite[2:]
                dico_by_id_concentration_min_max[id_metabolite] = [float(concentration_min),float(concentration_max)]
                
            return dico_by_id_concentration_min_max
        
        def correspondance_kegg_id_metabolite(concentration_min_max):                       
            """
            Get correspondance Kegg_id_metabolite in a dict 
            Input : 
            concentration_min_max (str)   id_metabolite;id_kegg;concentration_min;concentration_max
            Output : dico_by_kegg_to_id (dict) {'id_kegg' : 'id_metabolite', ...} 
                    dico_by_id_to_kegg (dict) {'id_metabolite' : 'id_kegg', ...} 
            Attention on enlève si besoin le M_ devant les id_metabolites
            """
            dico_by_kegg_to_id = {}
            dico_by_id_to_kegg = {}
            for couple in concentration_min_max:
                couple = couple.split(";")
                id_metabolite = couple[0]
                
                # Remove M_ in front of metabolite_id if metabolites begin with M_.
                if id_metabolite[0:2] == "M_":
                    id_metabolite = id_metabolite[2:]
                id_kegg = couple[1]
                dico_by_kegg_to_id[id_kegg] = id_metabolite
                dico_by_id_to_kegg[id_metabolite] = id_kegg
            return dico_by_kegg_to_id, dico_by_id_to_kegg
        
        # => Creation of the dictionnaries
        self.dico_by_id_concentration_min_max = get_dico_by_id_concentration_min_max(self.concentration_min_max, 'id_metabolite')    
        #print("dico_by_id_concentration_min_max : ", self.dico_by_id_concentration_min_max)
        #print("")
        
        self.dico_by_kegg_to_id, self.dico_by_id_to_kegg = correspondance_kegg_id_metabolite(self.concentration_min_max)
        #print("dico_by_kegg_to_id : ", self.dico_by_kegg_to_id)
        #print("dico_by_id_to_kegg : ", self.dico_by_id_to_kegg)
        
    
        # Calcul des valeurs de lnCj_Co
        
        def get_dico_by_id_lnCj_Co(dico_by_id_concentration_min_max, Co):
            """
            Get lnCj_Co in a dict 
            Input : 
            dico_by_id_concentration_min_max (dict) {'type_id' : [concentration_min, concentration_max], ...}
            Co (float) : standard concentration
            Output : dico_by_id_lnCj_Co (dict) {'type_id' : [lnCj_Co_min, lnCj_Co_max], ...}
            """
            dico_by_id_lnCj_Co = {}
            for id_metabolite in dico_by_id_concentration_min_max:
                lnCj_Co_min = np.log(dico_by_id_concentration_min_max[id_metabolite][0]/Co)
                lnCj_Co_max = np.log(dico_by_id_concentration_min_max[id_metabolite][1]/Co)
                dico_by_id_lnCj_Co[id_metabolite] = [lnCj_Co_min, lnCj_Co_max]
            return dico_by_id_lnCj_Co
        
        self.dico_by_id_lnCj_Co = get_dico_by_id_lnCj_Co(self.dico_by_id_concentration_min_max, self.Co)  # A VERIFIER LES VALEURS
        #print("dico_by_id_lnCj_Co : ", self.dico_by_id_lnCj_Co)
        
        # DELTAGO
        
        # -- Spécificités du fichier d'entrée de deltaG0---.
        # Le fichier d'entrée de deltaG0 est un fichier texte avec les colonnes suivantes:
        # id_metabolite;id_kegg;deltaG0
        # ------------------------------------------------
        
        # DELTAG0 dictionary building functions
        
        def get_dico_by_id_deltafG0(deltaG0, type_id): 
            """
            Recupération des bonnes valeurs de deltaG0 dans un dictionnaire
            Input : deltaG0 (str) : id_metabolite;id_kegg;deltaG0
                    type_id (str) : 'id_metabolite' or 'id_kegg', par defaut 'id_metabolite'
            Output : dico_deltafG0 (dict) {'metaboliteName' : dfG_sp1, ...}
            Attention on enlève si besoin le M_ devant les id_metabolites
            """
            dico_by_id_deltafG0 = {}
            for line in deltaG0:
                line = line.split(";")
                try:
                    if type_id == 'id_kegg':
                        id_metabolite = line[1]
                    else: # on veut par id_metabolite
                        id_metabolite = line[0]
                except KeyError:
                    print("Erreur dans la récupération des valeurs de deltaG0")
                    if self.debug:
                        warnings.warn("Erreur dans la récupération des valeurs de deltaG0")
                
                # Il faut enlever M_ devant les id_metabolites si les métabolites commencent par M_
                if id_metabolite[0:2] == "M_":
                    id_metabolite = id_metabolite[2:]
                    #print("line[2]", line[2].rstrip('\n'))
                    #print("float(line[2].rstrip('\n'))", float(line[2].rstrip('\n')))
                dico_by_id_deltafG0[id_metabolite] = float(line[2].rstrip('\n'))
            return dico_by_id_deltafG0

        self.dico_by_id_deltafG0=get_dico_by_id_deltafG0(self.deltaG0, 'id_metabolite')

        #print("dico_by_id_deltafG0 : ", self.dico_by_id_deltafG0)

        
        def get_dico_by_id_deltafgprime(dico_by_id_deltafG0, dico_by_id_lnCj_Co, R, T):
            '''calculer en amont les valeurs de deltaGprime pour chaque métabolite en fonction des concentrations
            input :
            dico_by_id_deltafG0 (dict) : {'metaboliteName' : dfG_sp1, ...}
            dico_by_id_lnCj_Co (dict) : {'type_id' : [lnCj_Co_min, lnCj_Co_max], ...}
            R (float) : perfect gas constant
            T (float) : Temperature in Kelvin
            output :
            dico_by_id_deltafgprime (dict) : {'type_id' : [deltaGprime_min, deltaGprime_max], ...}
            '''
            dico_by_id_deltafgprime = {}
            for id_metabolite in dico_by_id_deltafG0:
                deltafgprime_min = dico_by_id_deltafG0[id_metabolite] + R * T * dico_by_id_lnCj_Co[id_metabolite][0]
                deltafgprime_max = dico_by_id_deltafG0[id_metabolite] + R * T * dico_by_id_lnCj_Co[id_metabolite][1]
                dico_by_id_deltafgprime[id_metabolite] = [deltafgprime_min, deltafgprime_max]
            return dico_by_id_deltafgprime
        
        self.dico_by_id_deltafgprime = get_dico_by_id_deltafgprime(self.dico_by_id_deltafG0, self.dico_by_id_lnCj_Co, self.R, self.T)
        #print("dico_by_id_deltafgprime : ", self.dico_by_id_deltafgprime)
        
        try :
            self.name_BIOMASS = self.dico_by_kegg_to_id["C00000"]
        except KeyError:
            self.name_BIOMASS = ""
            if self.debug:
                warnings.warn("Attention : pas de métabolite de biomasse dans le fichier de concentration")
        
        # Initialiation of inmpotant variables, for the next steps :
        
        self.literals = {} # dict of literals, filled in init_action  
        

                
        # Statistics of checker # DON'T TOUCH
        self.added_nogoods = 0 # incremented in extension
        self.checked_solutions = 0 # incremented in extension
        self.cumultime2 = 0 # to be incremented in respects_constraints
                      
        
    def init_action(self, init): # DON'T TOUCH
        """
        Init action from clingoLPExtension/listOfExtensions.init_action

        Params:
            init: PropagatorInit from clingo/clingoLP
        """
        #print("init_action")
        # Propagator is needed for getting literals
        tmp_prop = SupportPropagator()
        tmp_prop.init(init)
        self.literals = tmp_prop.literals()
        assert(self.literals)
        tmp_prop.dump_literals()   
        
        """ -------------- RESTES DE MAXIME (à supprimer si tout fonctionnne bien, et à expliquer dans le readme)------------------
        appelé quand clingoLP.init
        self.literals depend de chaque execution de clingolp, on peut recuperer au moment clingoLP.init
        support EFM = reactions actives
        thermo Checker me dit : support EFM pas bien
        donc on ne veut plus jamais support EFM
        
        ex: R1, R3, on veut contrainte [R1, R3] (ensemble) non autorisé
        ---> nogood
        
        {15: 'R1', 12: 'R3'}
        {'R1': 15, 'R3': 12}
        R1 avec R3 pas autorisé = nogood
        [literal[R1],  literal[R3]]
        15, 12
        # add nogood prend liste en argument
        self.add_nogood([15, 12])
        self.add_nogood([self.literals[r] for r in support])
        """
        
    def construct_constraints(self, partial_support): # MODIFY
        """
        Construct constraints
        
        ######## EXPLAINS THE PROCESS ########
        # To apply the thermodynamic constraints, we'll check whether, for a given reaction, we have all the constants required for thermodynamic calculations, and whether it's not a transport reaction.
        # > If we have all the constants, we can apply the thermodynamic constraint. 
        # > Otherwise, the thermodynamic constraint cannot be applied.
        # IMPORTANT: metabolites must be in the same compartment as the reaction in order to apply the thermo constraint (> allows you to deal with the case where you have a transport reaction, in which case you don't need to apply the thermodynamic constraints).
        # ####################################
        
        Params:
            partial_support (dict): reaction name: 1 if active, else 0 (get_support_dict)
        """
        # A. Creation of the model :
        
        mp_model=Model(name='reduction_EFM_thermo')
        #print("Model créé")
        
        # B. Variable definition : (These defined variables are the ones that the solver must adjust to find a solution)
        # ln(Cj/Co) is the concentration of metabolite j for a given efm i, bearing in mind that we don't just take the variable cj because otherwise one of the constraints is non-linear (the boxed inequality).
        # We therefore have a dictionary of lnCj_Co for each metabolite (with the metabolite id as key) and the variable lnCj_Co as value.
        
        #lnCj_Co = mp_model.continuous_var_dict(self.id_metabolites, lb=self.Cj_min, ub=self.Cj_max) 
        # IDEE Amélioration : mettre directemement la contrainte sur les ln dans les bornes possible des métabolites

        ##### Changements on va calculer les valeurs avant de définir la variable #####
        #
        
        ######
        
        lowerbound=[]
        uperbound=[]

        #print("new dico_by_id_deltafgprime : ", self.dico_by_id_deltafgprime)

        for j in self.id_metabolites:
            #print("j : ", j)
            try:
                #print("je suis dans le try")
                
                
                
                lowerbound.append(self.dico_by_id_deltafgprime[j][0])
                uperbound.append(self.dico_by_id_deltafgprime[j][1])
            except KeyError:
                # on applique la borne par défaut
                lowerbound.append(self.lnCj_Co_min)
                uperbound.append(self.lnCj_Co_max)
                
                print("Attention : les valeurs de deltaG0 pour le métabolite "+j+" ne sont pas présentes")
        
        

                
                 
        deltafgprime = mp_model.continuous_var_dict(self.id_metabolites, lb=lowerbound, ub=uperbound) ## IDEE Amélioration : mettre directemement la liste des Cmax et Cmin
        #print("Variables deltafgprime créées")
        
        # C. Objective function: (No need for an objective function, we just want to find a solution that satisfies the constraints)
            
        mp_model.minimize(0)

        # D. Constraints definition :
        
        
        # pour toutes les réaction du partial_support avec 1, on ajoute la contrainte de thermodynamique
        #for name_react in partial_support:
        for name_react in partial_support.keys():
            
            if [cle for cle in partial_support.keys()][0][0:2] == "R_": # Des fois il y a une différence entre les fichiers (Tefma)
                name_reaction = "R_"+name_react
            else:
                name_reaction = name_react
            
     
                       
            if partial_support[name_reaction] == 1: # If the reaction participates in the EFM (if the reaction is active) => Constraint 1 is added
                # Before adding the constraint, we need to check the following:
                # > Are all the metabolites in the reaction in the same compartment (it's not a transport reaction)?
                # > Do we have all the constants needed for thermodynamic calculations?
                
                # First check: Are all the metabolites in the reaction in the same compartment (it's not a transport reaction)?

                
                # si la réaction finie par _rev on enlève le _rev
                
                if name_react[-4:] == "_rev":
                    fake_name_react = name_react[:-4]
                    signe_stochio = -1 # il faut aussi inverser les signes des stochiometries des métabolites
                else:
                    fake_name_react = name_react
                    signe_stochio = 1
                
                metabolite_react = self.model.reactions.get_by_id(fake_name_react).metabolites #  Recovery of reaction metabolites
                # print("metabolite_react : ", metabolite_react)
                #print("ok")
                metabolite_react_id = [metabolite.id for metabolite in metabolite_react] #  Recovery of reaction metabolites

                #if not 'R70' in name_react: # si la réaction n'est pas la biomasse {a modifier}
                if not self.name_BIOMASS in metabolite_react_id: # si la réaction n'est pas la biomasse {a modifier}
                    
                    # Check that metabolites are in the same compartment :
                    compartiment = self.model.reactions.get_by_id(fake_name_react).compartments
                    #print("compartiment : ", compartiment)
                    if len(compartiment) == 1: 
                        #print("All reaction metabolites are in the same compartment")
                        
                        # Second condition: [Potentially SIMPLIFY this part by directly using a try on the constraint setting in the model]. ]
                        # Do we have all the constants required for thermodynamic calculations (i.e. do we have deltaG0, Cmin, Cmax for each metabolite)?
                        try:
                            #print("on arrive au try")
                            for metabolite in metabolite_react: # /!\ NE PAS SUPPRIMER LES PRINTS, ILS SONT LA POUR LE TRY
                                self.dico_by_id_deltafgprime[metabolite.id]
                                
                        except KeyError:

                            print("Toutes les constantes pour faire les calculs de thermodynamique ne sont pas présentes")
                        else:
                            
                            # We have all the information => We add constraint n°1 to the mp_model
                            
                            
                            #### MODIFIEEEE ####
                            # Constraint 1: Reaction feasibility (For reactions in EFM only)                               
                            
                            mp_model.add_constraint( sum((signe_stochio*self.matrix_stoichiometry.loc[j.id, fake_name_react] * (deltafgprime[j.id]))  for j in metabolite_react)<= 0, ctname="reaction_feasibility")


        self.sol=mp_model.solve() 

        # test idéal: if unbounded false, verifier dans doc cplex 'unbounded_or_infeasible' => que none

        if self.sol is not None: # importance: faible / à vérifier, self.sol not None but unbounded ?
            return True
            
        else: 
            return False
    

    def call_model_cplex(self, partial_support): # MODIFY
        """
        Calls CPLEX model to determine if efm is thermodynamically feasible or not

        Params:
            efm (dict): reaction name: 1 if active, else 0 (get_support_dict)
            
        Returns: 
            True if feasible else False
        """   


        return self.construct_constraints(partial_support)
                    

    def respects_thermodynamics(self, state):
        """
        Gets EFM from propagator state
        Calls CPLEX model to determine if efm is thermodynamically feasible or not
        
        Params:
            state (ClingoLP.State): propagator state, to get fluxes and active reactions
            
        Returns: 
            True if feasible else False
        """     
        fluxes = self.get_support_dict(state.current_assignment)

        deb2 = time.time()
        support = [k for k, v in fluxes.items() if v == 1]
        if not support:
            return True
        #print("Fluxes :",fluxes)

        bool_respected = self.call_model_cplex(fluxes)
        
        end2 = time.time()
        self.cumultime2 += (end2 - deb2)
        #print("temps d'execution CLPEX ", self.cumultime2)

        return bool_respected

    """ Utilitary functions to get fluxes or support of EFMs """
    
    def get_support_dict(self, current_assignment):
        '''
        Get dict of the support
        Exemple: {'R1': 1, 'R2': 0, 'R3': 1} 
        '''
        return {defluxer(k): 1 if v != 0 else 0 for k, v in current_assignment[1].items()}

    def get_support_list(self, current_assignment):
        '''
        Get list of the support
        Exemple: ['R1', 'R3']
        '''
        return [defluxer(k) for k, v in current_assignment[1].items() if v != 0]

    def get_fluxes(self, current_assignment):
        '''
        Get dict of the fluxes, with the real value of the fluxes
        Exemple: {'R1': 1.556, 'R2': 0, 'R3': 0.556}
        '''
        return {defluxer(k): v for k, v in current_assignment[1].items()}
    
    """ Utilitary functions to get literals from reaction names using propagator literals """

    def get_literals(self, support): # return positive literals for support active reactions
        '''
        Get list of literals from support
        Exemple: [15, 12]
        '''
        return [self.literals[s] for s in support] # self.literals filled in extension

    def get_active_list(self, support): # active reactions, not null in efm
        '''
        Get list of active reactions
        Exemple: [15, 12]
        '''
        active = [self.literals[r] for r in support]
        return active # list on which we want a nogood
    
    def get_inactive_list(self, support): # inactive reactions, reactions not in support
        '''
        Get list of inactive reactions
        Exemple: [14, 16] (all reactions not in support)
        '''
        inactive = [self.literals[r] for r in self.reacs if r not in support]
        return inactive # list on which we want a clause
    
    @property
    def reacs(self):
        return list(self.literals.keys())
    
    """ 
        Clause: R1 or R2 or R3
        Littéraux: R1, R2, R3 (Ri, ... Rn)
        Opérateur: or
        
        Nogood: (not R1) or (not R2) or (not R3)
        Equivalent: not (R1 and R2 and R3)
        (via De Morgan); on ne veut ni R1, ni R2, ni R3
        Opérator: or
    """
    
    """ Print statistics of Checker"""
    def print_stats(self):
        #print("Je suis passé par la thermo")
        print("Added nogoods for " + str(self.added_nogoods) + " out of " + str(self.checked_solutions) + " partial solutions checked")
        print("Total time used by ThermoDGChecker: " + str(round(self.cumultime, 5)) + " s")
        print("Total time used by CPLEX: " + str(round(self.cumultime2, 5)) + " s")

#                                                                                      III. ThermoDGCheckerExtension
# Extension for the list of extensions
class ThermoDGCheckerExtension(ThermoDGChecker, clingoLPExtension):

    def __init__(self, sbml, concentrations, deltaG, debug_value):
        ThermoDGChecker.__init__(self, sbml, concentrations, deltaG)
        self.debug = (debug_value > 0) 
        print(" I nit de la thermo  ")
        
        #self.debug = True
        self.cumultime = 0 # to be incremented in respects_constraints

    def init_action(self, init): 
        ThermoDGChecker.init_action(self, init)

    def check_consistency_action(self, control, state):
        """
        Check consistency action from clingoLPExtension/listOfExtensions.check_consistency_action
        Is called when consistency is checked after a literal is propagated

        Params:
            control: PropagatorControl is a structure defined in Clingo API that can add nogoods
            state: ClingoLP.State is a structure defined in ClingoLP to store results of propagation and of Cplex
            
        """
        deb = time.time()
        # print("Check Consistency") # (after literal evaluation in execution tree)
        # test si 99% des littéraux sont affectés à vrai ou faux - solution partielles où 90% support à tester est connu
        if state.current_lit_percentage >= 99: # 99 %
            self.checked_solutions += 1
            #warnings.warn("Essais de solution")
            if not self.respects_thermodynamics(state):
                #print("AJOUT POTENTIEL DE NOGOOD")
                support = self.get_support_list(state.current_assignment)
                if self.debug: 
                    print('lp support', support)
                active_literals = self.get_active_list(support)
                if self.debug: 
                    print('lits for clause', active_literals)
                inactive_literals = self.get_inactive_list(support)
                
                # DO NOT TOUCH
                if not control.add_nogood(active_literals, lock=True) or not control.propagate():
                    self.added_nogoods += 1
                    if self.debug:
                        warnings.warn("Ajout de Nogoods")
                    self.cumultime += (time.time() - deb)
                    return False
                elif not control.add_clause(inactive_literals, lock=True) or not control.propagate():
                    if self.debug:
                        warnings.warn("Adding clause of inactive literals instead of nogoods")
                        print("Clause ajouté au lieu de nogood ajouté")
                    self.added_nogoods += 1
                    self.cumultime += (time.time() - deb)
                    return False
                if self.debug:
                    warnings.warn("Potentially abnormal behaviour: unable to add nogoods")
                self.cumultime += (time.time() - deb)
                return False
            self.cumultime += (time.time() - deb)
            return True
        end = time.time()
        self.cumultime += (end - deb)
        print("temps d'execution : ", self.cumultime)

    def after_prg_solve_action(self):
        """
        After prg solve action from clingoLPExtension/listOfExtensions.after_prg_solve_action
        Is called after ClingoLP.prg solve is finished
        """
        self.print_stats()
        

    @classmethod
    def from_prg(cls, prg):
        """
        Class method: is called like this: ThermoDGChecker.from_prg(prg)
        
        Params:
            prg: Program arguments, including constants (e.g. paramfile, debug, sbml, ...)
        
        Returns: 
            cls: Class instance using __init__ constructor
        """   
        print("Je passe ICI")     
        debug_value = int(prg.get_const('debug').number)
        sbml = ph.smart_remove_quotes(str(prg.get_const('sbml'))) 
        if sbml == 'nofile':
            raise ValueError('ThermoDG Checker invoked without SBML parameter')
        concentrations = ph.smart_remove_quotes(str(prg.get_const('concentrations')))
        if concentrations == 'nofile':
            raise ValueError('ThermoDG Checker invoked without Concentrations parameter')
        deltaG = ph.smart_remove_quotes(str(prg.get_const('deltaG')))
        if deltaG == 'nofile':
            raise ValueError('ThermoDG Checker invoked without DeltaG parameter')
        return cls(sbml, concentrations, deltaG, debug_value)


#end.
