# Explanation of extensions

This readme will explain in precise detail the extensions present in *aspefm*.

## DeltaGChecker

The DeltaGChecker extension allows thermodynamic constraints to be added when searching for EFMs with *aspefm*.

For more details on constraints, please refer to the [CMSB article](../CMSB_article/articleCMSB_2024.pdf)


The extension is organized as follows: 
for each potential EFM support received, the linear program is created with CPLEX. In this model, we define the variable that the solver must optimize, in this case: $\ln\left({C_{j}}/{C_{0}}\right)$. When defining this variable, we impose the first constraint, the framing constraint, on the concentration values of the metabolites. 
The structure of the extension is as follows: during the EFM computation the linear program (\ref{lp}) is created using CPLEX for each potential support of EFM. %Within this model, the optimization variable defined for the solver to maximize is $\ln\left({C_{j}}/{C_{0}}\right)$. During the definition of this variable, the initial constraint applied is the framing constraint, which governs the concentration values of the metabolites.
The logarithmic ratio of the concentration of metabolite $ln\left({C_{j}}/{C_{0}}\right)$ is bounded between a minimum concentration $ln\left(C_{j}^{\text{min}}/{C_{0}}\right)$ and a maximum concentration $\ln\left({C_{j}^{\text{max}}}/{C_{0}}\right)$. The values of $C_{j}^{\text{min}}$ and $C_{j}^{\text{max}}$ are retrieved from the user-supplied data in the concentration file. If the data is not available, a default value is assigned, with $C_{j}^{\text{min}}$ and $C_{j}^{\text{max}}$ respectively set to $10^{-7} M$ and $1 M$. 

This model is assigned a second constraint, relative to $\Delta_r G_{i}$. For each reaction in the network belonging to the potential support, the second constraint is applied. Note also that this constraint will be effectively applied if and only if all thermodynamic information is available for that given reaction. Thermodynamic constraints are not applied to molecule transport reactions and biomass reactions (hence the need to annotate the input SBML file).

Once the two constraints have been added, the model is given to the CPLEX solver. The objective is to find out whether a set of concentration solutions exists, which respects the thermodynamic constraints. The object is not to find an optimal solution, but rather to know whether a solution exists.

Since the thermodynamic constraint is support-monotone, the extension is used for a partial support which reduce the solution space during the computation.
A Boolean is returned depending on whether the CPLEX solver finds a solution to the system. But adding constraints requires biological knowledge of the minimum/maximum concentration data sets and $\Delta rG$ for the metabolites studied, which is not always the case.

