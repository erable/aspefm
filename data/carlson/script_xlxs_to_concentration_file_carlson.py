# Ce script permet de convertir un fichier Excel en fichier de concentration
# ce script permet de lire de xlxs que pour chaque temps initial, il y a une colonne de concentration

import pandas as pd
import numpy as np
import os

# fichier excel :
# 	       0  	       24	       48	        72	       96
# ACCOA	1,40E-07	1,40E-07	1,40E-07	1,40E-07	1,40E-07
# ADP	6,70E-07	6,70E-07	6,70E-07	6,70E-07	6,70E-07
# AKG	1,00E-07	4E-07	1,00E-07	1,00E-07	1,00E-07
# AMP	3,09E-07	3,09E-07	3,09E-07	3,09E-07	3,09E-07

# fichier correspodance concentration :

# M_ACCOA;C00024
# M_ADP;COOO4



# 1fichier de sortie pour chaque temps initial (mettre le temps dans le nom de fichier)
#concentration_mario_time_0.txt   :
# M_ACCOA;COOO2;1,40E-07
# M_ADP;COOO4;6,70E-07
# M_AKG;COOO6;1,00E-07

#concentration_mario_time_24.txt   :
# M_ACCOA;COOO2;1,40E-07
# M_ADP;COOO4;6,70E-07
# M_AKG;COOO6;4E-07


#################################################### A CHOISIR ####################################################

# ------------------------------------------------ Choix du fichier Excel ------------------------------------------------
#df = pd.read_excel('concentration_files/initial_cond13.xls')
df = pd.read_excel('concentrationMetab_ecoli.ods')
# ----------------------------------------------------------------------------------------------------------------------------

# ------------------------------------------------ Choix de la concentration par défaut ------------------------------------------------
#concentration_par_defaut=10E-5
concentration_par_defaut="default" # on ne met pas de concentration par défaut
# ----------------------------------------------------------------------------------------------------------------------------

# ------------------------------------------------ Choix de la concentration voulue ------------------------------------------------
concentration_file = []
concentration_file.append("fixe")
concentration_file.append("variable_10")
concentration_file.append("variable_20")
concentration_file.append("variable_50")
concentration_file.append("variable_75")
concentration_file.append("variable_85")
concentration_file.append("variable_90")
concentration_file.append("variable_95")
concentration_file.append("variable_99")

# ----------------------------------------------------------------------------------------------------------------------------

# ------------------------------------------------ Choix du mode ------------------------------------------------
#mode = "pourcentage_with_10E5_default"
mode = "pourcentage_range_10E7_1_default"
# ----------------------------------------------------------------------------------------------------------------------------

#################################################### A CHOISIR ####################################################


# lecture du fichier de correspondance concentration
dico = {}
# On ajoute tous les métabolites du fichier correspondance dans le dictionnaire avec une concentration de Base 10E-7

with open('correspondances_carlson.txt', 'r') as f:
    for line in f:
        line = line.split(';')
        # il faut enlever le retour à la ligne et les espaces en trop
        dico[line[0]] = [line[1].replace('\n', '')]

print(dico)
print("--------------------")

        
# Ajout de la biomasse dans le dictionnaire 

# Remplissage du dictionnaire de correspondance avec les concentrations pour chaque temps initial ( attention on a un M_ en moins dans le fichier excel devant tous les noms de métabolites)
# Avant :
# dico= {'M_ATP':[COOO4], 'M_ADP':[COOO2], 'M_AKG':[COOO6], 'M_AMP':[COOO8]}
# Après
# dico = {'M_ATP':[COOO4, 1.40E-07], 'M_ADP':[COOO2, 1.40E-07], 'M_AKG':[COOO6, 1.40E-07], 'M_AMP':[COOO8, 1.40E-07]}
# on ajoute à la liste de chaque métabolite la concentration pour chaque temps initial


# On va tester si dans le fichier ods on retrouve une partie du nom du métabolite du fichier texte
# Si c'est le cas on ajoute la concentration du fichier texte dans le dictionnaire
# Sinon on ajoute le métabolite dans le dictionnaire avec une concentration de 10E-7

liste_non_trouve=[]

for nom_metabolite_correspondance in dico.keys() : # pour chaque métabolite du fichier de correspondance on va chercher si on le retrouve dans le fichier excel
    trouve = False
    for i in range(0, len(df)):
        nom_metabolite_correspondance_modifie = nom_metabolite_correspondance.replace('M_', '')
        nom_metabolite_correspondance_modifie = nom_metabolite_correspondance_modifie.replace('_ext', '')
        nom_metabolite_correspondance_modifie = nom_metabolite_correspondance_modifie.replace('_main', '')
        nom_metabolite_excel = df.iloc[i, 0]
        if nom_metabolite_correspondance_modifie in nom_metabolite_excel:# on a retrouvé le métabolite dans le fichier excel
            dico[nom_metabolite_correspondance].append(df.iloc[i, 3]) # on prend la colonne M pour avoir les concentrations en moles
            print('Ajout de la concentration du métabolite '+nom_metabolite_correspondance+' pour un métabolite correspondant dans le excel, nommé : '+nom_metabolite_excel)
            trouve = True
            break
    if not trouve:
        print('## Métabolite non trouvé dans le fichier de correspondance : '+nom_metabolite_correspondance)
        dico[nom_metabolite_correspondance].append(concentration_par_defaut)
        print('Ajout du métabolite dans le fichier de correspondance : '+nom_metabolite_correspondance+" avec une concentration par défaut")
        liste_non_trouve.append(nom_metabolite_correspondance)
print("--------------------")
print (' dico après remplissage :',dico)
print("liste des métabolites non trouvés : ",liste_non_trouve)
        
            
    
########################################################  SORTIES  ########################################################

# création d'un dossier pour les fichiers de sortie
if not os.path.exists('concentration_files'):
    os.makedirs('concentration_files')



    

# ________________ Mode pourcentage_range_10E7_1_default ______________________
if mode=="pourcentage_range_10E7_1_default":
    for concentratation_test in concentration_file:
        
        # Faire un nouveau dossier dans concentration_files pour concentration_file
        if not os.path.exists('concentration_files/concentration_files_'+concentratation_test):
            os.makedirs('concentration_files/concentration_files_'+concentratation_test)
            
        if concentratation_test == "fixe":
            with open('concentration_files/concentration_files_'+concentratation_test+'/concentration_Carlson_time_modifie_ext_fixe_pourcentage_range_10E7_1_default.txt', 'w') as f:
                for key in dico.keys():
                    if dico[key][1] == "default":
                        f.write(key+';'+dico[key][0]+';'+str(10E-7)+';'+str(1)+'\n')
                    else:
                        f.write(key+';'+dico[key][0]+';'+str(dico[key][1])+';'+str(dico[key][1])+'\n')
                        
        if concentratation_test == "variable_10":
            with open('concentration_files/concentration_files_'+concentratation_test+'/concentration_Carlson_time_modifie_ext_variable_10_pourcentage_range_10E7_1_default.txt', 'w') as f:
                for key in dico.keys():
                    if dico[key][1] == "default":
                        f.write(key+';'+dico[key][0]+';'+str(10E-7)+';'+str(1)+'\n')
                    else:
                        f.write(key+';'+dico[key][0]+';'+str(0.9*dico[key][1])+';'+str(dico[key][1]*1.1)+'\n')
                        
        if concentratation_test == "variable_20":
            with open('concentration_files/concentration_files_'+concentratation_test+'/concentration_Carlson_time_modifie_ext_variable_20_pourcentage_range_10E7_1_default.txt', 'w') as f:
                for key in dico.keys():
                    if dico[key][1] == "default":
                        f.write(key+';'+dico[key][0]+';'+str(10E-7)+';'+str(1)+'\n')
                    else:
                        f.write(key+';'+dico[key][0]+';'+str(0.8*dico[key][1])+';'+str(dico[key][1]*1.2)+'\n')
        
        if concentratation_test == "variable_50":
            with open('concentration_files/concentration_files_'+concentratation_test+'/concentration_Carlson_time_modifie_ext_variable_50_pourcentage_range_10E7_1_default.txt', 'w') as f:
                for key in dico.keys():
                    if dico[key][1] == "default":
                        f.write(key+';'+dico[key][0]+';'+str(10E-7)+';'+str(1)+'\n')
                    else:
                        f.write(key+';'+dico[key][0]+';'+str(0.5*dico[key][1])+';'+str(dico[key][1]*1.5)+'\n')
        
        if concentratation_test == "variable_75":
            with open('concentration_files/concentration_files_'+concentratation_test+'/concentration_Carlson_time_modifie_ext_variable_75_pourcentage_range_10E7_1_default.txt', 'w') as f:
                for key in dico.keys():
                    if dico[key][1] == "default":
                        f.write(key+';'+dico[key][0]+';'+str(10E-7)+';'+str(1)+'\n')
                    else:
                        f.write(key+';'+dico[key][0]+';'+str(0.25*dico[key][1])+';'+str(dico[key][1]*1.75)+'\n')
                        
        if concentratation_test == "variable_85":
            with open('concentration_files/concentration_files_'+concentratation_test+'/concentration_Carlson_time_modifie_ext_variable_85_pourcentage_range_10E7_1_default.txt', 'w') as f:
                for key in dico.keys():
                    if dico[key][1] == "default":
                        f.write(key+';'+dico[key][0]+';'+str(10E-7)+';'+str(1)+'\n')
                    else:
                        f.write(key+';'+dico[key][0]+';'+str(0.15*dico[key][1])+';'+str(dico[key][1]*1.85)+'\n')
        
        if concentratation_test == "variable_90":
            with open('concentration_files/concentration_files_'+concentratation_test+'/concentration_Carlson_time_modifie_ext_variable_90_pourcentage_range_10E7_1_default.txt', 'w') as f:
                for key in dico.keys():
                    if dico[key][1] == "default":
                        f.write(key+';'+dico[key][0]+';'+str(10E-7)+';'+str(1)+'\n')
                    else:
                        f.write(key+';'+dico[key][0]+';'+str(0.1*dico[key][1])+';'+str(dico[key][1]*1.9)+'\n')
                        
        if concentratation_test == "variable_95":
            with open('concentration_files/concentration_files_'+concentratation_test+'/concentration_Carlson_time_modifie_ext_variable_95_pourcentage_range_10E7_1_default.txt', 'w') as f:
                for key in dico.keys():
                    if dico[key][1] == "default":
                        f.write(key+';'+dico[key][0]+';'+str(10E-7)+';'+str(1)+'\n')
                    else:
                        f.write(key+';'+dico[key][0]+';'+str(0.05*dico[key][1])+';'+str(dico[key][1]*1.95)+'\n')
        
        if concentratation_test == "variable_99":
            with open('concentration_files/concentration_files_'+concentratation_test+'/concentration_Carlson_time_modifie_ext_variable_99_pourcentage_range_10E7_1_default.txt', 'w') as f:
                for key in dico.keys():
                    if dico[key][1] == "default":
                        f.write(key+';'+dico[key][0]+';'+str(10E-7)+';'+str(1)+'\n')
                    else:
                        f.write(key+';'+dico[key][0]+';'+str(0.01*dico[key][1])+';'+str(dico[key][1]*1.99)+'\n')

        



# ________________ Autre mode ______________________

if mode=="pourcentage_with_10E5_default":
    for concentratation_test in concentration_file:
        
        # Faire un nouveau dossier dans concentration_files pour concentration_file
        if not os.path.exists('concentration_files/concentration_files_'+concentratation_test):
            os.makedirs('concentration_files/concentration_files_'+concentratation_test)
            
        if concentratation_test == "fixe":
            with open('concentration_files/concentration_files_'+concentratation_test+'/concentration_Carlson_time_modifie_ext_fixe.txt', 'w') as f:
                for key in dico.keys():
                    f.write(key+';'+dico[key][0]+';'+str(dico[key][1])+';'+str(dico[key][1])+'\n')

        if concentratation_test == "variable_10":
            with open('concentration_files/concentration_files_'+concentratation_test+'/concentration_Carlson_time_modifie_ext_variable_10.txt', 'w') as f:
                for key in dico.keys():
                    f.write(key+';'+dico[key][0]+';'+str(0.9*dico[key][1])+';'+str(dico[key][1]*1.1)+'\n')

        if concentratation_test == "variable_20":
            with open('concentration_files/concentration_files_'+concentratation_test+'/concentration_Carlson_time_modifie_ext_variable_20.txt', 'w') as f:
                for key in dico.keys():
                    f.write(key+';'+dico[key][0]+';'+str(0.8*dico[key][1])+';'+str(dico[key][1]*1.2)+'\n')
                    
        if concentratation_test == "variable_50":
            with open('concentration_files/concentration_files_'+concentratation_test+'/concentration_Carlson_time_modifie_ext_variable_50.txt', 'w') as f:
                for key in dico.keys():
                    f.write(key+';'+dico[key][0]+';'+str(0.5*dico[key][1])+';'+str(dico[key][1]*1.5)+'\n')

        if concentratation_test == "variable_75":
            with open('concentration_files/concentration_files_'+concentratation_test+'/concentration_Carlson_time_modifie_ext_variable_75.txt', 'w') as f:
                for key in dico.keys():
                    f.write(key+';'+dico[key][0]+';'+str(0.25*dico[key][1])+';'+str(dico[key][1]*1.75)+'\n')
                    
        if concentratation_test == "variable_85":
            with open('concentration_files/concentration_files_'+concentratation_test+'/concentration_Carlson_time_modifie_ext_variable_85.txt', 'w') as f:
                for key in dico.keys():
                    f.write(key+';'+dico[key][0]+';'+str(0.15*dico[key][1])+';'+str(dico[key][1]*1.85)+'\n')
                    
        if concentratation_test == "variable_90":
            with open('concentration_files/concentration_files_'+concentratation_test+'/concentration_Carlson_time_modifie_ext_variable_90.txt', 'w') as f:
                for key in dico.keys():
                    f.write(key+';'+dico[key][0]+';'+str(0.1*dico[key][1])+';'+str(dico[key][1]*1.9)+'\n')

        if concentratation_test == "variable_95":
            with open('concentration_files/concentration_files_'+concentratation_test+'/concentration_Carlson_time_modifie_ext_variable_95.txt', 'w') as f:
                for key in dico.keys():
                    f.write(key+';'+dico[key][0]+';'+str(0.05*dico[key][1])+';'+str(dico[key][1]*1.95)+'\n')

        if concentratation_test == "variable_99":
            with open('concentration_files/concentration_files_'+concentratation_test+'/concentration_Carlson_time_modifie_ext_variable_99.txt', 'w') as f:
                for key in dico.keys():
                    f.write(key+';'+dico[key][0]+';'+str(0.01*dico[key][1])+';'+str(dico[key][1]*1.99)+'\n')
                    
                    
        if concentratation_test == "variable_200":
            with open('concentration_files/concentration_files_'+concentratation_test+'/concentration_Carlson_time_modifie_ext_variable_200.txt', 'w') as f:
                for key in dico.keys():
                    f.write(key+';'+dico[key][0]+';'+str(0.1*dico[key][1])+';'+str(dico[key][1]*2)+'\n')
            