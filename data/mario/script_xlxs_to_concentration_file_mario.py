# Ce script permet de convertir un fichier Excel en fichier de concentration
# ce script permet de lire de xlxs que pour chaque temps initial, il y a une colonne de concentration

import pandas as pd
import numpy as np
import os

# fichier excel :
# 	       0  	       24	       48	        72	       96
# ACCOA	1,40E-07	1,40E-07	1,40E-07	1,40E-07	1,40E-07
# ADP	6,70E-07	6,70E-07	6,70E-07	6,70E-07	6,70E-07
# AKG	1,00E-07	4E-07	1,00E-07	1,00E-07	1,00E-07
# AMP	3,09E-07	3,09E-07	3,09E-07	3,09E-07	3,09E-07

# fichier correspodance concentration :

# M_ACCOA;C00024
# M_ADP;COOO4



# 1fichier de sortie pour chaque temps initial (mettre le temps dans le nom de fichier)
#concentration_mario_time_0.txt   :
# M_ACCOA;COOO2;1,40E-07
# M_ADP;COOO4;6,70E-07
# M_AKG;COOO6;1,00E-07

#concentration_mario_time_24.txt   :
# M_ACCOA;COOO2;1,40E-07
# M_ADP;COOO4;6,70E-07
# M_AKG;COOO6;4E-07


# lecture du fichier xls :

# ------------------------------------------------ Choix du fichier Excel ------------------------------------------------
#df = pd.read_excel('concentration_files/initial_cond13.xls')
df = pd.read_excel('concentration_files/initial_cond13_modifie.xls')
# ----------------------------------------------------------------------------------------------------------------------------


# lecture du fichier de correspondance concentration
temps = ['0', '24', '48', '72', '96']
dicot0 = {'temps':'0'}
dicot24 = {'temps':'24'}
dicot48 = {'temps':'48'}
dicot72 = {'temps':'72'}
dicot96 = {'temps':'96'}
with open('Correspondances_mario.txt', 'r') as f:
    for line in f:
        line = line.split(';')
        # il faut enlever le retour à la ligne et les espaces en trop
        dicot0[line[0]] = [line[1].replace('\n', '')]
        dicot24[line[0]] = [line[1].replace('\n', '')]
        dicot48[line[0]] = [line[1].replace('\n', '')]
        dicot72[line[0]] = [line[1].replace('\n', '')]
        dicot96[line[0]] = [line[1].replace('\n', '')]

        
# Ajout de la biomasse dans le dictionnaire 

# Remplissage du dictionnaire de correspondance avec les concentrations pour chaque temps initial ( attention on a un M_ en moins dans le fichier excel devant tous les noms de métabolites)
# Avant :
# dicot0 = {'M_ATP':[COOO4], 'M_ADP':[COOO2], 'M_AKG':[COOO6], 'M_AMP':[COOO8]}
# Après
# dicot0 = {'M_ATP':[COOO4, 1.40E-07], 'M_ADP':[COOO2, 1.40E-07], 'M_AKG':[COOO6, 1.40E-07], 'M_AMP':[COOO8, 1.40E-07]}
# on ajoute à la liste de chaque métabolite la concentration pour chaque temps initial

for i in range(0, len(df)):
    try:
        dicot0['M_'+df.iloc[i, 0]].append(df.iloc[i, 1])
        dicot24['M_'+df.iloc[i, 0]].append(df.iloc[i, 2])
        dicot48['M_'+df.iloc[i, 0]].append(df.iloc[i, 3])
        dicot72['M_'+df.iloc[i, 0]].append(df.iloc[i, 4])
        dicot96['M_'+df.iloc[i, 0]].append(df.iloc[i, 5])
    except:
        print('Métabolite non trouvé dans le fichier de correspondance : '+df.iloc[i, 0])
        dicot0['M_'+df.iloc[i, 0]] = ['C00000',df.iloc[i, 1]]
        dicot24['M_'+df.iloc[i, 0]] = ['C00000',df.iloc[i, 1]]
        dicot48['M_'+df.iloc[i, 0]] = ['C00000',df.iloc[i, 1]]
        dicot72['M_'+df.iloc[i, 0]] = ['C00000',df.iloc[i, 1]]
        dicot96['M_'+df.iloc[i, 0]] = ['C00000',df.iloc[i, 1]]
        print('Ajout du métabolite dans le fichier de correspondance : '+df.iloc[i, 0]+" avec le numéro Kegg C00000")
    
print (dicot0)

L_dico_temps = [dicot0, dicot24, dicot48, dicot72, dicot96]

# Ecriture des fichiers de sortie
#concentration_mario_time_0.txt   :
# M_ACCOA;COOO2;1,40E-07
# M_ADP;COOO4;6,70E-07
# M_AKG;COOO6;1,00E-07

# création d'un dossier pour les fichiers de sortie
if not os.path.exists('concentration_files'):
    os.makedirs('concentration_files')

# ------------------------------------------------ Choix de la concentration ------------------------------------------------
concentration_file = []
concentration_file.append("variable_0")
concentration_file.append("variable_10")
concentration_file.append("variable_15")
concentration_file.append("variable_20")
concentration_file.append("variable_50")
concentration_file.append("variable_75")
concentration_file.append("variable_80")
concentration_file.append("variable_85")
concentration_file.append("variable_90")
concentration_file.append("variable_99")

# ----------------------------------------------------------------------------------------------------------------------------

# Faire un nouveau dossier dans concentration_files pour concentration_file


for concentration_test in concentration_file:
    if not os.path.exists('concentration_files/concentration_files_'+concentration_test):
        os.makedirs('concentration_files/concentration_files_'+concentration_test)
    # Pour une concentration fixée
    if concentration_test == "variable_0":
        for dicoTemp in L_dico_temps:
            with open('concentration_files/concentration_files_'+concentration_test+'/concentration_mario_time_'+dicoTemp['temps']+'_modifie_ext_0_pourcents.txt', 'w') as f:
                for key in dicoTemp.keys():
                    if not key == 'temps':
                        f.write(key+';'+dicoTemp[key][0]+';'+str(dicoTemp[key][1])+';'+str(dicoTemp[key][1])+'\n')

    if concentration_test == "variable_10":
        for dicoTemp in L_dico_temps:
            with open('concentration_files/concentration_files_'+concentration_test+'/concentration_mario_time_'+dicoTemp['temps']+'_modifie_ext_10_pourcents.txt', 'w') as f:
                for key in dicoTemp.keys():
                    if not key == 'temps':
                        f.write(key+';'+dicoTemp[key][0]+';'+str(0.9*dicoTemp[key][1])+';'+str(1.10*dicoTemp[key][1])+'\n')
    
    if concentration_test == "variable_15":
        for dicoTemp in L_dico_temps:
            with open('concentration_files/concentration_files_'+concentration_test+'/concentration_mario_time_'+dicoTemp['temps']+'_modifie_ext_15_pourcents.txt', 'w') as f:
                for key in dicoTemp.keys():
                    if not key == 'temps':
                        f.write(key+';'+dicoTemp[key][0]+';'+str(0.85*dicoTemp[key][1])+';'+str(1.15*dicoTemp[key][1])+'\n')
                        
    if concentration_test == "variable_20":
        for dicoTemp in L_dico_temps:
            with open('concentration_files/concentration_files_'+concentration_test+'/concentration_mario_time_'+dicoTemp['temps']+'_modifie_ext_20_pourcents.txt', 'w') as f:
                for key in dicoTemp.keys():
                    if not key == 'temps':
                        f.write(key+';'+dicoTemp[key][0]+';'+str(0.8*dicoTemp[key][1])+';'+str(1.20*dicoTemp[key][1])+'\n')
    
    if concentration_test == "variable_50":
        for dicoTemp in L_dico_temps:
            with open('concentration_files/concentration_files_'+concentration_test+'/concentration_mario_time_'+dicoTemp['temps']+'_modifie_ext_50_pourcents.txt', 'w') as f:
                for key in dicoTemp.keys():
                    if not key == 'temps':
                        f.write(key+';'+dicoTemp[key][0]+';'+str(0.5*dicoTemp[key][1])+';'+str(1.50*dicoTemp[key][1])+'\n')
    
    if concentration_test == "variable_75":
        for dicoTemp in L_dico_temps:
            with open('concentration_files/concentration_files_'+concentration_test+'/concentration_mario_time_'+dicoTemp['temps']+'_modifie_ext_75_pourcents.txt', 'w') as f:
                for key in dicoTemp.keys():
                    if not key == 'temps':
                        f.write(key+';'+dicoTemp[key][0]+';'+str(0.25*dicoTemp[key][1])+';'+str(1.75*dicoTemp[key][1])+'\n')
    
    if concentration_test == "variable_80":
        for dicoTemp in L_dico_temps:
            with open('concentration_files/concentration_files_'+concentration_test+'/concentration_mario_time_'+dicoTemp['temps']+'_modifie_ext_80_pourcents.txt', 'w') as f:
                for key in dicoTemp.keys():
                    if not key == 'temps':
                        f.write(key+';'+dicoTemp[key][0]+';'+str(0.2*dicoTemp[key][1])+';'+str(1.80*dicoTemp[key][1])+'\n')
    
    if concentration_test == "variable_85":
        for dicoTemp in L_dico_temps:
            with open('concentration_files/concentration_files_'+concentration_test+'/concentration_mario_time_'+dicoTemp['temps']+'_modifie_ext_85_pourcents.txt', 'w') as f:
                for key in dicoTemp.keys():
                    if not key == 'temps':
                        f.write(key+';'+dicoTemp[key][0]+';'+str(0.15*dicoTemp[key][1])+';'+str(1.85*dicoTemp[key][1])+'\n')
    
    if concentration_test == "variable_90":
        for dicoTemp in L_dico_temps:
            with open('concentration_files/concentration_files_'+concentration_test+'/concentration_mario_time_'+dicoTemp['temps']+'_modifie_ext_90_pourcents.txt', 'w') as f:
                for key in dicoTemp.keys():
                    if not key == 'temps':
                        f.write(key+';'+dicoTemp[key][0]+';'+str(0.1*dicoTemp[key][1])+';'+str(1.90*dicoTemp[key][1])+'\n')
    
    if concentration_test == "variable_99":
        for dicoTemp in L_dico_temps:
            with open('concentration_files/concentration_files_'+concentration_test+'/concentration_mario_time_'+dicoTemp['temps']+'_modifie_ext_99_pourcents.txt', 'w') as f:
                for key in dicoTemp.keys():
                    if not key == 'temps':
                        f.write(key+';'+dicoTemp[key][0]+';'+str(0.01*dicoTemp[key][1])+';'+str(1.99*dicoTemp[key][1])+'\n')
                        
    