# Example models and Data Inputs




## Exemple pour l’extension *DeltaGChecker*


*DeltaGChecker* extension is the *aspefm* extension that enables the incorporation of thermodynamic constraints. As input to *aspefm*, for performing EFMs computations, it is required to specify the metabolic model, the desired extension, *DeltaGChecker*, and to provide additional data files containing the necessary information (a file for concentration bounds and a file for $\Delta_f G’^{\circ{}}$ in text format).

### Data inputs : Data file mandatory

The DeltaGChecker extension needs 3 additional files to work compared to using *aspefm* alone.

- **Network file (SBML)**: The file containing information on the metabolic network must be entered in Systems Biology Markup Language (SBML) format. The SBML file will be used to form the stoichiometric matrix. This stoichiometric matrix is required when calculating the $\Delta rG_{i}$, when we need the stoichiometric coefficient for the $j^{th}$ metabolite and the $i{th}$ reaction. It is essential that the SBML file is correctly completed with the correct variable names and comparisons for each metabolite. Please note that metabolite identifiers must be identical in all files, whether SBML, L4P or text data files. Please note that reversible reactions must be duplicated, i.e. for a reversible reaction R1, two reactions must be entered, one labelled R1 and the other R1\_reverse.
This file can be created using the Mparser module, which allows conversion of networks between various formats, command line interface. Please use the module *[mparser](https://github.com/maxm4/mparser)* for creation of metabolic networks for usage with *aspefm*.
The compartments of each metabolite must be clearly labelled. The information on the location of the metabolite is used to determine whether or not it is a transport reaction. No thermo constraint is applied to transport reactions because they are not chemical reactions.

An example of an annotated SBML file: [SBML from Carlson's modele](carlson/carlson_2004.xml)

- **Concentration boundary file (TXT):** Thefile containing information on the concentration limits for each metabolite ID. If a metabolite identifier is not entered, the default concentration limits will be applied, i.e. 10E-7 for the minimum limit and 1 for the maximum limit. Concentrations must be given in Molar (g/l).
The text file will be organized in lines, as follows:

    `identifier_metabolite;id_kegg;concentration_minimale;concentration_maximale`

An example of an annotated concentration file: [Concentration data file from Carlson's modele](carlson/concentration_files/concentration_files_variable_10/concentration_Carlson_time_modifie_ext_variable_10.txt)

- **File $\Delta fG’^{\circ{}}$ (TXT):** the second text file should contain thermodynamic information, i.e. the $\Delta fG’^{\circ{}}$ of each metabolite, in kJ per mole. The text file should be organized as follows:
    `identifiant_metabolite;id_kegg;valeur_deltafG`

An example of an annotated delta file: [Delta data file from Carlson's modele](carlson/deltaGo_carlson.txt)

### Get Data for extension

The use of APIs is highly recommended in cases where dozens of values need to be retrieved from a database.

#### Concentration data

For the application analyses, we already had spreadsheets of metabolite concentration values for the species studied from previous studies. A Python script was developed to create the text file readable by the extension ([script traduction table data to file input for carson modele](carlson/script_xlxs_to_concentration_file_carlson.py) and [script traduction table data to file input for Mario modele](mario/script_xlxs_to_concentration_file_mario.py) ).


The extension requests **a concentration file with the metabolite identifier in table form**, its Kegg number and the minimum and maximum concentration limits.
The Kegg identifier is not used directly for concentrations, but for associating the $\Delta fG’^{\circ{}}_{j} $ with the correct metabolite. It should be noted that it is perfectly possible to set the concentration of a metabolite to a value by assigning the minimum and maximum variable identical values.
Furthermore, when defining the value concentration interval, a concentration pair must be defined for each metabolite identifier ($C_{j}^{\text{min}}$ , $C_{j}^{\text{max}}$), i.e. the metabolites in each compartment must be differentiated (Example: M\_ACETATE and M\_ACETATE\_EXT) and assigned a value pair for each.

>**Warning**: it is absolutely necessary to have a biomass metabolite marked C00000 in the concentration file if you want to neglect the thermo constraints on reactions using biomass.

#### Thermodynamics data

 
To retrieve the thermodynamic data, we developed a script using the well-documented Equilibrator API [Script get data eQuilibrator](../script/equilibrator_recup_data.py).

In particular, we used the following standard conditions recommended by Equilibrator: a pH of 7.4, an ionic strength of 0.25M and a temperature of 298.15K .  %Retrieving data via the API requires a Kegg identifier file. Using the Kegg identifier for the search provides more reliable results than simply using the metabolite name.

In the API, don’t forget to add the search for $\Delta fG$ in biological constraints. Biological constraints are considered by Equilibrator to have a pH of 7.4, an ionic strength of 0.25M and a temperature of 298.15K. Equilibrator’s API allows us to retrieve a value of $\Delta fG$ for each Kegg identifier and for each metabolite identifier. 

The absence of data on a reaction or metabolite will result in either no constraint or a default value (the conditions for applying either condition will be described at greater length in the following section).

### What you need to know before use

- the default transport recation will be eliminated (all reactions starting with EX_ ..., it is up to the user to clearly write his reactions)

-  Molecule transport reactions and the biomassede biomass reaction have no constraints on the deltaG

### Other information

If you want to use the extension, you’ll need to specify the extension by pointing to the extension script.
Note that reversible reactions are split in *aspefm* in two reactions in the opposite direction. If this weren’t the case, we wouldn’t be able to write the linear constraint $\Delta rG \le 0$. The constraint implying that we can’t have the reaction in both directions at the same time is a constraint directly inscribed in the EFM search. If we define the support as a list of Boolean describing the active reactions of a given EFM. In this case, it’s impossible to have a reaction in both directions, i.e. it’s impossible to have a Boolean number of 1 for the reaction in one direction and 1 in the other.

