clingoLP="clingo clingoLP.py"
clingoLPParams=(-c nstrict=0 -c epsilon="(1,2)" -c accuracy=10)
clingoLPParams="${clingoLPParams[@]}"
minSupport="--heuristic Domain --enum-mode domRec "
clingoParams="--stats=2"

# ------------------------------- Mario Equilibrator ---------------------------

echo "== Go to equilibrator to find Delta f G0'. =="
python equilibrator_recup_data.py -i data/mario/Correspondances_mario.txt -o data/mario/deltaGf0_mario.txt
echo "> written in deltaGo_carlson.txt"

# ------------------------------- Mario Basic Test ---------------------------

echo "== Remove solutions ASPEFM Mario Without Thermodynamics =="
$clingoLP $clingoLPParams $clingoParams $minSupport solveLP.lp4 -c debug=1 data/mario/model_mario2013_asp.lp4 -n 0 > outputs/Mario/model_mario2013.txt
echo "= Ecriture dans Output pour Mario without_Thermo="


# ------------------------------- Mario Test for loop for percentages and fixed ---------------------------


for pourcentage in 0 10 15 20 50 75 85 90 95 99
do
    for time in 0 48 72 96
    do
        echo "== Remove solutions using thermodynamics Carlson Test de Base with time and pourcentage =="
        $clingoLP $clingoLPParams $clingoParams $minSupport solveLP.lp4 -c debug=1 extensions/thermo_dg_checker.py -c sbml=\"data/mario/model_mario2013.xml\" -c concentrations=\"data/mario/concentration_files/concentration_files_variable_${pourcentage}/concentration_mario_time_${time}_modifie_ext_${pourcentage}_pourcents.txt\" -c deltaG=\"data/mario/deltaGf0_mario.txt\" data/mario/model_mario2013_asp.lp4 -n 0 > outputs/Mario/t_${time}/model_mario2013_deltaG0_t_${time}_C_fixe_modifie_ext_${pourcentage}_pourcents.txt
        echo "> Writing in Output for carlson DeltaGo ="

        #parse output
        echo "== parse EFMs into a csv file =="
        python parse_output.py outputs/Mario/t_${time}/model_mario2013_deltaG0_t_${time}_C_fixe_modifie_ext_${pourcentage}_pourcents.txt --csv outputs/Mario/t_${time}/model_mario2013_deltaG0_t_${time}_C_fixe_modifie_ext_${pourcentage}_pourcents.csv
        echo "> written in Mario.csv"
    done
done





