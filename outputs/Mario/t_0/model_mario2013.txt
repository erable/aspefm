clingo version 5.6.2
Reading from clingoLP.py ...
Solving...

LP Solver output
(0.0, {'flux("PGI")': 0.01, 'flux("GLDH")': 0.0, 'flux("GLNT_rev")': 0.0, 'flux("HK")': 0.01, 'flux("PK")': 0.02, 'flux("SDH")': 0.02, 'flux("MDH")': 0.02, 'flux("CK")': 0.0, 'flux("PPRIBP")': 0.0, 'flux("TK")': 0.0, 'flux("AKGDH")': 0.02, 'flux("GLNT")': 0.0, 'flux("RESP")': 0.0, 'flux("GLNS")': 0.0, 'flux("LEAK")': 0.0566, 'flux("EP")': 0.0, 'flux("PDH")': 0.02, 'flux("GLUT")': 0.0, 'flux("AK")': 0.0, 'flux("LDH_rev")': 0.0, 'flux("ATPASE")': 0.04, 'flux("LDH")': 0.0, 'flux("PGK")': 0.02, 'flux("G6PDH")': 0.0, 'flux("CS")': 0.02, 'flux("GLNS_rev")': 0.0, 'flux("ALATA")': 0.0, 'flux("CK_rev")': 0.0, 'flux("NADPHOX")': 0.0, 'flux("SCOAS")': 0.02, 'flux("ME")': 0.0, 'flux("GROWTH")': 0.0, 'flux("CITS")': 0.02, 'flux("AK_rev")': 0.0, 'flux("PFK")': 0.01})
Answer: 1
support("HK") support("PGI") support("PFK") support("PGK") support("PK") support("PDH") support("CS") support("CITS") support("AKGDH") support("SCOAS") support("SDH") support("MDH") support("LEAK") support("ATPASE")

LP solver calls: 52   Time cplex :  0.009435415267944336



LP Solver output
(0.0, {'flux("PGI")': 0.0, 'flux("GLDH")': 0.0, 'flux("GLNT_rev")': 0.0, 'flux("HK")': 0.0, 'flux("PK")': 0.0, 'flux("SDH")': 0.01, 'flux("MDH")': 0.01, 'flux("CK")': 0.0, 'flux("PPRIBP")': 0.0, 'flux("TK")': 0.0, 'flux("AKGDH")': 0.01, 'flux("GLNT")': 0.0, 'flux("RESP")': 0.0283, 'flux("GLNS")': 0.0, 'flux("LEAK")': 0.0, 'flux("EP")': 0.0, 'flux("PDH")': 0.01, 'flux("GLUT")': 0.0, 'flux("AK")': 0.0, 'flux("LDH_rev")': 0.01, 'flux("ATPASE")': 0.0666, 'flux("LDH")': 0.0, 'flux("PGK")': 0.0, 'flux("G6PDH")': 0.0, 'flux("CS")': 0.01, 'flux("GLNS_rev")': 0.0, 'flux("ALATA")': 0.0, 'flux("CK_rev")': 0.0, 'flux("NADPHOX")': 0.0, 'flux("SCOAS")': 0.01, 'flux("ME")': 0.0, 'flux("GROWTH")': 0.0, 'flux("CITS")': 0.01, 'flux("AK_rev")': 0.0, 'flux("PFK")': 0.0})
Answer: 2
support("LDH_rev") support("PDH") support("CS") support("CITS") support("AKGDH") support("SCOAS") support("SDH") support("MDH") support("RESP") support("ATPASE")

LP solver calls: 61   Time cplex :  0.011241674423217773



LP Solver output
(0.0, {'flux("PGI")': 0.0, 'flux("GLDH")': 0.0, 'flux("GLNT_rev")': 0.0, 'flux("HK")': 0.0, 'flux("PK")': 0.0, 'flux("SDH")': 0.01, 'flux("MDH")': 0.01, 'flux("CK")': 0.0, 'flux("PPRIBP")': 0.0, 'flux("TK")': 0.0, 'flux("AKGDH")': 0.01, 'flux("GLNT")': 0.0, 'flux("RESP")': 0.0, 'flux("GLNS")': 0.0, 'flux("LEAK")': 0.0283, 'flux("EP")': 0.0, 'flux("PDH")': 0.01, 'flux("GLUT")': 0.0, 'flux("AK")': 0.0, 'flux("LDH_rev")': 0.01, 'flux("ATPASE")': 0.01, 'flux("LDH")': 0.0, 'flux("PGK")': 0.0, 'flux("G6PDH")': 0.0, 'flux("CS")': 0.01, 'flux("GLNS_rev")': 0.0, 'flux("ALATA")': 0.0, 'flux("CK_rev")': 0.0, 'flux("NADPHOX")': 0.0, 'flux("SCOAS")': 0.01, 'flux("ME")': 0.0, 'flux("GROWTH")': 0.0, 'flux("CITS")': 0.01, 'flux("AK_rev")': 0.0, 'flux("PFK")': 0.0})
Answer: 3
support("LDH_rev") support("PDH") support("CS") support("CITS") support("AKGDH") support("SCOAS") support("SDH") support("MDH") support("LEAK") support("ATPASE")

LP solver calls: 65   Time cplex :  0.011953353881835938



LP Solver output
(0.0, {'flux("PGI")': 0.0, 'flux("GLDH")': 0.0, 'flux("GLNT_rev")': 0.0, 'flux("HK")': 0.0, 'flux("PK")': 0.0, 'flux("SDH")': 0.0, 'flux("MDH")': 0.0, 'flux("CK")': 0.01, 'flux("PPRIBP")': 0.0, 'flux("TK")': 0.0, 'flux("AKGDH")': 0.0, 'flux("GLNT")': 0.01, 'flux("RESP")': 0.0, 'flux("GLNS")': 0.01, 'flux("LEAK")': 0.0, 'flux("EP")': 0.0, 'flux("PDH")': 0.0, 'flux("GLUT")': 0.01, 'flux("AK")': 0.0, 'flux("LDH_rev")': 0.0, 'flux("ATPASE")': 0.01, 'flux("LDH")': 0.0, 'flux("PGK")': 0.0, 'flux("G6PDH")': 0.0, 'flux("CS")': 0.0, 'flux("GLNS_rev")': 0.0, 'flux("ALATA")': 0.0, 'flux("CK_rev")': 0.0, 'flux("NADPHOX")': 0.0, 'flux("SCOAS")': 0.0, 'flux("ME")': 0.0, 'flux("GROWTH")': 0.0, 'flux("CITS")': 0.0, 'flux("AK_rev")': 0.0, 'flux("PFK")': 0.0})
Answer: 4
support("GLNT") support("GLNS") support("GLUT") support("ATPASE") support("CK")

LP solver calls: 72   Time cplex :  0.013215303421020508



LP Solver output
(0.0, {'flux("PGI")': 0.01, 'flux("GLDH")': 0.0, 'flux("GLNT_rev")': 0.0, 'flux("HK")': 0.01, 'flux("PK")': 0.02, 'flux("SDH")': 0.0, 'flux("MDH")': 0.0, 'flux("CK")': 0.0, 'flux("PPRIBP")': 0.0, 'flux("TK")': 0.0, 'flux("AKGDH")': 0.0, 'flux("GLNT")': 0.0, 'flux("RESP")': 0.0, 'flux("GLNS")': 0.0, 'flux("LEAK")': 0.0, 'flux("EP")': 0.0, 'flux("PDH")': 0.0, 'flux("GLUT")': 0.0, 'flux("AK")': 0.0, 'flux("LDH_rev")': 0.0, 'flux("ATPASE")': 0.02, 'flux("LDH")': 0.02, 'flux("PGK")': 0.02, 'flux("G6PDH")': 0.0, 'flux("CS")': 0.0, 'flux("GLNS_rev")': 0.0, 'flux("ALATA")': 0.0, 'flux("CK_rev")': 0.0, 'flux("NADPHOX")': 0.0, 'flux("SCOAS")': 0.0, 'flux("ME")': 0.0, 'flux("GROWTH")': 0.0, 'flux("CITS")': 0.0, 'flux("AK_rev")': 0.0, 'flux("PFK")': 0.01})
Answer: 5
support("HK") support("PGI") support("PFK") support("PGK") support("PK") support("LDH") support("ATPASE")

LP solver calls: 78   Time cplex :  0.014603137969970703



LP Solver output
(0.0, {'flux("PGI")': 0.0, 'flux("GLDH")': 0.0, 'flux("GLNT_rev")': 0.0, 'flux("HK")': 0.03, 'flux("PK")': 0.05, 'flux("SDH")': 0.0, 'flux("MDH")': 0.0, 'flux("CK")': 0.0, 'flux("PPRIBP")': 0.0, 'flux("TK")': 0.01, 'flux("AKGDH")': 0.0, 'flux("GLNT")': 0.0, 'flux("RESP")': 0.0, 'flux("GLNS")': 0.0, 'flux("LEAK")': 0.0, 'flux("EP")': 0.02, 'flux("PDH")': 0.0, 'flux("GLUT")': 0.0, 'flux("AK")': 0.0, 'flux("LDH_rev")': 0.0, 'flux("ATPASE")': 0.05, 'flux("LDH")': 0.05, 'flux("PGK")': 0.05, 'flux("G6PDH")': 0.03, 'flux("CS")': 0.0, 'flux("GLNS_rev")': 0.0, 'flux("ALATA")': 0.0, 'flux("CK_rev")': 0.0, 'flux("NADPHOX")': 0.06, 'flux("SCOAS")': 0.0, 'flux("ME")': 0.0, 'flux("GROWTH")': 0.0, 'flux("CITS")': 0.0, 'flux("AK_rev")': 0.0, 'flux("PFK")': 0.02})
Answer: 6
support("HK") support("PFK") support("PGK") support("PK") support("LDH") support("G6PDH") support("EP") support("TK") support("ATPASE") support("NADPHOX")

LP solver calls: 94   Time cplex :  0.017398834228515625



LP Solver output
(0.0, {'flux("PGI")': 0.01, 'flux("GLDH")': 0.0, 'flux("GLNT_rev")': 0.0, 'flux("HK")': 0.01, 'flux("PK")': 0.02, 'flux("SDH")': 0.02, 'flux("MDH")': 0.02, 'flux("CK")': 0.0, 'flux("PPRIBP")': 0.0, 'flux("TK")': 0.0, 'flux("AKGDH")': 0.02, 'flux("GLNT")': 0.0, 'flux("RESP")': 0.0566, 'flux("GLNS")': 0.0, 'flux("LEAK")': 0.0, 'flux("EP")': 0.0, 'flux("PDH")': 0.02, 'flux("GLUT")': 0.0, 'flux("AK")': 0.0, 'flux("LDH_rev")': 0.0, 'flux("ATPASE")': 0.1532, 'flux("LDH")': 0.0, 'flux("PGK")': 0.02, 'flux("G6PDH")': 0.0, 'flux("CS")': 0.02, 'flux("GLNS_rev")': 0.0, 'flux("ALATA")': 0.0, 'flux("CK_rev")': 0.0, 'flux("NADPHOX")': 0.0, 'flux("SCOAS")': 0.02, 'flux("ME")': 0.0, 'flux("GROWTH")': 0.0, 'flux("CITS")': 0.02, 'flux("AK_rev")': 0.0, 'flux("PFK")': 0.01})
Answer: 7
support("HK") support("PGI") support("PFK") support("PGK") support("PK") support("PDH") support("CS") support("CITS") support("AKGDH") support("SCOAS") support("SDH") support("MDH") support("RESP") support("ATPASE")

LP solver calls: 104   Time cplex :  0.01959967613220215



LP Solver output
(0.0, {'flux("PGI")': 0.0, 'flux("GLDH")': 0.0, 'flux("GLNT_rev")': 0.0, 'flux("HK")': 0.03, 'flux("PK")': 0.05, 'flux("SDH")': 0.05, 'flux("MDH")': 0.05, 'flux("CK")': 0.0, 'flux("PPRIBP")': 0.0, 'flux("TK")': 0.01, 'flux("AKGDH")': 0.05, 'flux("GLNT")': 0.0, 'flux("RESP")': 0.0, 'flux("GLNS")': 0.0, 'flux("LEAK")': 0.1415, 'flux("EP")': 0.02, 'flux("PDH")': 0.05, 'flux("GLUT")': 0.0, 'flux("AK")': 0.0, 'flux("LDH_rev")': 0.0, 'flux("ATPASE")': 0.1, 'flux("LDH")': 0.0, 'flux("PGK")': 0.05, 'flux("G6PDH")': 0.03, 'flux("CS")': 0.05, 'flux("GLNS_rev")': 0.0, 'flux("ALATA")': 0.0, 'flux("CK_rev")': 0.0, 'flux("NADPHOX")': 0.06, 'flux("SCOAS")': 0.05, 'flux("ME")': 0.0, 'flux("GROWTH")': 0.0, 'flux("CITS")': 0.05, 'flux("AK_rev")': 0.0, 'flux("PFK")': 0.02})
Answer: 8
support("HK") support("PFK") support("PGK") support("PK") support("G6PDH") support("EP") support("TK") support("PDH") support("CS") support("CITS") support("AKGDH") support("SCOAS") support("SDH") support("MDH") support("LEAK") support("ATPASE") support("NADPHOX")

LP solver calls: 112   Time cplex :  0.021289825439453125



LP Solver output
(0.0, {'flux("PGI")': 0.0, 'flux("GLDH")': 0.0, 'flux("GLNT_rev")': 0.0, 'flux("HK")': 0.03, 'flux("PK")': 0.05, 'flux("SDH")': 0.05, 'flux("MDH")': 0.05, 'flux("CK")': 0.0, 'flux("PPRIBP")': 0.0, 'flux("TK")': 0.01, 'flux("AKGDH")': 0.05, 'flux("GLNT")': 0.0, 'flux("RESP")': 0.1415, 'flux("GLNS")': 0.0, 'flux("LEAK")': 0.0, 'flux("EP")': 0.02, 'flux("PDH")': 0.05, 'flux("GLUT")': 0.0, 'flux("AK")': 0.0, 'flux("LDH_rev")': 0.0, 'flux("ATPASE")': 0.383, 'flux("LDH")': 0.0, 'flux("PGK")': 0.05, 'flux("G6PDH")': 0.03, 'flux("CS")': 0.05, 'flux("GLNS_rev")': 0.0, 'flux("ALATA")': 0.0, 'flux("CK_rev")': 0.0, 'flux("NADPHOX")': 0.06, 'flux("SCOAS")': 0.05, 'flux("ME")': 0.0, 'flux("GROWTH")': 0.0, 'flux("CITS")': 0.05, 'flux("AK_rev")': 0.0, 'flux("PFK")': 0.02})
Answer: 9
support("HK") support("PFK") support("PGK") support("PK") support("G6PDH") support("EP") support("TK") support("PDH") support("CS") support("CITS") support("AKGDH") support("SCOAS") support("SDH") support("MDH") support("RESP") support("ATPASE") support("NADPHOX")

LP solver calls: 117   Time cplex :  0.022153615951538086



LP Solver output
(0.0, {'flux("PGI")': 0.0, 'flux("GLDH")': 0.0, 'flux("GLNT_rev")': 0.0, 'flux("HK")': 0.0, 'flux("PK")': 0.0, 'flux("SDH")': 0.01, 'flux("MDH")': 0.0, 'flux("CK")': 0.01, 'flux("PPRIBP")': 0.0, 'flux("TK")': 0.0, 'flux("AKGDH")': 0.01, 'flux("GLNT")': 0.01, 'flux("RESP")': 0.0, 'flux("GLNS")': 0.01, 'flux("LEAK")': 0.0133, 'flux("EP")': 0.0, 'flux("PDH")': 0.0, 'flux("GLUT")': 0.0, 'flux("AK")': 0.0, 'flux("LDH_rev")': 0.0, 'flux("ATPASE")': 0.01, 'flux("LDH")': 0.0, 'flux("PGK")': 0.0, 'flux("G6PDH")': 0.0, 'flux("CS")': 0.0, 'flux("GLNS_rev")': 0.0, 'flux("ALATA")': 0.01, 'flux("CK_rev")': 0.0, 'flux("NADPHOX")': 0.0, 'flux("SCOAS")': 0.01, 'flux("ME")': 0.01, 'flux("GROWTH")': 0.0, 'flux("CITS")': 0.0, 'flux("AK_rev")': 0.0, 'flux("PFK")': 0.0})
Answer: 10
support("AKGDH") support("SCOAS") support("SDH") support("ME") support("GLNT") support("GLNS") support("ALATA") support("LEAK") support("ATPASE") support("CK")

LP solver calls: 129   Time cplex :  0.02457427978515625



LP Solver output
(0.0, {'flux("PGI")': 0.0, 'flux("GLDH")': 0.0, 'flux("GLNT_rev")': 0.0, 'flux("HK")': 0.0, 'flux("PK")': 0.0, 'flux("SDH")': 0.01, 'flux("MDH")': 0.0, 'flux("CK")': 0.01, 'flux("PPRIBP")': 0.0, 'flux("TK")': 0.0, 'flux("AKGDH")': 0.01, 'flux("GLNT")': 0.01, 'flux("RESP")': 0.0133, 'flux("GLNS")': 0.01, 'flux("LEAK")': 0.0, 'flux("EP")': 0.0, 'flux("PDH")': 0.0, 'flux("GLUT")': 0.0, 'flux("AK")': 0.0, 'flux("LDH_rev")': 0.0, 'flux("ATPASE")': 0.0366, 'flux("LDH")': 0.0, 'flux("PGK")': 0.0, 'flux("G6PDH")': 0.0, 'flux("CS")': 0.0, 'flux("GLNS_rev")': 0.0, 'flux("ALATA")': 0.01, 'flux("CK_rev")': 0.0, 'flux("NADPHOX")': 0.0, 'flux("SCOAS")': 0.01, 'flux("ME")': 0.01, 'flux("GROWTH")': 0.0, 'flux("CITS")': 0.0, 'flux("AK_rev")': 0.0, 'flux("PFK")': 0.0})
Answer: 11
support("AKGDH") support("SCOAS") support("SDH") support("ME") support("GLNT") support("GLNS") support("ALATA") support("RESP") support("ATPASE") support("CK")

LP solver calls: 134   Time cplex :  0.025441408157348633



LP Solver output
(0.0, {'flux("PGI")': 0.0, 'flux("GLDH")': 0.01, 'flux("GLNT_rev")': 0.0, 'flux("HK")': 0.0, 'flux("PK")': 0.0, 'flux("SDH")': 0.02, 'flux("MDH")': 0.01, 'flux("CK")': 0.01, 'flux("PPRIBP")': 0.0, 'flux("TK")': 0.0, 'flux("AKGDH")': 0.02, 'flux("GLNT")': 0.01, 'flux("RESP")': 0.0416, 'flux("GLNS")': 0.01, 'flux("LEAK")': 0.0, 'flux("EP")': 0.0, 'flux("PDH")': 0.01, 'flux("GLUT")': 0.0, 'flux("AK")': 0.0, 'flux("LDH_rev")': 0.0, 'flux("ATPASE")': 0.1032, 'flux("LDH")': 0.0, 'flux("PGK")': 0.0, 'flux("G6PDH")': 0.0, 'flux("CS")': 0.01, 'flux("GLNS_rev")': 0.0, 'flux("ALATA")': 0.0, 'flux("CK_rev")': 0.0, 'flux("NADPHOX")': 0.0, 'flux("SCOAS")': 0.02, 'flux("ME")': 0.01, 'flux("GROWTH")': 0.0, 'flux("CITS")': 0.01, 'flux("AK_rev")': 0.0, 'flux("PFK")': 0.0})
Answer: 12
support("PDH") support("CS") support("CITS") support("AKGDH") support("SCOAS") support("SDH") support("MDH") support("ME") support("GLNT") support("GLNS") support("GLDH") support("RESP") support("ATPASE") support("CK")

LP solver calls: 144   Time cplex :  0.027441740036010742



LP Solver output
(0.0, {'flux("PGI")': 0.0, 'flux("GLDH")': 0.01, 'flux("GLNT_rev")': 0.0, 'flux("HK")': 0.0, 'flux("PK")': 0.0, 'flux("SDH")': 0.02, 'flux("MDH")': 0.01, 'flux("CK")': 0.01, 'flux("PPRIBP")': 0.0, 'flux("TK")': 0.0, 'flux("AKGDH")': 0.02, 'flux("GLNT")': 0.01, 'flux("RESP")': 0.0, 'flux("GLNS")': 0.01, 'flux("LEAK")': 0.0416, 'flux("EP")': 0.0, 'flux("PDH")': 0.01, 'flux("GLUT")': 0.0, 'flux("AK")': 0.0, 'flux("LDH_rev")': 0.0, 'flux("ATPASE")': 0.02, 'flux("LDH")': 0.0, 'flux("PGK")': 0.0, 'flux("G6PDH")': 0.0, 'flux("CS")': 0.01, 'flux("GLNS_rev")': 0.0, 'flux("ALATA")': 0.0, 'flux("CK_rev")': 0.0, 'flux("NADPHOX")': 0.0, 'flux("SCOAS")': 0.02, 'flux("ME")': 0.01, 'flux("GROWTH")': 0.0, 'flux("CITS")': 0.01, 'flux("AK_rev")': 0.0, 'flux("PFK")': 0.0})
Answer: 13
support("PDH") support("CS") support("CITS") support("AKGDH") support("SCOAS") support("SDH") support("MDH") support("ME") support("GLNT") support("GLNS") support("GLDH") support("LEAK") support("ATPASE") support("CK")

LP solver calls: 147   Time cplex :  0.027954578399658203



LP Solver output
(0.0, {'flux("PGI")': 0.0, 'flux("GLDH")': 0.0, 'flux("GLNT_rev")': 0.0, 'flux("HK")': 0.3179452055, 'flux("PK")': 0.0, 'flux("SDH")': 0.5260273973, 'flux("MDH")': 0.7808219178, 'flux("CK")': 2.3991780822, 'flux("PPRIBP")': 0.5178082192, 'flux("TK")': 0.0, 'flux("AKGDH")': 0.5260273973, 'flux("GLNT")': 0.01, 'flux("RESP")': 0.0, 'flux("GLNS")': 0.0, 'flux("LEAK")': 1.8708493151, 'flux("EP")': 0.0, 'flux("PDH")': 0.7808219178, 'flux("GLUT")': 0.0, 'flux("AK")': 0.5178082192, 'flux("LDH_rev")': 0.0, 'flux("ATPASE")': 0.5260273973, 'flux("LDH")': 0.0, 'flux("PGK")': 0.0, 'flux("G6PDH")': 0.3139726027, 'flux("CS")': 0.7808219178, 'flux("GLNS_rev")': 1.0310958904, 'flux("ALATA")': 0.0, 'flux("CK_rev")': 0.0, 'flux("NADPHOX")': 0.6279452055, 'flux("SCOAS")': 0.5260273973, 'flux("ME")': 0.7808219178, 'flux("GROWTH")': 0.1369863014, 'flux("CITS")': 0.5260273973, 'flux("AK_rev")': 0.0, 'flux("PFK")': 0.0})
Answer: 14
support("HK") support("G6PDH") support("PDH") support("CS") support("CITS") support("AKGDH") support("SCOAS") support("SDH") support("MDH") support("ME") support("GLNT") support("GLNS_rev") support("LEAK") support("ATPASE") support("AK") support("CK") support("PPRIBP") support("NADPHOX") support("GROWTH")

LP solver calls: 163   Time cplex :  0.0326690673828125



LP Solver output
(0.0, {'flux("PGI")': 0.0, 'flux("GLDH")': 0.0, 'flux("GLNT_rev")': 0.0, 'flux("HK")': 0.02321, 'flux("PK")': 0.0, 'flux("SDH")': 0.0384, 'flux("MDH")': 0.057, 'flux("CK")': 0.25041, 'flux("PPRIBP")': 0.0378, 'flux("TK")': 0.0, 'flux("AKGDH")': 0.0384, 'flux("GLNT")': 0.076, 'flux("RESP")': 0.0, 'flux("GLNS")': 0.0, 'flux("LEAK")': 0.136572, 'flux("EP")': 0.0, 'flux("PDH")': 0.057, 'flux("GLUT")': 0.07527, 'flux("AK")': 0.0378, 'flux("LDH_rev")': 0.0, 'flux("ATPASE")': 0.11367, 'flux("LDH")': 0.0, 'flux("PGK")': 0.0, 'flux("G6PDH")': 0.02292, 'flux("CS")': 0.057, 'flux("GLNS_rev")': 0.0, 'flux("ALATA")': 0.0, 'flux("CK_rev")': 0.0, 'flux("NADPHOX")': 0.04584, 'flux("SCOAS")': 0.0384, 'flux("ME")': 0.057, 'flux("GROWTH")': 0.01, 'flux("CITS")': 0.0384, 'flux("AK_rev")': 0.0, 'flux("PFK")': 0.0})
Answer: 15
support("HK") support("G6PDH") support("PDH") support("CS") support("CITS") support("AKGDH") support("SCOAS") support("SDH") support("MDH") support("ME") support("GLNT") support("GLUT") support("LEAK") support("ATPASE") support("AK") support("CK") support("PPRIBP") support("NADPHOX") support("GROWTH")

LP solver calls: 166   Time cplex :  0.03349733352661133



LP Solver output
(0.0, {'flux("PGI")': 0.0, 'flux("GLDH")': 0.07527, 'flux("GLNT_rev")': 0.0, 'flux("HK")': 0.02321, 'flux("PK")': 0.0, 'flux("SDH")': 0.18894, 'flux("MDH")': 0.13227, 'flux("CK")': 0.25041, 'flux("PPRIBP")': 0.0378, 'flux("TK")': 0.0, 'flux("AKGDH")': 0.18894, 'flux("GLNT")': 0.076, 'flux("RESP")': 0.0, 'flux("GLNS")': 0.0, 'flux("LEAK")': 0.4496952, 'flux("EP")': 0.0, 'flux("PDH")': 0.13227, 'flux("GLUT")': 0.0, 'flux("AK")': 0.0378, 'flux("LDH_rev")': 0.0, 'flux("ATPASE")': 0.18894, 'flux("LDH")': 0.0, 'flux("PGK")': 0.0, 'flux("G6PDH")': 0.02292, 'flux("CS")': 0.13227, 'flux("GLNS_rev")': 0.0, 'flux("ALATA")': 0.0, 'flux("CK_rev")': 0.0, 'flux("NADPHOX")': 0.04584, 'flux("SCOAS")': 0.18894, 'flux("ME")': 0.13227, 'flux("GROWTH")': 0.01, 'flux("CITS")': 0.11367, 'flux("AK_rev")': 0.0, 'flux("PFK")': 0.0})
Answer: 16
support("HK") support("G6PDH") support("PDH") support("CS") support("CITS") support("AKGDH") support("SCOAS") support("SDH") support("MDH") support("ME") support("GLNT") support("GLDH") support("LEAK") support("ATPASE") support("AK") support("CK") support("PPRIBP") support("NADPHOX") support("GROWTH")

LP solver calls: 168   Time cplex :  0.03403306007385254



LP Solver output
(0.0, {'flux("PGI")': 0.0, 'flux("GLDH")': 0.0, 'flux("GLNT_rev")': 0.0, 'flux("HK")': 0.3179452055, 'flux("PK")': 0.0, 'flux("SDH")': 0.5260273973, 'flux("MDH")': 0.7808219178, 'flux("CK")': 2.3991780822, 'flux("PPRIBP")': 0.5178082192, 'flux("TK")': 0.0, 'flux("AKGDH")': 0.5260273973, 'flux("GLNT")': 0.01, 'flux("RESP")': 1.8708493151, 'flux("GLNS")': 0.0, 'flux("LEAK")': 0.0, 'flux("EP")': 0.0, 'flux("PDH")': 0.7808219178, 'flux("GLUT")': 0.0, 'flux("AK")': 0.5178082192, 'flux("LDH_rev")': 0.0, 'flux("ATPASE")': 4.2677260274, 'flux("LDH")': 0.0, 'flux("PGK")': 0.0, 'flux("G6PDH")': 0.3139726027, 'flux("CS")': 0.7808219178, 'flux("GLNS_rev")': 1.0310958904, 'flux("ALATA")': 0.0, 'flux("CK_rev")': 0.0, 'flux("NADPHOX")': 0.6279452055, 'flux("SCOAS")': 0.5260273973, 'flux("ME")': 0.7808219178, 'flux("GROWTH")': 0.1369863014, 'flux("CITS")': 0.5260273973, 'flux("AK_rev")': 0.0, 'flux("PFK")': 0.0})
Answer: 17
support("HK") support("G6PDH") support("PDH") support("CS") support("CITS") support("AKGDH") support("SCOAS") support("SDH") support("MDH") support("ME") support("GLNT") support("GLNS_rev") support("RESP") support("ATPASE") support("AK") support("CK") support("PPRIBP") support("NADPHOX") support("GROWTH")

LP solver calls: 173   Time cplex :  0.035417795181274414



LP Solver output
(0.0, {'flux("PGI")': 0.0, 'flux("GLDH")': 0.0, 'flux("GLNT_rev")': 0.0, 'flux("HK")': 0.02321, 'flux("PK")': 0.0, 'flux("SDH")': 0.0384, 'flux("MDH")': 0.057, 'flux("CK")': 0.25041, 'flux("PPRIBP")': 0.0378, 'flux("TK")': 0.0, 'flux("AKGDH")': 0.0384, 'flux("GLNT")': 0.076, 'flux("RESP")': 0.136572, 'flux("GLNS")': 0.0, 'flux("LEAK")': 0.0, 'flux("EP")': 0.0, 'flux("PDH")': 0.057, 'flux("GLUT")': 0.07527, 'flux("AK")': 0.0378, 'flux("LDH_rev")': 0.0, 'flux("ATPASE")': 0.386814, 'flux("LDH")': 0.0, 'flux("PGK")': 0.0, 'flux("G6PDH")': 0.02292, 'flux("CS")': 0.057, 'flux("GLNS_rev")': 0.0, 'flux("ALATA")': 0.0, 'flux("CK_rev")': 0.0, 'flux("NADPHOX")': 0.04584, 'flux("SCOAS")': 0.0384, 'flux("ME")': 0.057, 'flux("GROWTH")': 0.01, 'flux("CITS")': 0.0384, 'flux("AK_rev")': 0.0, 'flux("PFK")': 0.0})
Answer: 18
support("HK") support("G6PDH") support("PDH") support("CS") support("CITS") support("AKGDH") support("SCOAS") support("SDH") support("MDH") support("ME") support("GLNT") support("GLUT") support("RESP") support("ATPASE") support("AK") support("CK") support("PPRIBP") support("NADPHOX") support("GROWTH")

LP solver calls: 175   Time cplex :  0.035982370376586914



LP Solver output
(0.0, {'flux("PGI")': 0.0, 'flux("GLDH")': 0.07527, 'flux("GLNT_rev")': 0.0, 'flux("HK")': 0.02321, 'flux("PK")': 0.0, 'flux("SDH")': 0.18894, 'flux("MDH")': 0.13227, 'flux("CK")': 0.25041, 'flux("PPRIBP")': 0.0378, 'flux("TK")': 0.0, 'flux("AKGDH")': 0.18894, 'flux("GLNT")': 0.076, 'flux("RESP")': 0.4496952, 'flux("GLNS")': 0.0, 'flux("LEAK")': 0.0, 'flux("EP")': 0.0, 'flux("PDH")': 0.13227, 'flux("GLUT")': 0.0, 'flux("AK")': 0.0378, 'flux("LDH_rev")': 0.0, 'flux("ATPASE")': 1.0883304, 'flux("LDH")': 0.0, 'flux("PGK")': 0.0, 'flux("G6PDH")': 0.02292, 'flux("CS")': 0.13227, 'flux("GLNS_rev")': 0.0, 'flux("ALATA")': 0.0, 'flux("CK_rev")': 0.0, 'flux("NADPHOX")': 0.04584, 'flux("SCOAS")': 0.18894, 'flux("ME")': 0.13227, 'flux("GROWTH")': 0.01, 'flux("CITS")': 0.11367, 'flux("AK_rev")': 0.0, 'flux("PFK")': 0.0})
Answer: 19
support("HK") support("G6PDH") support("PDH") support("CS") support("CITS") support("AKGDH") support("SCOAS") support("SDH") support("MDH") support("ME") support("GLNT") support("GLDH") support("RESP") support("ATPASE") support("AK") support("CK") support("PPRIBP") support("NADPHOX") support("GROWTH")

LP solver calls: 176   Time cplex :  0.036252498626708984



LP Solver output
(0.0, {'flux("PGI")': 0.0, 'flux("GLDH")': 0.0, 'flux("GLNT_rev")': 0.0, 'flux("HK")': 0.02321, 'flux("PK")': 0.0, 'flux("SDH")': 0.11367, 'flux("MDH")': 0.057, 'flux("CK")': 0.25041, 'flux("PPRIBP")': 0.0378, 'flux("TK")': 0.0, 'flux("AKGDH")': 0.11367, 'flux("GLNT")': 0.076, 'flux("RESP")': 0.2366811, 'flux("GLNS")': 0.0, 'flux("LEAK")': 0.0, 'flux("EP")': 0.0, 'flux("PDH")': 0.057, 'flux("GLUT")': 0.0, 'flux("AK")': 0.0378, 'flux("LDH_rev")': 0.0, 'flux("ATPASE")': 0.5870322, 'flux("LDH")': 0.0, 'flux("PGK")': 0.0, 'flux("G6PDH")': 0.02292, 'flux("CS")': 0.057, 'flux("GLNS_rev")': 0.0, 'flux("ALATA")': 0.07527, 'flux("CK_rev")': 0.0, 'flux("NADPHOX")': 0.04584, 'flux("SCOAS")': 0.11367, 'flux("ME")': 0.13227, 'flux("GROWTH")': 0.01, 'flux("CITS")': 0.0384, 'flux("AK_rev")': 0.0, 'flux("PFK")': 0.0})
Answer: 20
support("HK") support("G6PDH") support("PDH") support("CS") support("CITS") support("AKGDH") support("SCOAS") support("SDH") support("MDH") support("ME") support("GLNT") support("ALATA") support("RESP") support("ATPASE") support("AK") support("CK") support("PPRIBP") support("NADPHOX") support("GROWTH")

LP solver calls: 182   Time cplex :  0.038043975830078125



LP Solver output
(0.0, {'flux("PGI")': 0.0, 'flux("GLDH")': 0.0, 'flux("GLNT_rev")': 0.0, 'flux("HK")': 0.02321, 'flux("PK")': 0.0, 'flux("SDH")': 0.11367, 'flux("MDH")': 0.057, 'flux("CK")': 0.25041, 'flux("PPRIBP")': 0.0378, 'flux("TK")': 0.0, 'flux("AKGDH")': 0.11367, 'flux("GLNT")': 0.076, 'flux("RESP")': 0.0, 'flux("GLNS")': 0.0, 'flux("LEAK")': 0.2366811, 'flux("EP")': 0.0, 'flux("PDH")': 0.057, 'flux("GLUT")': 0.0, 'flux("AK")': 0.0378, 'flux("LDH_rev")': 0.0, 'flux("ATPASE")': 0.11367, 'flux("LDH")': 0.0, 'flux("PGK")': 0.0, 'flux("G6PDH")': 0.02292, 'flux("CS")': 0.057, 'flux("GLNS_rev")': 0.0, 'flux("ALATA")': 0.07527, 'flux("CK_rev")': 0.0, 'flux("NADPHOX")': 0.04584, 'flux("SCOAS")': 0.11367, 'flux("ME")': 0.13227, 'flux("GROWTH")': 0.01, 'flux("CITS")': 0.0384, 'flux("AK_rev")': 0.0, 'flux("PFK")': 0.0})
Answer: 21
support("HK") support("G6PDH") support("PDH") support("CS") support("CITS") support("AKGDH") support("SCOAS") support("SDH") support("MDH") support("ME") support("GLNT") support("ALATA") support("LEAK") support("ATPASE") support("AK") support("CK") support("PPRIBP") support("NADPHOX") support("GROWTH")

LP solver calls: 183   Time cplex :  0.038309335708618164



LP Solver output
(0.0, {'flux("PGI")': 0.0, 'flux("GLDH")': 0.01, 'flux("GLNT_rev")': 0.0, 'flux("HK")': 0.0, 'flux("PK")': 0.0, 'flux("SDH")': 0.01, 'flux("MDH")': 0.0, 'flux("CK")': 0.01, 'flux("PPRIBP")': 0.0, 'flux("TK")': 0.0, 'flux("AKGDH")': 0.01, 'flux("GLNT")': 0.01, 'flux("RESP")': 0.0133, 'flux("GLNS")': 0.01, 'flux("LEAK")': 0.0, 'flux("EP")': 0.0, 'flux("PDH")': 0.0, 'flux("GLUT")': 0.0, 'flux("AK")': 0.0, 'flux("LDH_rev")': 0.0, 'flux("ATPASE")': 0.0366, 'flux("LDH")': 0.01, 'flux("PGK")': 0.0, 'flux("G6PDH")': 0.0, 'flux("CS")': 0.0, 'flux("GLNS_rev")': 0.0, 'flux("ALATA")': 0.0, 'flux("CK_rev")': 0.0, 'flux("NADPHOX")': 0.0, 'flux("SCOAS")': 0.01, 'flux("ME")': 0.01, 'flux("GROWTH")': 0.0, 'flux("CITS")': 0.0, 'flux("AK_rev")': 0.0, 'flux("PFK")': 0.0})
Answer: 22
support("LDH") support("AKGDH") support("SCOAS") support("SDH") support("ME") support("GLNT") support("GLNS") support("GLDH") support("RESP") support("ATPASE") support("CK")

LP solver calls: 193   Time cplex :  0.04059123992919922



LP Solver output
(0.0, {'flux("PGI")': 0.0, 'flux("GLDH")': 0.01, 'flux("GLNT_rev")': 0.0, 'flux("HK")': 0.0, 'flux("PK")': 0.0, 'flux("SDH")': 0.01, 'flux("MDH")': 0.0, 'flux("CK")': 0.01, 'flux("PPRIBP")': 0.0, 'flux("TK")': 0.0, 'flux("AKGDH")': 0.01, 'flux("GLNT")': 0.01, 'flux("RESP")': 0.0, 'flux("GLNS")': 0.01, 'flux("LEAK")': 0.0133, 'flux("EP")': 0.0, 'flux("PDH")': 0.0, 'flux("GLUT")': 0.0, 'flux("AK")': 0.0, 'flux("LDH_rev")': 0.0, 'flux("ATPASE")': 0.01, 'flux("LDH")': 0.01, 'flux("PGK")': 0.0, 'flux("G6PDH")': 0.0, 'flux("CS")': 0.0, 'flux("GLNS_rev")': 0.0, 'flux("ALATA")': 0.0, 'flux("CK_rev")': 0.0, 'flux("NADPHOX")': 0.0, 'flux("SCOAS")': 0.01, 'flux("ME")': 0.01, 'flux("GROWTH")': 0.0, 'flux("CITS")': 0.0, 'flux("AK_rev")': 0.0, 'flux("PFK")': 0.0})
Answer: 23
support("LDH") support("AKGDH") support("SCOAS") support("SDH") support("ME") support("GLNT") support("GLNS") support("GLDH") support("LEAK") support("ATPASE") support("CK")

LP solver calls: 195   Time cplex :  0.04096794128417969



LP Solver output
(0.0, {'flux("PGI")': 0.0, 'flux("GLDH")': 0.0, 'flux("GLNT_rev")': 0.0, 'flux("HK")': 0.3179452055, 'flux("PK")': 0.0, 'flux("SDH")': 0.0, 'flux("MDH")': 0.2547945205, 'flux("CK")': 2.3991780822, 'flux("PPRIBP")': 0.5178082192, 'flux("TK")': 0.0, 'flux("AKGDH")': 0.0, 'flux("GLNT")': 0.01, 'flux("RESP")': 0.3821917808, 'flux("GLNS")': 0.0, 'flux("LEAK")': 0.0, 'flux("EP")': 0.0, 'flux("PDH")': 0.2547945205, 'flux("GLUT")': 0.0, 'flux("AK")': 0.5178082192, 'flux("LDH_rev")': 0.0, 'flux("ATPASE")': 0.7643835616, 'flux("LDH")': 0.5260273973, 'flux("PGK")': 0.0, 'flux("G6PDH")': 0.3139726027, 'flux("CS")': 0.2547945205, 'flux("GLNS_rev")': 1.0310958904, 'flux("ALATA")': 0.0, 'flux("CK_rev")': 0.0, 'flux("NADPHOX")': 0.6279452055, 'flux("SCOAS")': 0.0, 'flux("ME")': 0.7808219178, 'flux("GROWTH")': 0.1369863014, 'flux("CITS")': 0.0, 'flux("AK_rev")': 0.0, 'flux("PFK")': 0.0})
Answer: 24
support("HK") support("LDH") support("G6PDH") support("PDH") support("CS") support("MDH") support("ME") support("GLNT") support("GLNS_rev") support("RESP") support("ATPASE") support("AK") support("CK") support("PPRIBP") support("NADPHOX") support("GROWTH")

LP solver calls: 203   Time cplex :  0.043125152587890625



LP Solver output
(0.0, {'flux("PGI")': 0.0, 'flux("GLDH")': 0.0, 'flux("GLNT_rev")': 0.0, 'flux("HK")': 0.02321, 'flux("PK")': 0.0, 'flux("SDH")': 0.0, 'flux("MDH")': 0.0186, 'flux("CK")': 0.25041, 'flux("PPRIBP")': 0.0378, 'flux("TK")': 0.0, 'flux("AKGDH")': 0.0, 'flux("GLNT")': 0.076, 'flux("RESP")': 0.0279, 'flux("GLNS")': 0.0, 'flux("LEAK")': 0.0, 'flux("EP")': 0.0, 'flux("PDH")': 0.0186, 'flux("GLUT")': 0.07527, 'flux("AK")': 0.0378, 'flux("LDH_rev")': 0.0, 'flux("ATPASE")': 0.13107, 'flux("LDH")': 0.0384, 'flux("PGK")': 0.0, 'flux("G6PDH")': 0.02292, 'flux("CS")': 0.0186, 'flux("GLNS_rev")': 0.0, 'flux("ALATA")': 0.0, 'flux("CK_rev")': 0.0, 'flux("NADPHOX")': 0.04584, 'flux("SCOAS")': 0.0, 'flux("ME")': 0.057, 'flux("GROWTH")': 0.01, 'flux("CITS")': 0.0, 'flux("AK_rev")': 0.0, 'flux("PFK")': 0.0})
Answer: 25
support("HK") support("LDH") support("G6PDH") support("PDH") support("CS") support("MDH") support("ME") support("GLNT") support("GLUT") support("RESP") support("ATPASE") support("AK") support("CK") support("PPRIBP") support("NADPHOX") support("GROWTH")

LP solver calls: 206   Time cplex :  0.043869733810424805



LP Solver output
(0.0, {'flux("PGI")': 0.0, 'flux("GLDH")': 0.0, 'flux("GLNT_rev")': 0.0, 'flux("HK")': 0.3179452055, 'flux("PK")': 0.0, 'flux("SDH")': 0.0, 'flux("MDH")': 0.2547945205, 'flux("CK")': 2.3991780822, 'flux("PPRIBP")': 0.5178082192, 'flux("TK")': 0.0, 'flux("AKGDH")': 0.0, 'flux("GLNT")': 0.01, 'flux("RESP")': 0.0, 'flux("GLNS")': 0.0, 'flux("LEAK")': 0.3821917808, 'flux("EP")': 0.0, 'flux("PDH")': 0.2547945205, 'flux("GLUT")': 0.0, 'flux("AK")': 0.5178082192, 'flux("LDH_rev")': 0.0, 'flux("ATPASE")': 0.0, 'flux("LDH")': 0.5260273973, 'flux("PGK")': 0.0, 'flux("G6PDH")': 0.3139726027, 'flux("CS")': 0.2547945205, 'flux("GLNS_rev")': 1.0310958904, 'flux("ALATA")': 0.0, 'flux("CK_rev")': 0.0, 'flux("NADPHOX")': 0.6279452055, 'flux("SCOAS")': 0.0, 'flux("ME")': 0.7808219178, 'flux("GROWTH")': 0.1369863014, 'flux("CITS")': 0.0, 'flux("AK_rev")': 0.0, 'flux("PFK")': 0.0})
Answer: 26
support("HK") support("LDH") support("G6PDH") support("PDH") support("CS") support("MDH") support("ME") support("GLNT") support("GLNS_rev") support("LEAK") support("AK") support("CK") support("PPRIBP") support("NADPHOX") support("GROWTH")

LP solver calls: 209   Time cplex :  0.04463672637939453



LP Solver output
(0.0, {'flux("PGI")': 0.0, 'flux("GLDH")': 0.0, 'flux("GLNT_rev")': 0.0, 'flux("HK")': 0.02321, 'flux("PK")': 0.0, 'flux("SDH")': 0.0, 'flux("MDH")': 0.0186, 'flux("CK")': 0.25041, 'flux("PPRIBP")': 0.0378, 'flux("TK")': 0.0, 'flux("AKGDH")': 0.0, 'flux("GLNT")': 0.076, 'flux("RESP")': 0.0, 'flux("GLNS")': 0.0, 'flux("LEAK")': 0.0279, 'flux("EP")': 0.0, 'flux("PDH")': 0.0186, 'flux("GLUT")': 0.07527, 'flux("AK")': 0.0378, 'flux("LDH_rev")': 0.0, 'flux("ATPASE")': 0.07527, 'flux("LDH")': 0.0384, 'flux("PGK")': 0.0, 'flux("G6PDH")': 0.02292, 'flux("CS")': 0.0186, 'flux("GLNS_rev")': 0.0, 'flux("ALATA")': 0.0, 'flux("CK_rev")': 0.0, 'flux("NADPHOX")': 0.04584, 'flux("SCOAS")': 0.0, 'flux("ME")': 0.057, 'flux("GROWTH")': 0.01, 'flux("CITS")': 0.0, 'flux("AK_rev")': 0.0, 'flux("PFK")': 0.0})
Answer: 27
support("HK") support("LDH") support("G6PDH") support("PDH") support("CS") support("MDH") support("ME") support("GLNT") support("GLUT") support("LEAK") support("ATPASE") support("AK") support("CK") support("PPRIBP") support("NADPHOX") support("GROWTH")

LP solver calls: 211   Time cplex :  0.045136451721191406



LP Solver output
(0.0, {'flux("PGI")': 0.0, 'flux("GLDH")': 0.0, 'flux("GLNT_rev")': 0.0, 'flux("HK")': 0.02321, 'flux("PK")': 0.0, 'flux("SDH")': 0.07527, 'flux("MDH")': 0.0186, 'flux("CK")': 0.25041, 'flux("PPRIBP")': 0.0378, 'flux("TK")': 0.0, 'flux("AKGDH")': 0.07527, 'flux("GLNT")': 0.076, 'flux("RESP")': 0.1280091, 'flux("GLNS")': 0.0, 'flux("LEAK")': 0.0, 'flux("EP")': 0.0, 'flux("PDH")': 0.0186, 'flux("GLUT")': 0.0, 'flux("AK")': 0.0378, 'flux("LDH_rev")': 0.0, 'flux("ATPASE")': 0.3312882, 'flux("LDH")': 0.0384, 'flux("PGK")': 0.0, 'flux("G6PDH")': 0.02292, 'flux("CS")': 0.0186, 'flux("GLNS_rev")': 0.0, 'flux("ALATA")': 0.07527, 'flux("CK_rev")': 0.0, 'flux("NADPHOX")': 0.04584, 'flux("SCOAS")': 0.07527, 'flux("ME")': 0.13227, 'flux("GROWTH")': 0.01, 'flux("CITS")': 0.0, 'flux("AK_rev")': 0.0, 'flux("PFK")': 0.0})
Answer: 28
support("HK") support("LDH") support("G6PDH") support("PDH") support("CS") support("AKGDH") support("SCOAS") support("SDH") support("MDH") support("ME") support("GLNT") support("ALATA") support("RESP") support("ATPASE") support("AK") support("CK") support("PPRIBP") support("NADPHOX") support("GROWTH")

LP solver calls: 214   Time cplex :  0.04594302177429199



LP Solver output
(0.0, {'flux("PGI")': 0.0, 'flux("GLDH")': 0.07527, 'flux("GLNT_rev")': 0.0, 'flux("HK")': 0.02321, 'flux("PK")': 0.0, 'flux("SDH")': 0.07527, 'flux("MDH")': 0.0186, 'flux("CK")': 0.25041, 'flux("PPRIBP")': 0.0378, 'flux("TK")': 0.0, 'flux("AKGDH")': 0.07527, 'flux("GLNT")': 0.076, 'flux("RESP")': 0.1280091, 'flux("GLNS")': 0.0, 'flux("LEAK")': 0.0, 'flux("EP")': 0.0, 'flux("PDH")': 0.0186, 'flux("GLUT")': 0.0, 'flux("AK")': 0.0378, 'flux("LDH_rev")': 0.0, 'flux("ATPASE")': 0.3312882, 'flux("LDH")': 0.11367, 'flux("PGK")': 0.0, 'flux("G6PDH")': 0.02292, 'flux("CS")': 0.0186, 'flux("GLNS_rev")': 0.0, 'flux("ALATA")': 0.0, 'flux("CK_rev")': 0.0, 'flux("NADPHOX")': 0.04584, 'flux("SCOAS")': 0.07527, 'flux("ME")': 0.13227, 'flux("GROWTH")': 0.01, 'flux("CITS")': 0.0, 'flux("AK_rev")': 0.0, 'flux("PFK")': 0.0})
Answer: 29
support("HK") support("LDH") support("G6PDH") support("PDH") support("CS") support("AKGDH") support("SCOAS") support("SDH") support("MDH") support("ME") support("GLNT") support("GLDH") support("RESP") support("ATPASE") support("AK") support("CK") support("PPRIBP") support("NADPHOX") support("GROWTH")

LP solver calls: 215   Time cplex :  0.04620027542114258



LP Solver output
(0.0, {'flux("PGI")': 0.0, 'flux("GLDH")': 0.0, 'flux("GLNT_rev")': 0.0, 'flux("HK")': 0.02321, 'flux("PK")': 0.0, 'flux("SDH")': 0.07527, 'flux("MDH")': 0.0186, 'flux("CK")': 0.25041, 'flux("PPRIBP")': 0.0378, 'flux("TK")': 0.0, 'flux("AKGDH")': 0.07527, 'flux("GLNT")': 0.076, 'flux("RESP")': 0.0, 'flux("GLNS")': 0.0, 'flux("LEAK")': 0.1280091, 'flux("EP")': 0.0, 'flux("PDH")': 0.0186, 'flux("GLUT")': 0.0, 'flux("AK")': 0.0378, 'flux("LDH_rev")': 0.0, 'flux("ATPASE")': 0.07527, 'flux("LDH")': 0.0384, 'flux("PGK")': 0.0, 'flux("G6PDH")': 0.02292, 'flux("CS")': 0.0186, 'flux("GLNS_rev")': 0.0, 'flux("ALATA")': 0.07527, 'flux("CK_rev")': 0.0, 'flux("NADPHOX")': 0.04584, 'flux("SCOAS")': 0.07527, 'flux("ME")': 0.13227, 'flux("GROWTH")': 0.01, 'flux("CITS")': 0.0, 'flux("AK_rev")': 0.0, 'flux("PFK")': 0.0})
Answer: 30
support("HK") support("LDH") support("G6PDH") support("PDH") support("CS") support("AKGDH") support("SCOAS") support("SDH") support("MDH") support("ME") support("GLNT") support("ALATA") support("LEAK") support("ATPASE") support("AK") support("CK") support("PPRIBP") support("NADPHOX") support("GROWTH")

LP solver calls: 217   Time cplex :  0.04670548439025879



LP Solver output
(0.0, {'flux("PGI")': 0.0, 'flux("GLDH")': 0.07527, 'flux("GLNT_rev")': 0.0, 'flux("HK")': 0.02321, 'flux("PK")': 0.0, 'flux("SDH")': 0.07527, 'flux("MDH")': 0.0186, 'flux("CK")': 0.25041, 'flux("PPRIBP")': 0.0378, 'flux("TK")': 0.0, 'flux("AKGDH")': 0.07527, 'flux("GLNT")': 0.076, 'flux("RESP")': 0.0, 'flux("GLNS")': 0.0, 'flux("LEAK")': 0.1280091, 'flux("EP")': 0.0, 'flux("PDH")': 0.0186, 'flux("GLUT")': 0.0, 'flux("AK")': 0.0378, 'flux("LDH_rev")': 0.0, 'flux("ATPASE")': 0.07527, 'flux("LDH")': 0.11367, 'flux("PGK")': 0.0, 'flux("G6PDH")': 0.02292, 'flux("CS")': 0.0186, 'flux("GLNS_rev")': 0.0, 'flux("ALATA")': 0.0, 'flux("CK_rev")': 0.0, 'flux("NADPHOX")': 0.04584, 'flux("SCOAS")': 0.07527, 'flux("ME")': 0.13227, 'flux("GROWTH")': 0.01, 'flux("CITS")': 0.0, 'flux("AK_rev")': 0.0, 'flux("PFK")': 0.0})
Answer: 31
support("HK") support("LDH") support("G6PDH") support("PDH") support("CS") support("AKGDH") support("SCOAS") support("SDH") support("MDH") support("ME") support("GLNT") support("GLDH") support("LEAK") support("ATPASE") support("AK") support("CK") support("PPRIBP") support("NADPHOX") support("GROWTH")
SATISFIABLE

Models       : 31
Calls        : 1
Time         : 0.912s (Solving: 0.79s 1st Model: 0.31s Unsat: 0.00s)
CPU Time     : 2.070s

Choices      : 626      (Domain: 626)
Conflicts    : 149      (Analyzed: 148)
Restarts     : 2        (Average: 74.00 Last: 136)
Model-Level  : 8.6     
Problems     : 1        (Average Length: 1.00 Splits: 0)
Lemmas       : 148      (Deleted: 0)
  Binary     : 41       (Ratio:  27.70%)
  Ternary    : 23       (Ratio:  15.54%)
  Conflict   : 148      (Average Length:    4.9 Ratio: 100.00%) 
  Loop       : 0        (Average Length:    0.0 Ratio:   0.00%) 
  Other      : 0        (Average Length:    0.0 Ratio:   0.00%) 
Backjumps    : 148      (Average:  3.28 Max:  21 Sum:    485)
  Executed   : 147      (Average:  3.27 Max:  21 Sum:    484 Ratio:  99.79%)
  Bounded    : 1        (Average:  1.00 Max:   1 Sum:      1 Ratio:   0.21%)

Rules        : 372     
  Heuristic  : 35      
Atoms        : 361     
Bodies       : 42       (Original: 41)
Equivalences : 105      (Atom=Atom: 35 Body=Body: 0 Other: 70)
Tight        : Yes
Variables    : 35       (Eliminated:    0 Frozen:   35)
Constraints  : 6        (Binary:  83.3% Ternary:   0.0% Other:  16.7%)


LP solver calls: 218   Time cplex :  0.046952247619628906


