clingo version 5.6.2
Reading from clingoLP.py ...
Je passe ICI
Début de la thermo
line[2] -1781.6444081489221
float(line[2].rstrip('
')) -1781.6444081489221
line[2] -1395.6292467273274
float(line[2].rstrip('
')) -1395.6292467273274
line[2] -622.1619221256469
float(line[2].rstrip('
')) -622.1619221256469
line[2] -521.0449892833246
float(line[2].rstrip('
')) -521.0449892833246
line[2] -2270.4800916865584
float(line[2].rstrip('
')) -2270.4800916865584
line[2] -954.8038650821667
float(line[2].rstrip('
')) -954.8038650821667
line[2] -386.0000000000019
float(line[2].rstrip('
')) -386.0000000000019
line[2] -1727.9787463436437
float(line[2].rstrip('
')) -1727.9787463436437
line[2] 277.8738483657367
float(line[2].rstrip('
')) 277.8738483657367
line[2] -66.99962570890881
float(line[2].rstrip('
')) -66.99962570890881
line[2] -440.7037923253865
float(line[2].rstrip('
')) -440.7037923253865
line[2] -244.1034774178704
float(line[2].rstrip('
')) -244.1034774178704
line[2] -93.47316959760116
float(line[2].rstrip('
')) -93.47316959760116
line[2] -349.1667977317067
float(line[2].rstrip('
')) -349.1667977317067
line[2] -298.5015020229598
float(line[2].rstrip('
')) -298.5015020229598
line[2] 89.29312212928599
float(line[2].rstrip('
')) 89.29312212928599
line[2] -1291.2323086083798
float(line[2].rstrip('
')) -1291.2323086083798
line[2] -1293.8755834729386
float(line[2].rstrip('
')) -1293.8755834729386
line[2] -1077.3005052258698
float(line[2].rstrip('
')) -1077.3005052258698
line[2] -93.47316959760116
float(line[2].rstrip('
')) -93.47316959760116
line[2] -349.1667977317067
float(line[2].rstrip('
')) -349.1667977317067
line[2] -672.3277467814481
float(line[2].rstrip('
')) -672.3277467814481
line[2] -1143.7174255495095
float(line[2].rstrip('
')) -1143.7174255495095
line[2] -1078.084459875474
float(line[2].rstrip('
')) -1078.084459875474
line[2] -2030.775868056622
float(line[2].rstrip('
')) -2030.775868056622
line[2] -1078.084459875474
float(line[2].rstrip('
')) -1078.084459875474
line[2] 16.399999999865035
float(line[2].rstrip('
')) 16.399999999865035
line[2] -710.862322029855
float(line[2].rstrip('
')) -710.862322029855
line[2] -1189.121616347144
float(line[2].rstrip('
')) -1189.121616347144
line[2] -339.8307842472289
float(line[2].rstrip('
')) -339.8307842472289
line[2] -734.1247792774911
float(line[2].rstrip('
')) -734.1247792774911
line[2] -1056.0794480718703
float(line[2].rstrip('
')) -1056.0794480718703
line[2] -1214.0678353454375
float(line[2].rstrip('
')) -1214.0678353454375
line[2] -2057.961788555722
float(line[2].rstrip('
')) -2057.961788555722
line[2] -509.92900869798234
float(line[2].rstrip('
')) -509.92900869798234
line[2] -1215.322945313571
float(line[2].rstrip('
')) -1215.322945313571
Extension ThermoDGCheckerExtension loaded
Dumping reactions/literals dict...
{'HK': 2, 'PGI': 3, 'PFK': 4, 'PGK': 5, 'PK': 6, 'LDH': 7, 'LDH_rev': 8, 'G6PDH': 9, 'EP': 10, 'TK': 11, 'PDH': 12, 'CS': 13, 'CITS': 14, 'AKGDH': 15, 'SCOAS': 16, 'SDH': 17, 'MDH': 18, 'ME': 19, 'GLNT': 20, 'GLNT_rev': 21, 'GLNS': 22, 'GLNS_rev': 23, 'GLDH': 24, 'ALATA': 25, 'GLUT': 26, 'RESP': 27, 'LEAK': 28, 'ATPASE': 29, 'AK': 30, 'AK_rev': 31, 'CK': 32, 'CK_rev': 33, 'PPRIBP': 34, 'NADPHOX': 35, 'GROWTH': 36}
Solving...

LP Solver output
(0.0, {'flux("GLNS_rev")': 0.0, 'flux("GLNT_rev")': 0.0, 'flux("GLNT")': 0.0, 'flux("PGK")': 0.02, 'flux("PPRIBP")': 0.0, 'flux("AK")': 0.0, 'flux("SDH")': 0.02, 'flux("CS")': 0.02, 'flux("LDH_rev")': 0.0, 'flux("HK")': 0.01, 'flux("EP")': 0.0, 'flux("AKGDH")': 0.02, 'flux("PDH")': 0.02, 'flux("SCOAS")': 0.02, 'flux("GROWTH")': 0.0, 'flux("CK_rev")': 0.0, 'flux("PFK")': 0.01, 'flux("CK")': 0.0, 'flux("ME")': 0.0, 'flux("PK")': 0.02, 'flux("CITS")': 0.02, 'flux("GLUT")': 0.0, 'flux("ATPASE")': 0.04, 'flux("RESP")': 0.0, 'flux("MDH")': 0.02, 'flux("PGI")': 0.01, 'flux("G6PDH")': 0.0, 'flux("GLNS")': 0.0, 'flux("AK_rev")': 0.0, 'flux("LEAK")': 0.0566, 'flux("TK")': 0.0, 'flux("GLDH")': 0.0, 'flux("LDH")': 0.0, 'flux("ALATA")': 0.0, 'flux("NADPHOX")': 0.0})
Answer: 1
support("HK") support("PGI") support("PFK") support("PGK") support("PK") support("PDH") support("CS") support("CITS") support("AKGDH") support("SCOAS") support("SDH") support("MDH") support("LEAK") support("ATPASE")

LP solver calls: 52   Time cplex :  0.009267091751098633



LP Solver output
(0.0, {'flux("GLNS_rev")': 0.0, 'flux("GLNT_rev")': 0.0, 'flux("GLNT")': 0.0, 'flux("PGK")': 0.0, 'flux("PPRIBP")': 0.0, 'flux("AK")': 0.0, 'flux("SDH")': 0.01, 'flux("CS")': 0.01, 'flux("LDH_rev")': 0.01, 'flux("HK")': 0.0, 'flux("EP")': 0.0, 'flux("AKGDH")': 0.01, 'flux("PDH")': 0.01, 'flux("SCOAS")': 0.01, 'flux("GROWTH")': 0.0, 'flux("CK_rev")': 0.0, 'flux("PFK")': 0.0, 'flux("CK")': 0.0, 'flux("ME")': 0.0, 'flux("PK")': 0.0, 'flux("CITS")': 0.01, 'flux("GLUT")': 0.0, 'flux("ATPASE")': 0.0666, 'flux("RESP")': 0.0283, 'flux("MDH")': 0.01, 'flux("PGI")': 0.0, 'flux("G6PDH")': 0.0, 'flux("GLNS")': 0.0, 'flux("AK_rev")': 0.0, 'flux("LEAK")': 0.0, 'flux("TK")': 0.0, 'flux("GLDH")': 0.0, 'flux("LDH")': 0.0, 'flux("ALATA")': 0.0, 'flux("NADPHOX")': 0.0})
Answer: 2
support("LDH_rev") support("PDH") support("CS") support("CITS") support("AKGDH") support("SCOAS") support("SDH") support("MDH") support("RESP") support("ATPASE")

LP solver calls: 61   Time cplex :  0.012056589126586914



LP Solver output
(0.0, {'flux("GLNS_rev")': 0.0, 'flux("GLNT_rev")': 0.0, 'flux("GLNT")': 0.0, 'flux("PGK")': 0.0, 'flux("PPRIBP")': 0.0, 'flux("AK")': 0.0, 'flux("SDH")': 0.01, 'flux("CS")': 0.01, 'flux("LDH_rev")': 0.01, 'flux("HK")': 0.0, 'flux("EP")': 0.0, 'flux("AKGDH")': 0.01, 'flux("PDH")': 0.01, 'flux("SCOAS")': 0.01, 'flux("GROWTH")': 0.0, 'flux("CK_rev")': 0.0, 'flux("PFK")': 0.0, 'flux("CK")': 0.0, 'flux("ME")': 0.0, 'flux("PK")': 0.0, 'flux("CITS")': 0.01, 'flux("GLUT")': 0.0, 'flux("ATPASE")': 0.01, 'flux("RESP")': 0.0, 'flux("MDH")': 0.01, 'flux("PGI")': 0.0, 'flux("G6PDH")': 0.0, 'flux("GLNS")': 0.0, 'flux("AK_rev")': 0.0, 'flux("LEAK")': 0.0283, 'flux("TK")': 0.0, 'flux("GLDH")': 0.0, 'flux("LDH")': 0.0, 'flux("ALATA")': 0.0, 'flux("NADPHOX")': 0.0})
Answer: 3
support("LDH_rev") support("PDH") support("CS") support("CITS") support("AKGDH") support("SCOAS") support("SDH") support("MDH") support("LEAK") support("ATPASE")

LP solver calls: 65   Time cplex :  0.012842178344726562



LP Solver output
(0.0, {'flux("GLNS_rev")': 0.0, 'flux("GLNT_rev")': 0.0, 'flux("GLNT")': 0.01, 'flux("PGK")': 0.0, 'flux("PPRIBP")': 0.0, 'flux("AK")': 0.0, 'flux("SDH")': 0.0, 'flux("CS")': 0.0, 'flux("LDH_rev")': 0.0, 'flux("HK")': 0.0, 'flux("EP")': 0.0, 'flux("AKGDH")': 0.0, 'flux("PDH")': 0.0, 'flux("SCOAS")': 0.0, 'flux("GROWTH")': 0.0, 'flux("CK_rev")': 0.0, 'flux("PFK")': 0.0, 'flux("CK")': 0.01, 'flux("ME")': 0.0, 'flux("PK")': 0.0, 'flux("CITS")': 0.0, 'flux("GLUT")': 0.01, 'flux("ATPASE")': 0.01, 'flux("RESP")': 0.0, 'flux("MDH")': 0.0, 'flux("PGI")': 0.0, 'flux("G6PDH")': 0.0, 'flux("GLNS")': 0.01, 'flux("AK_rev")': 0.0, 'flux("LEAK")': 0.0, 'flux("TK")': 0.0, 'flux("GLDH")': 0.0, 'flux("LDH")': 0.0, 'flux("ALATA")': 0.0, 'flux("NADPHOX")': 0.0})
Answer: 4
support("GLNT") support("GLNS") support("GLUT") support("ATPASE") support("CK")

LP solver calls: 72   Time cplex :  0.014226436614990234



LP Solver output
(0.0, {'flux("GLNS_rev")': 0.0, 'flux("GLNT_rev")': 0.0, 'flux("GLNT")': 0.0, 'flux("PGK")': 0.02, 'flux("PPRIBP")': 0.0, 'flux("AK")': 0.0, 'flux("SDH")': 0.0, 'flux("CS")': 0.0, 'flux("LDH_rev")': 0.0, 'flux("HK")': 0.01, 'flux("EP")': 0.0, 'flux("AKGDH")': 0.0, 'flux("PDH")': 0.0, 'flux("SCOAS")': 0.0, 'flux("GROWTH")': 0.0, 'flux("CK_rev")': 0.0, 'flux("PFK")': 0.01, 'flux("CK")': 0.0, 'flux("ME")': 0.0, 'flux("PK")': 0.02, 'flux("CITS")': 0.0, 'flux("GLUT")': 0.0, 'flux("ATPASE")': 0.02, 'flux("RESP")': 0.0, 'flux("MDH")': 0.0, 'flux("PGI")': 0.01, 'flux("G6PDH")': 0.0, 'flux("GLNS")': 0.0, 'flux("AK_rev")': 0.0, 'flux("LEAK")': 0.0, 'flux("TK")': 0.0, 'flux("GLDH")': 0.0, 'flux("LDH")': 0.02, 'flux("ALATA")': 0.0, 'flux("NADPHOX")': 0.0})
Answer: 5
support("HK") support("PGI") support("PFK") support("PGK") support("PK") support("LDH") support("ATPASE")

LP solver calls: 78   Time cplex :  0.01565861701965332



LP Solver output
(0.0, {'flux("GLNS_rev")': 0.0, 'flux("GLNT_rev")': 0.0, 'flux("GLNT")': 0.0, 'flux("PGK")': 0.05, 'flux("PPRIBP")': 0.0, 'flux("AK")': 0.0, 'flux("SDH")': 0.0, 'flux("CS")': 0.0, 'flux("LDH_rev")': 0.0, 'flux("HK")': 0.03, 'flux("EP")': 0.02, 'flux("AKGDH")': 0.0, 'flux("PDH")': 0.0, 'flux("SCOAS")': 0.0, 'flux("GROWTH")': 0.0, 'flux("CK_rev")': 0.0, 'flux("PFK")': 0.02, 'flux("CK")': 0.0, 'flux("ME")': 0.0, 'flux("PK")': 0.05, 'flux("CITS")': 0.0, 'flux("GLUT")': 0.0, 'flux("ATPASE")': 0.05, 'flux("RESP")': 0.0, 'flux("MDH")': 0.0, 'flux("PGI")': 0.0, 'flux("G6PDH")': 0.03, 'flux("GLNS")': 0.0, 'flux("AK_rev")': 0.0, 'flux("LEAK")': 0.0, 'flux("TK")': 0.01, 'flux("GLDH")': 0.0, 'flux("LDH")': 0.05, 'flux("ALATA")': 0.0, 'flux("NADPHOX")': 0.06})
Answer: 6
support("HK") support("PFK") support("PGK") support("PK") support("LDH") support("G6PDH") support("EP") support("TK") support("ATPASE") support("NADPHOX")

LP solver calls: 94   Time cplex :  0.018479585647583008



LP Solver output
(0.0, {'flux("GLNS_rev")': 0.0, 'flux("GLNT_rev")': 0.0, 'flux("GLNT")': 0.0, 'flux("PGK")': 0.02, 'flux("PPRIBP")': 0.0, 'flux("AK")': 0.0, 'flux("SDH")': 0.02, 'flux("CS")': 0.02, 'flux("LDH_rev")': 0.0, 'flux("HK")': 0.01, 'flux("EP")': 0.0, 'flux("AKGDH")': 0.02, 'flux("PDH")': 0.02, 'flux("SCOAS")': 0.02, 'flux("GROWTH")': 0.0, 'flux("CK_rev")': 0.0, 'flux("PFK")': 0.01, 'flux("CK")': 0.0, 'flux("ME")': 0.0, 'flux("PK")': 0.02, 'flux("CITS")': 0.02, 'flux("GLUT")': 0.0, 'flux("ATPASE")': 0.1532, 'flux("RESP")': 0.0566, 'flux("MDH")': 0.02, 'flux("PGI")': 0.01, 'flux("G6PDH")': 0.0, 'flux("GLNS")': 0.0, 'flux("AK_rev")': 0.0, 'flux("LEAK")': 0.0, 'flux("TK")': 0.0, 'flux("GLDH")': 0.0, 'flux("LDH")': 0.0, 'flux("ALATA")': 0.0, 'flux("NADPHOX")': 0.0})
Answer: 7
support("HK") support("PGI") support("PFK") support("PGK") support("PK") support("PDH") support("CS") support("CITS") support("AKGDH") support("SCOAS") support("SDH") support("MDH") support("RESP") support("ATPASE")

LP solver calls: 104   Time cplex :  0.020686864852905273



LP Solver output
(0.0, {'flux("GLNS_rev")': 0.0, 'flux("GLNT_rev")': 0.0, 'flux("GLNT")': 0.0, 'flux("PGK")': 0.05, 'flux("PPRIBP")': 0.0, 'flux("AK")': 0.0, 'flux("SDH")': 0.05, 'flux("CS")': 0.05, 'flux("LDH_rev")': 0.0, 'flux("HK")': 0.03, 'flux("EP")': 0.02, 'flux("AKGDH")': 0.05, 'flux("PDH")': 0.05, 'flux("SCOAS")': 0.05, 'flux("GROWTH")': 0.0, 'flux("CK_rev")': 0.0, 'flux("PFK")': 0.02, 'flux("CK")': 0.0, 'flux("ME")': 0.0, 'flux("PK")': 0.05, 'flux("CITS")': 0.05, 'flux("GLUT")': 0.0, 'flux("ATPASE")': 0.1, 'flux("RESP")': 0.0, 'flux("MDH")': 0.05, 'flux("PGI")': 0.0, 'flux("G6PDH")': 0.03, 'flux("GLNS")': 0.0, 'flux("AK_rev")': 0.0, 'flux("LEAK")': 0.1415, 'flux("TK")': 0.01, 'flux("GLDH")': 0.0, 'flux("LDH")': 0.0, 'flux("ALATA")': 0.0, 'flux("NADPHOX")': 0.06})
Answer: 8
support("HK") support("PFK") support("PGK") support("PK") support("G6PDH") support("EP") support("TK") support("PDH") support("CS") support("CITS") support("AKGDH") support("SCOAS") support("SDH") support("MDH") support("LEAK") support("ATPASE") support("NADPHOX")

LP solver calls: 112   Time cplex :  0.02237415313720703



LP Solver output
(0.0, {'flux("GLNS_rev")': 0.0, 'flux("GLNT_rev")': 0.0, 'flux("GLNT")': 0.0, 'flux("PGK")': 0.05, 'flux("PPRIBP")': 0.0, 'flux("AK")': 0.0, 'flux("SDH")': 0.05, 'flux("CS")': 0.05, 'flux("LDH_rev")': 0.0, 'flux("HK")': 0.03, 'flux("EP")': 0.02, 'flux("AKGDH")': 0.05, 'flux("PDH")': 0.05, 'flux("SCOAS")': 0.05, 'flux("GROWTH")': 0.0, 'flux("CK_rev")': 0.0, 'flux("PFK")': 0.02, 'flux("CK")': 0.0, 'flux("ME")': 0.0, 'flux("PK")': 0.05, 'flux("CITS")': 0.05, 'flux("GLUT")': 0.0, 'flux("ATPASE")': 0.383, 'flux("RESP")': 0.1415, 'flux("MDH")': 0.05, 'flux("PGI")': 0.0, 'flux("G6PDH")': 0.03, 'flux("GLNS")': 0.0, 'flux("AK_rev")': 0.0, 'flux("LEAK")': 0.0, 'flux("TK")': 0.01, 'flux("GLDH")': 0.0, 'flux("LDH")': 0.0, 'flux("ALATA")': 0.0, 'flux("NADPHOX")': 0.06})
Answer: 9
support("HK") support("PFK") support("PGK") support("PK") support("G6PDH") support("EP") support("TK") support("PDH") support("CS") support("CITS") support("AKGDH") support("SCOAS") support("SDH") support("MDH") support("RESP") support("ATPASE") support("NADPHOX")

LP solver calls: 117   Time cplex :  0.02328324317932129



LP Solver output
(0.0, {'flux("GLNS_rev")': 0.0, 'flux("GLNT_rev")': 0.0, 'flux("GLNT")': 0.01, 'flux("PGK")': 0.0, 'flux("PPRIBP")': 0.0, 'flux("AK")': 0.0, 'flux("SDH")': 0.01, 'flux("CS")': 0.0, 'flux("LDH_rev")': 0.0, 'flux("HK")': 0.0, 'flux("EP")': 0.0, 'flux("AKGDH")': 0.01, 'flux("PDH")': 0.0, 'flux("SCOAS")': 0.01, 'flux("GROWTH")': 0.0, 'flux("CK_rev")': 0.0, 'flux("PFK")': 0.0, 'flux("CK")': 0.01, 'flux("ME")': 0.01, 'flux("PK")': 0.0, 'flux("CITS")': 0.0, 'flux("GLUT")': 0.0, 'flux("ATPASE")': 0.01, 'flux("RESP")': 0.0, 'flux("MDH")': 0.0, 'flux("PGI")': 0.0, 'flux("G6PDH")': 0.0, 'flux("GLNS")': 0.01, 'flux("AK_rev")': 0.0, 'flux("LEAK")': 0.0133, 'flux("TK")': 0.0, 'flux("GLDH")': 0.0, 'flux("LDH")': 0.0, 'flux("ALATA")': 0.01, 'flux("NADPHOX")': 0.0})
Answer: 10
support("AKGDH") support("SCOAS") support("SDH") support("ME") support("GLNT") support("GLNS") support("ALATA") support("LEAK") support("ATPASE") support("CK")

LP solver calls: 129   Time cplex :  0.02582526206970215



LP Solver output
(0.0, {'flux("GLNS_rev")': 0.0, 'flux("GLNT_rev")': 0.0, 'flux("GLNT")': 0.01, 'flux("PGK")': 0.0, 'flux("PPRIBP")': 0.0, 'flux("AK")': 0.0, 'flux("SDH")': 0.01, 'flux("CS")': 0.0, 'flux("LDH_rev")': 0.0, 'flux("HK")': 0.0, 'flux("EP")': 0.0, 'flux("AKGDH")': 0.01, 'flux("PDH")': 0.0, 'flux("SCOAS")': 0.01, 'flux("GROWTH")': 0.0, 'flux("CK_rev")': 0.0, 'flux("PFK")': 0.0, 'flux("CK")': 0.01, 'flux("ME")': 0.01, 'flux("PK")': 0.0, 'flux("CITS")': 0.0, 'flux("GLUT")': 0.0, 'flux("ATPASE")': 0.0366, 'flux("RESP")': 0.0133, 'flux("MDH")': 0.0, 'flux("PGI")': 0.0, 'flux("G6PDH")': 0.0, 'flux("GLNS")': 0.01, 'flux("AK_rev")': 0.0, 'flux("LEAK")': 0.0, 'flux("TK")': 0.0, 'flux("GLDH")': 0.0, 'flux("LDH")': 0.0, 'flux("ALATA")': 0.01, 'flux("NADPHOX")': 0.0})
Answer: 11
support("AKGDH") support("SCOAS") support("SDH") support("ME") support("GLNT") support("GLNS") support("ALATA") support("RESP") support("ATPASE") support("CK")

LP solver calls: 134   Time cplex :  0.02676081657409668



LP Solver output
(0.0, {'flux("GLNS_rev")': 0.0, 'flux("GLNT_rev")': 0.0, 'flux("GLNT")': 0.01, 'flux("PGK")': 0.0, 'flux("PPRIBP")': 0.0, 'flux("AK")': 0.0, 'flux("SDH")': 0.02, 'flux("CS")': 0.01, 'flux("LDH_rev")': 0.0, 'flux("HK")': 0.0, 'flux("EP")': 0.0, 'flux("AKGDH")': 0.02, 'flux("PDH")': 0.01, 'flux("SCOAS")': 0.02, 'flux("GROWTH")': 0.0, 'flux("CK_rev")': 0.0, 'flux("PFK")': 0.0, 'flux("CK")': 0.01, 'flux("ME")': 0.01, 'flux("PK")': 0.0, 'flux("CITS")': 0.01, 'flux("GLUT")': 0.0, 'flux("ATPASE")': 0.1032, 'flux("RESP")': 0.0416, 'flux("MDH")': 0.01, 'flux("PGI")': 0.0, 'flux("G6PDH")': 0.0, 'flux("GLNS")': 0.01, 'flux("AK_rev")': 0.0, 'flux("LEAK")': 0.0, 'flux("TK")': 0.0, 'flux("GLDH")': 0.01, 'flux("LDH")': 0.0, 'flux("ALATA")': 0.0, 'flux("NADPHOX")': 0.0})
Answer: 12
support("PDH") support("CS") support("CITS") support("AKGDH") support("SCOAS") support("SDH") support("MDH") support("ME") support("GLNT") support("GLNS") support("GLDH") support("RESP") support("ATPASE") support("CK")

LP solver calls: 144   Time cplex :  0.028937578201293945



LP Solver output
(0.0, {'flux("GLNS_rev")': 0.0, 'flux("GLNT_rev")': 0.0, 'flux("GLNT")': 0.01, 'flux("PGK")': 0.0, 'flux("PPRIBP")': 0.0, 'flux("AK")': 0.0, 'flux("SDH")': 0.02, 'flux("CS")': 0.01, 'flux("LDH_rev")': 0.0, 'flux("HK")': 0.0, 'flux("EP")': 0.0, 'flux("AKGDH")': 0.02, 'flux("PDH")': 0.01, 'flux("SCOAS")': 0.02, 'flux("GROWTH")': 0.0, 'flux("CK_rev")': 0.0, 'flux("PFK")': 0.0, 'flux("CK")': 0.01, 'flux("ME")': 0.01, 'flux("PK")': 0.0, 'flux("CITS")': 0.01, 'flux("GLUT")': 0.0, 'flux("ATPASE")': 0.02, 'flux("RESP")': 0.0, 'flux("MDH")': 0.01, 'flux("PGI")': 0.0, 'flux("G6PDH")': 0.0, 'flux("GLNS")': 0.01, 'flux("AK_rev")': 0.0, 'flux("LEAK")': 0.0416, 'flux("TK")': 0.0, 'flux("GLDH")': 0.01, 'flux("LDH")': 0.0, 'flux("ALATA")': 0.0, 'flux("NADPHOX")': 0.0})
Answer: 13
support("PDH") support("CS") support("CITS") support("AKGDH") support("SCOAS") support("SDH") support("MDH") support("ME") support("GLNT") support("GLNS") support("GLDH") support("LEAK") support("ATPASE") support("CK")

LP solver calls: 147   Time cplex :  0.02947258949279785



LP Solver output
(0.0, {'flux("GLNS_rev")': 1.0310958904, 'flux("GLNT_rev")': 0.0, 'flux("GLNT")': 0.01, 'flux("PGK")': 0.0, 'flux("PPRIBP")': 0.5178082192, 'flux("AK")': 0.5178082192, 'flux("SDH")': 0.5260273973, 'flux("CS")': 0.7808219178, 'flux("LDH_rev")': 0.0, 'flux("HK")': 0.3179452055, 'flux("EP")': 0.0, 'flux("AKGDH")': 0.5260273973, 'flux("PDH")': 0.7808219178, 'flux("SCOAS")': 0.5260273973, 'flux("GROWTH")': 0.1369863014, 'flux("CK_rev")': 0.0, 'flux("PFK")': 0.0, 'flux("CK")': 2.3991780822, 'flux("ME")': 0.7808219178, 'flux("PK")': 0.0, 'flux("CITS")': 0.5260273973, 'flux("GLUT")': 0.0, 'flux("ATPASE")': 0.5260273973, 'flux("RESP")': 0.0, 'flux("MDH")': 0.7808219178, 'flux("PGI")': 0.0, 'flux("G6PDH")': 0.3139726027, 'flux("GLNS")': 0.0, 'flux("AK_rev")': 0.0, 'flux("LEAK")': 1.8708493151, 'flux("TK")': 0.0, 'flux("GLDH")': 0.0, 'flux("LDH")': 0.0, 'flux("ALATA")': 0.0, 'flux("NADPHOX")': 0.6279452055})
Answer: 14
support("HK") support("G6PDH") support("PDH") support("CS") support("CITS") support("AKGDH") support("SCOAS") support("SDH") support("MDH") support("ME") support("GLNT") support("GLNS_rev") support("LEAK") support("ATPASE") support("AK") support("CK") support("PPRIBP") support("NADPHOX") support("GROWTH")

LP solver calls: 163   Time cplex :  0.034172773361206055



LP Solver output
(0.0, {'flux("GLNS_rev")': 0.0, 'flux("GLNT_rev")': 0.0, 'flux("GLNT")': 0.076, 'flux("PGK")': 0.0, 'flux("PPRIBP")': 0.0378, 'flux("AK")': 0.0378, 'flux("SDH")': 0.0384, 'flux("CS")': 0.057, 'flux("LDH_rev")': 0.0, 'flux("HK")': 0.02321, 'flux("EP")': 0.0, 'flux("AKGDH")': 0.0384, 'flux("PDH")': 0.057, 'flux("SCOAS")': 0.0384, 'flux("GROWTH")': 0.01, 'flux("CK_rev")': 0.0, 'flux("PFK")': 0.0, 'flux("CK")': 0.25041, 'flux("ME")': 0.057, 'flux("PK")': 0.0, 'flux("CITS")': 0.0384, 'flux("GLUT")': 0.07527, 'flux("ATPASE")': 0.11367, 'flux("RESP")': 0.0, 'flux("MDH")': 0.057, 'flux("PGI")': 0.0, 'flux("G6PDH")': 0.02292, 'flux("GLNS")': 0.0, 'flux("AK_rev")': 0.0, 'flux("LEAK")': 0.136572, 'flux("TK")': 0.0, 'flux("GLDH")': 0.0, 'flux("LDH")': 0.0, 'flux("ALATA")': 0.0, 'flux("NADPHOX")': 0.04584})
Answer: 15
support("HK") support("G6PDH") support("PDH") support("CS") support("CITS") support("AKGDH") support("SCOAS") support("SDH") support("MDH") support("ME") support("GLNT") support("GLUT") support("LEAK") support("ATPASE") support("AK") support("CK") support("PPRIBP") support("NADPHOX") support("GROWTH")

LP solver calls: 166   Time cplex :  0.035080909729003906



LP Solver output
(0.0, {'flux("GLNS_rev")': 0.0, 'flux("GLNT_rev")': 0.0, 'flux("GLNT")': 0.076, 'flux("PGK")': 0.0, 'flux("PPRIBP")': 0.0378, 'flux("AK")': 0.0378, 'flux("SDH")': 0.18894, 'flux("CS")': 0.13227, 'flux("LDH_rev")': 0.0, 'flux("HK")': 0.02321, 'flux("EP")': 0.0, 'flux("AKGDH")': 0.18894, 'flux("PDH")': 0.13227, 'flux("SCOAS")': 0.18894, 'flux("GROWTH")': 0.01, 'flux("CK_rev")': 0.0, 'flux("PFK")': 0.0, 'flux("CK")': 0.25041, 'flux("ME")': 0.13227, 'flux("PK")': 0.0, 'flux("CITS")': 0.11367, 'flux("GLUT")': 0.0, 'flux("ATPASE")': 0.18894, 'flux("RESP")': 0.0, 'flux("MDH")': 0.13227, 'flux("PGI")': 0.0, 'flux("G6PDH")': 0.02292, 'flux("GLNS")': 0.0, 'flux("AK_rev")': 0.0, 'flux("LEAK")': 0.4496952, 'flux("TK")': 0.0, 'flux("GLDH")': 0.07527, 'flux("LDH")': 0.0, 'flux("ALATA")': 0.0, 'flux("NADPHOX")': 0.04584})
Answer: 16
support("HK") support("G6PDH") support("PDH") support("CS") support("CITS") support("AKGDH") support("SCOAS") support("SDH") support("MDH") support("ME") support("GLNT") support("GLDH") support("LEAK") support("ATPASE") support("AK") support("CK") support("PPRIBP") support("NADPHOX") support("GROWTH")

LP solver calls: 168   Time cplex :  0.03568434715270996



LP Solver output
(0.0, {'flux("GLNS_rev")': 1.0310958904, 'flux("GLNT_rev")': 0.0, 'flux("GLNT")': 0.01, 'flux("PGK")': 0.0, 'flux("PPRIBP")': 0.5178082192, 'flux("AK")': 0.5178082192, 'flux("SDH")': 0.5260273973, 'flux("CS")': 0.7808219178, 'flux("LDH_rev")': 0.0, 'flux("HK")': 0.3179452055, 'flux("EP")': 0.0, 'flux("AKGDH")': 0.5260273973, 'flux("PDH")': 0.7808219178, 'flux("SCOAS")': 0.5260273973, 'flux("GROWTH")': 0.1369863014, 'flux("CK_rev")': 0.0, 'flux("PFK")': 0.0, 'flux("CK")': 2.3991780822, 'flux("ME")': 0.7808219178, 'flux("PK")': 0.0, 'flux("CITS")': 0.5260273973, 'flux("GLUT")': 0.0, 'flux("ATPASE")': 4.2677260274, 'flux("RESP")': 1.8708493151, 'flux("MDH")': 0.7808219178, 'flux("PGI")': 0.0, 'flux("G6PDH")': 0.3139726027, 'flux("GLNS")': 0.0, 'flux("AK_rev")': 0.0, 'flux("LEAK")': 0.0, 'flux("TK")': 0.0, 'flux("GLDH")': 0.0, 'flux("LDH")': 0.0, 'flux("ALATA")': 0.0, 'flux("NADPHOX")': 0.6279452055})
Answer: 17
support("HK") support("G6PDH") support("PDH") support("CS") support("CITS") support("AKGDH") support("SCOAS") support("SDH") support("MDH") support("ME") support("GLNT") support("GLNS_rev") support("RESP") support("ATPASE") support("AK") support("CK") support("PPRIBP") support("NADPHOX") support("GROWTH")

LP solver calls: 173   Time cplex :  0.037110328674316406



LP Solver output
(0.0, {'flux("GLNS_rev")': 0.0, 'flux("GLNT_rev")': 0.0, 'flux("GLNT")': 0.076, 'flux("PGK")': 0.0, 'flux("PPRIBP")': 0.0378, 'flux("AK")': 0.0378, 'flux("SDH")': 0.0384, 'flux("CS")': 0.057, 'flux("LDH_rev")': 0.0, 'flux("HK")': 0.02321, 'flux("EP")': 0.0, 'flux("AKGDH")': 0.0384, 'flux("PDH")': 0.057, 'flux("SCOAS")': 0.0384, 'flux("GROWTH")': 0.01, 'flux("CK_rev")': 0.0, 'flux("PFK")': 0.0, 'flux("CK")': 0.25041, 'flux("ME")': 0.057, 'flux("PK")': 0.0, 'flux("CITS")': 0.0384, 'flux("GLUT")': 0.07527, 'flux("ATPASE")': 0.386814, 'flux("RESP")': 0.136572, 'flux("MDH")': 0.057, 'flux("PGI")': 0.0, 'flux("G6PDH")': 0.02292, 'flux("GLNS")': 0.0, 'flux("AK_rev")': 0.0, 'flux("LEAK")': 0.0, 'flux("TK")': 0.0, 'flux("GLDH")': 0.0, 'flux("LDH")': 0.0, 'flux("ALATA")': 0.0, 'flux("NADPHOX")': 0.04584})
Answer: 18
support("HK") support("G6PDH") support("PDH") support("CS") support("CITS") support("AKGDH") support("SCOAS") support("SDH") support("MDH") support("ME") support("GLNT") support("GLUT") support("RESP") support("ATPASE") support("AK") support("CK") support("PPRIBP") support("NADPHOX") support("GROWTH")

LP solver calls: 175   Time cplex :  0.037714481353759766



LP Solver output
(0.0, {'flux("GLNS_rev")': 0.0, 'flux("GLNT_rev")': 0.0, 'flux("GLNT")': 0.076, 'flux("PGK")': 0.0, 'flux("PPRIBP")': 0.0378, 'flux("AK")': 0.0378, 'flux("SDH")': 0.18894, 'flux("CS")': 0.13227, 'flux("LDH_rev")': 0.0, 'flux("HK")': 0.02321, 'flux("EP")': 0.0, 'flux("AKGDH")': 0.18894, 'flux("PDH")': 0.13227, 'flux("SCOAS")': 0.18894, 'flux("GROWTH")': 0.01, 'flux("CK_rev")': 0.0, 'flux("PFK")': 0.0, 'flux("CK")': 0.25041, 'flux("ME")': 0.13227, 'flux("PK")': 0.0, 'flux("CITS")': 0.11367, 'flux("GLUT")': 0.0, 'flux("ATPASE")': 1.0883304, 'flux("RESP")': 0.4496952, 'flux("MDH")': 0.13227, 'flux("PGI")': 0.0, 'flux("G6PDH")': 0.02292, 'flux("GLNS")': 0.0, 'flux("AK_rev")': 0.0, 'flux("LEAK")': 0.0, 'flux("TK")': 0.0, 'flux("GLDH")': 0.07527, 'flux("LDH")': 0.0, 'flux("ALATA")': 0.0, 'flux("NADPHOX")': 0.04584})
Answer: 19
support("HK") support("G6PDH") support("PDH") support("CS") support("CITS") support("AKGDH") support("SCOAS") support("SDH") support("MDH") support("ME") support("GLNT") support("GLDH") support("RESP") support("ATPASE") support("AK") support("CK") support("PPRIBP") support("NADPHOX") support("GROWTH")

LP solver calls: 176   Time cplex :  0.03803515434265137



LP Solver output
(0.0, {'flux("GLNS_rev")': 0.0, 'flux("GLNT_rev")': 0.0, 'flux("GLNT")': 0.076, 'flux("PGK")': 0.0, 'flux("PPRIBP")': 0.0378, 'flux("AK")': 0.0378, 'flux("SDH")': 0.11367, 'flux("CS")': 0.057, 'flux("LDH_rev")': 0.0, 'flux("HK")': 0.02321, 'flux("EP")': 0.0, 'flux("AKGDH")': 0.11367, 'flux("PDH")': 0.057, 'flux("SCOAS")': 0.11367, 'flux("GROWTH")': 0.01, 'flux("CK_rev")': 0.0, 'flux("PFK")': 0.0, 'flux("CK")': 0.25041, 'flux("ME")': 0.13227, 'flux("PK")': 0.0, 'flux("CITS")': 0.0384, 'flux("GLUT")': 0.0, 'flux("ATPASE")': 0.5870322, 'flux("RESP")': 0.2366811, 'flux("MDH")': 0.057, 'flux("PGI")': 0.0, 'flux("G6PDH")': 0.02292, 'flux("GLNS")': 0.0, 'flux("AK_rev")': 0.0, 'flux("LEAK")': 0.0, 'flux("TK")': 0.0, 'flux("GLDH")': 0.0, 'flux("LDH")': 0.0, 'flux("ALATA")': 0.07527, 'flux("NADPHOX")': 0.04584})
Answer: 20
support("HK") support("G6PDH") support("PDH") support("CS") support("CITS") support("AKGDH") support("SCOAS") support("SDH") support("MDH") support("ME") support("GLNT") support("ALATA") support("RESP") support("ATPASE") support("AK") support("CK") support("PPRIBP") support("NADPHOX") support("GROWTH")

LP solver calls: 182   Time cplex :  0.03989076614379883



LP Solver output
(0.0, {'flux("GLNS_rev")': 0.0, 'flux("GLNT_rev")': 0.0, 'flux("GLNT")': 0.076, 'flux("PGK")': 0.0, 'flux("PPRIBP")': 0.0378, 'flux("AK")': 0.0378, 'flux("SDH")': 0.11367, 'flux("CS")': 0.057, 'flux("LDH_rev")': 0.0, 'flux("HK")': 0.02321, 'flux("EP")': 0.0, 'flux("AKGDH")': 0.11367, 'flux("PDH")': 0.057, 'flux("SCOAS")': 0.11367, 'flux("GROWTH")': 0.01, 'flux("CK_rev")': 0.0, 'flux("PFK")': 0.0, 'flux("CK")': 0.25041, 'flux("ME")': 0.13227, 'flux("PK")': 0.0, 'flux("CITS")': 0.0384, 'flux("GLUT")': 0.0, 'flux("ATPASE")': 0.11367, 'flux("RESP")': 0.0, 'flux("MDH")': 0.057, 'flux("PGI")': 0.0, 'flux("G6PDH")': 0.02292, 'flux("GLNS")': 0.0, 'flux("AK_rev")': 0.0, 'flux("LEAK")': 0.2366811, 'flux("TK")': 0.0, 'flux("GLDH")': 0.0, 'flux("LDH")': 0.0, 'flux("ALATA")': 0.07527, 'flux("NADPHOX")': 0.04584})
Answer: 21
support("HK") support("G6PDH") support("PDH") support("CS") support("CITS") support("AKGDH") support("SCOAS") support("SDH") support("MDH") support("ME") support("GLNT") support("ALATA") support("LEAK") support("ATPASE") support("AK") support("CK") support("PPRIBP") support("NADPHOX") support("GROWTH")

LP solver calls: 183   Time cplex :  0.040207862854003906



LP Solver output
(0.0, {'flux("GLNS_rev")': 0.0, 'flux("GLNT_rev")': 0.0, 'flux("GLNT")': 0.01, 'flux("PGK")': 0.0, 'flux("PPRIBP")': 0.0, 'flux("AK")': 0.0, 'flux("SDH")': 0.01, 'flux("CS")': 0.0, 'flux("LDH_rev")': 0.0, 'flux("HK")': 0.0, 'flux("EP")': 0.0, 'flux("AKGDH")': 0.01, 'flux("PDH")': 0.0, 'flux("SCOAS")': 0.01, 'flux("GROWTH")': 0.0, 'flux("CK_rev")': 0.0, 'flux("PFK")': 0.0, 'flux("CK")': 0.01, 'flux("ME")': 0.01, 'flux("PK")': 0.0, 'flux("CITS")': 0.0, 'flux("GLUT")': 0.0, 'flux("ATPASE")': 0.0366, 'flux("RESP")': 0.0133, 'flux("MDH")': 0.0, 'flux("PGI")': 0.0, 'flux("G6PDH")': 0.0, 'flux("GLNS")': 0.01, 'flux("AK_rev")': 0.0, 'flux("LEAK")': 0.0, 'flux("TK")': 0.0, 'flux("GLDH")': 0.01, 'flux("LDH")': 0.01, 'flux("ALATA")': 0.0, 'flux("NADPHOX")': 0.0})
Answer: 22
support("LDH") support("AKGDH") support("SCOAS") support("SDH") support("ME") support("GLNT") support("GLNS") support("GLDH") support("RESP") support("ATPASE") support("CK")

LP solver calls: 193   Time cplex :  0.04271388053894043



LP Solver output
(0.0, {'flux("GLNS_rev")': 0.0, 'flux("GLNT_rev")': 0.0, 'flux("GLNT")': 0.01, 'flux("PGK")': 0.0, 'flux("PPRIBP")': 0.0, 'flux("AK")': 0.0, 'flux("SDH")': 0.01, 'flux("CS")': 0.0, 'flux("LDH_rev")': 0.0, 'flux("HK")': 0.0, 'flux("EP")': 0.0, 'flux("AKGDH")': 0.01, 'flux("PDH")': 0.0, 'flux("SCOAS")': 0.01, 'flux("GROWTH")': 0.0, 'flux("CK_rev")': 0.0, 'flux("PFK")': 0.0, 'flux("CK")': 0.01, 'flux("ME")': 0.01, 'flux("PK")': 0.0, 'flux("CITS")': 0.0, 'flux("GLUT")': 0.0, 'flux("ATPASE")': 0.01, 'flux("RESP")': 0.0, 'flux("MDH")': 0.0, 'flux("PGI")': 0.0, 'flux("G6PDH")': 0.0, 'flux("GLNS")': 0.01, 'flux("AK_rev")': 0.0, 'flux("LEAK")': 0.0133, 'flux("TK")': 0.0, 'flux("GLDH")': 0.01, 'flux("LDH")': 0.01, 'flux("ALATA")': 0.0, 'flux("NADPHOX")': 0.0})
Answer: 23
support("LDH") support("AKGDH") support("SCOAS") support("SDH") support("ME") support("GLNT") support("GLNS") support("GLDH") support("LEAK") support("ATPASE") support("CK")

LP solver calls: 195   Time cplex :  0.0430755615234375



LP Solver output
(0.0, {'flux("GLNS_rev")': 1.0310958904, 'flux("GLNT_rev")': 0.0, 'flux("GLNT")': 0.01, 'flux("PGK")': 0.0, 'flux("PPRIBP")': 0.5178082192, 'flux("AK")': 0.5178082192, 'flux("SDH")': 0.0, 'flux("CS")': 0.2547945205, 'flux("LDH_rev")': 0.0, 'flux("HK")': 0.3179452055, 'flux("EP")': 0.0, 'flux("AKGDH")': 0.0, 'flux("PDH")': 0.2547945205, 'flux("SCOAS")': 0.0, 'flux("GROWTH")': 0.1369863014, 'flux("CK_rev")': 0.0, 'flux("PFK")': 0.0, 'flux("CK")': 2.3991780822, 'flux("ME")': 0.7808219178, 'flux("PK")': 0.0, 'flux("CITS")': 0.0, 'flux("GLUT")': 0.0, 'flux("ATPASE")': 0.7643835616, 'flux("RESP")': 0.3821917808, 'flux("MDH")': 0.2547945205, 'flux("PGI")': 0.0, 'flux("G6PDH")': 0.3139726027, 'flux("GLNS")': 0.0, 'flux("AK_rev")': 0.0, 'flux("LEAK")': 0.0, 'flux("TK")': 0.0, 'flux("GLDH")': 0.0, 'flux("LDH")': 0.5260273973, 'flux("ALATA")': 0.0, 'flux("NADPHOX")': 0.6279452055})
Answer: 24
support("HK") support("LDH") support("G6PDH") support("PDH") support("CS") support("MDH") support("ME") support("GLNT") support("GLNS_rev") support("RESP") support("ATPASE") support("AK") support("CK") support("PPRIBP") support("NADPHOX") support("GROWTH")

LP solver calls: 202   Time cplex :  0.04511237144470215



LP Solver output
(0.0, {'flux("GLNS_rev")': 0.0, 'flux("GLNT_rev")': 0.0, 'flux("GLNT")': 0.076, 'flux("PGK")': 0.0, 'flux("PPRIBP")': 0.0378, 'flux("AK")': 0.0378, 'flux("SDH")': 0.0, 'flux("CS")': 0.0186, 'flux("LDH_rev")': 0.0, 'flux("HK")': 0.02321, 'flux("EP")': 0.0, 'flux("AKGDH")': 0.0, 'flux("PDH")': 0.0186, 'flux("SCOAS")': 0.0, 'flux("GROWTH")': 0.01, 'flux("CK_rev")': 0.0, 'flux("PFK")': 0.0, 'flux("CK")': 0.25041, 'flux("ME")': 0.057, 'flux("PK")': 0.0, 'flux("CITS")': 0.0, 'flux("GLUT")': 0.07527, 'flux("ATPASE")': 0.13107, 'flux("RESP")': 0.0279, 'flux("MDH")': 0.0186, 'flux("PGI")': 0.0, 'flux("G6PDH")': 0.02292, 'flux("GLNS")': 0.0, 'flux("AK_rev")': 0.0, 'flux("LEAK")': 0.0, 'flux("TK")': 0.0, 'flux("GLDH")': 0.0, 'flux("LDH")': 0.0384, 'flux("ALATA")': 0.0, 'flux("NADPHOX")': 0.04584})
Answer: 25
support("HK") support("LDH") support("G6PDH") support("PDH") support("CS") support("MDH") support("ME") support("GLNT") support("GLUT") support("RESP") support("ATPASE") support("AK") support("CK") support("PPRIBP") support("NADPHOX") support("GROWTH")

LP solver calls: 205   Time cplex :  0.046068668365478516



LP Solver output
(0.0, {'flux("GLNS_rev")': 1.0310958904, 'flux("GLNT_rev")': 0.0, 'flux("GLNT")': 0.01, 'flux("PGK")': 0.0, 'flux("PPRIBP")': 0.5178082192, 'flux("AK")': 0.5178082192, 'flux("SDH")': 0.0, 'flux("CS")': 0.2547945205, 'flux("LDH_rev")': 0.0, 'flux("HK")': 0.3179452055, 'flux("EP")': 0.0, 'flux("AKGDH")': 0.0, 'flux("PDH")': 0.2547945205, 'flux("SCOAS")': 0.0, 'flux("GROWTH")': 0.1369863014, 'flux("CK_rev")': 0.0, 'flux("PFK")': 0.0, 'flux("CK")': 2.3991780822, 'flux("ME")': 0.7808219178, 'flux("PK")': 0.0, 'flux("CITS")': 0.0, 'flux("GLUT")': 0.0, 'flux("ATPASE")': 0.0, 'flux("RESP")': 0.0, 'flux("MDH")': 0.2547945205, 'flux("PGI")': 0.0, 'flux("G6PDH")': 0.3139726027, 'flux("GLNS")': 0.0, 'flux("AK_rev")': 0.0, 'flux("LEAK")': 0.3821917808, 'flux("TK")': 0.0, 'flux("GLDH")': 0.0, 'flux("LDH")': 0.5260273973, 'flux("ALATA")': 0.0, 'flux("NADPHOX")': 0.6279452055})
Answer: 26
support("HK") support("LDH") support("G6PDH") support("PDH") support("CS") support("MDH") support("ME") support("GLNT") support("GLNS_rev") support("LEAK") support("AK") support("CK") support("PPRIBP") support("NADPHOX") support("GROWTH")

LP solver calls: 208   Time cplex :  0.04693007469177246



LP Solver output
(0.0, {'flux("GLNS_rev")': 0.0, 'flux("GLNT_rev")': 0.0, 'flux("GLNT")': 0.076, 'flux("PGK")': 0.0, 'flux("PPRIBP")': 0.0378, 'flux("AK")': 0.0378, 'flux("SDH")': 0.0, 'flux("CS")': 0.0186, 'flux("LDH_rev")': 0.0, 'flux("HK")': 0.02321, 'flux("EP")': 0.0, 'flux("AKGDH")': 0.0, 'flux("PDH")': 0.0186, 'flux("SCOAS")': 0.0, 'flux("GROWTH")': 0.01, 'flux("CK_rev")': 0.0, 'flux("PFK")': 0.0, 'flux("CK")': 0.25041, 'flux("ME")': 0.057, 'flux("PK")': 0.0, 'flux("CITS")': 0.0, 'flux("GLUT")': 0.07527, 'flux("ATPASE")': 0.07527, 'flux("RESP")': 0.0, 'flux("MDH")': 0.0186, 'flux("PGI")': 0.0, 'flux("G6PDH")': 0.02292, 'flux("GLNS")': 0.0, 'flux("AK_rev")': 0.0, 'flux("LEAK")': 0.0279, 'flux("TK")': 0.0, 'flux("GLDH")': 0.0, 'flux("LDH")': 0.0384, 'flux("ALATA")': 0.0, 'flux("NADPHOX")': 0.04584})
Answer: 27
support("HK") support("LDH") support("G6PDH") support("PDH") support("CS") support("MDH") support("ME") support("GLNT") support("GLUT") support("LEAK") support("ATPASE") support("AK") support("CK") support("PPRIBP") support("NADPHOX") support("GROWTH")

LP solver calls: 210   Time cplex :  0.047467947006225586



LP Solver output
(0.0, {'flux("GLNS_rev")': 0.0, 'flux("GLNT_rev")': 0.0, 'flux("GLNT")': 0.076, 'flux("PGK")': 0.0, 'flux("PPRIBP")': 0.0378, 'flux("AK")': 0.0378, 'flux("SDH")': 0.07527, 'flux("CS")': 0.0186, 'flux("LDH_rev")': 0.0, 'flux("HK")': 0.02321, 'flux("EP")': 0.0, 'flux("AKGDH")': 0.07527, 'flux("PDH")': 0.0186, 'flux("SCOAS")': 0.07527, 'flux("GROWTH")': 0.01, 'flux("CK_rev")': 0.0, 'flux("PFK")': 0.0, 'flux("CK")': 0.25041, 'flux("ME")': 0.13227, 'flux("PK")': 0.0, 'flux("CITS")': 0.0, 'flux("GLUT")': 0.0, 'flux("ATPASE")': 0.3312882, 'flux("RESP")': 0.1280091, 'flux("MDH")': 0.0186, 'flux("PGI")': 0.0, 'flux("G6PDH")': 0.02292, 'flux("GLNS")': 0.0, 'flux("AK_rev")': 0.0, 'flux("LEAK")': 0.0, 'flux("TK")': 0.0, 'flux("GLDH")': 0.0, 'flux("LDH")': 0.0384, 'flux("ALATA")': 0.07527, 'flux("NADPHOX")': 0.04584})
Answer: 28
support("HK") support("LDH") support("G6PDH") support("PDH") support("CS") support("AKGDH") support("SCOAS") support("SDH") support("MDH") support("ME") support("GLNT") support("ALATA") support("RESP") support("ATPASE") support("AK") support("CK") support("PPRIBP") support("NADPHOX") support("GROWTH")

LP solver calls: 213   Time cplex :  0.04840087890625



LP Solver output
(0.0, {'flux("GLNS_rev")': 0.0, 'flux("GLNT_rev")': 0.0, 'flux("GLNT")': 0.076, 'flux("PGK")': 0.0, 'flux("PPRIBP")': 0.0378, 'flux("AK")': 0.0378, 'flux("SDH")': 0.07527, 'flux("CS")': 0.0186, 'flux("LDH_rev")': 0.0, 'flux("HK")': 0.02321, 'flux("EP")': 0.0, 'flux("AKGDH")': 0.07527, 'flux("PDH")': 0.0186, 'flux("SCOAS")': 0.07527, 'flux("GROWTH")': 0.01, 'flux("CK_rev")': 0.0, 'flux("PFK")': 0.0, 'flux("CK")': 0.25041, 'flux("ME")': 0.13227, 'flux("PK")': 0.0, 'flux("CITS")': 0.0, 'flux("GLUT")': 0.0, 'flux("ATPASE")': 0.3312882, 'flux("RESP")': 0.1280091, 'flux("MDH")': 0.0186, 'flux("PGI")': 0.0, 'flux("G6PDH")': 0.02292, 'flux("GLNS")': 0.0, 'flux("AK_rev")': 0.0, 'flux("LEAK")': 0.0, 'flux("TK")': 0.0, 'flux("GLDH")': 0.07527, 'flux("LDH")': 0.11367, 'flux("ALATA")': 0.0, 'flux("NADPHOX")': 0.04584})
Answer: 29
support("HK") support("LDH") support("G6PDH") support("PDH") support("CS") support("AKGDH") support("SCOAS") support("SDH") support("MDH") support("ME") support("GLNT") support("GLDH") support("RESP") support("ATPASE") support("AK") support("CK") support("PPRIBP") support("NADPHOX") support("GROWTH")

LP solver calls: 214   Time cplex :  0.04892134666442871



LP Solver output
(0.0, {'flux("GLNS_rev")': 0.0, 'flux("GLNT_rev")': 0.0, 'flux("GLNT")': 0.076, 'flux("PGK")': 0.0, 'flux("PPRIBP")': 0.0378, 'flux("AK")': 0.0378, 'flux("SDH")': 0.07527, 'flux("CS")': 0.0186, 'flux("LDH_rev")': 0.0, 'flux("HK")': 0.02321, 'flux("EP")': 0.0, 'flux("AKGDH")': 0.07527, 'flux("PDH")': 0.0186, 'flux("SCOAS")': 0.07527, 'flux("GROWTH")': 0.01, 'flux("CK_rev")': 0.0, 'flux("PFK")': 0.0, 'flux("CK")': 0.25041, 'flux("ME")': 0.13227, 'flux("PK")': 0.0, 'flux("CITS")': 0.0, 'flux("GLUT")': 0.0, 'flux("ATPASE")': 0.07527, 'flux("RESP")': 0.0, 'flux("MDH")': 0.0186, 'flux("PGI")': 0.0, 'flux("G6PDH")': 0.02292, 'flux("GLNS")': 0.0, 'flux("AK_rev")': 0.0, 'flux("LEAK")': 0.1280091, 'flux("TK")': 0.0, 'flux("GLDH")': 0.0, 'flux("LDH")': 0.0384, 'flux("ALATA")': 0.07527, 'flux("NADPHOX")': 0.04584})
Answer: 30
support("HK") support("LDH") support("G6PDH") support("PDH") support("CS") support("AKGDH") support("SCOAS") support("SDH") support("MDH") support("ME") support("GLNT") support("ALATA") support("LEAK") support("ATPASE") support("AK") support("CK") support("PPRIBP") support("NADPHOX") support("GROWTH")

LP solver calls: 216   Time cplex :  0.04971790313720703



LP Solver output
(0.0, {'flux("GLNS_rev")': 0.0, 'flux("GLNT_rev")': 0.0, 'flux("GLNT")': 0.076, 'flux("PGK")': 0.0, 'flux("PPRIBP")': 0.0378, 'flux("AK")': 0.0378, 'flux("SDH")': 0.07527, 'flux("CS")': 0.0186, 'flux("LDH_rev")': 0.0, 'flux("HK")': 0.02321, 'flux("EP")': 0.0, 'flux("AKGDH")': 0.07527, 'flux("PDH")': 0.0186, 'flux("SCOAS")': 0.07527, 'flux("GROWTH")': 0.01, 'flux("CK_rev")': 0.0, 'flux("PFK")': 0.0, 'flux("CK")': 0.25041, 'flux("ME")': 0.13227, 'flux("PK")': 0.0, 'flux("CITS")': 0.0, 'flux("GLUT")': 0.0, 'flux("ATPASE")': 0.07527, 'flux("RESP")': 0.0, 'flux("MDH")': 0.0186, 'flux("PGI")': 0.0, 'flux("G6PDH")': 0.02292, 'flux("GLNS")': 0.0, 'flux("AK_rev")': 0.0, 'flux("LEAK")': 0.1280091, 'flux("TK")': 0.0, 'flux("GLDH")': 0.07527, 'flux("LDH")': 0.11367, 'flux("ALATA")': 0.0, 'flux("NADPHOX")': 0.04584})
Answer: 31
support("HK") support("LDH") support("G6PDH") support("PDH") support("CS") support("AKGDH") support("SCOAS") support("SDH") support("MDH") support("ME") support("GLNT") support("GLDH") support("LEAK") support("ATPASE") support("AK") support("CK") support("PPRIBP") support("NADPHOX") support("GROWTH")
SATISFIABLE

Models       : 31
Calls        : 1
Time         : 1.872s (Solving: 1.03s 1st Model: 0.33s Unsat: 0.00s)
CPU Time     : 4.169s

Choices      : 624      (Domain: 624)
Conflicts    : 148      (Analyzed: 147)
Restarts     : 2        (Average: 73.50 Last: 136)
Model-Level  : 8.6     
Problems     : 1        (Average Length: 1.00 Splits: 0)
Lemmas       : 147      (Deleted: 0)
  Binary     : 40       (Ratio:  27.21%)
  Ternary    : 24       (Ratio:  16.33%)
  Conflict   : 147      (Average Length:    4.9 Ratio: 100.00%) 
  Loop       : 0        (Average Length:    0.0 Ratio:   0.00%) 
  Other      : 0        (Average Length:    0.0 Ratio:   0.00%) 
Backjumps    : 147      (Average:  3.29 Max:  21 Sum:    483)
  Executed   : 146      (Average:  3.28 Max:  21 Sum:    482 Ratio:  99.79%)
  Bounded    : 1        (Average:  1.00 Max:   1 Sum:      1 Ratio:   0.21%)

Rules        : 372     
  Heuristic  : 35      
Atoms        : 361     
Bodies       : 42       (Original: 41)
Equivalences : 105      (Atom=Atom: 35 Body=Body: 0 Other: 70)
Tight        : Yes
Variables    : 35       (Eliminated:    0 Frozen:   35)
Constraints  : 6        (Binary:  83.3% Ternary:   0.0% Other:  16.7%)


LP solver calls: 217   Time cplex :  0.05024266242980957


Added nogoods for 0 out of 31 partial solutions checked
Total time used by ThermoDGChecker: 0.12954 s
