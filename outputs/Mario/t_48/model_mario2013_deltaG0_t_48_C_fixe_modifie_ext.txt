clingo version 5.6.2
Reading from clingoLP.py ...
Je passe ICI
Début de la thermo
line[2] -1781.6444081489221
float(line[2].rstrip('
')) -1781.6444081489221
line[2] -1395.6292467273274
float(line[2].rstrip('
')) -1395.6292467273274
line[2] -622.1619221256469
float(line[2].rstrip('
')) -622.1619221256469
line[2] -521.0449892833246
float(line[2].rstrip('
')) -521.0449892833246
line[2] -2270.4800916865584
float(line[2].rstrip('
')) -2270.4800916865584
line[2] -954.8038650821667
float(line[2].rstrip('
')) -954.8038650821667
line[2] -386.0000000000019
float(line[2].rstrip('
')) -386.0000000000019
line[2] -1727.9787463436437
float(line[2].rstrip('
')) -1727.9787463436437
line[2] 277.8738483657367
float(line[2].rstrip('
')) 277.8738483657367
line[2] -66.99962570890881
float(line[2].rstrip('
')) -66.99962570890881
line[2] -440.7037923253865
float(line[2].rstrip('
')) -440.7037923253865
line[2] -244.1034774178704
float(line[2].rstrip('
')) -244.1034774178704
line[2] -93.47316959760116
float(line[2].rstrip('
')) -93.47316959760116
line[2] -349.1667977317067
float(line[2].rstrip('
')) -349.1667977317067
line[2] -298.5015020229598
float(line[2].rstrip('
')) -298.5015020229598
line[2] 89.29312212928599
float(line[2].rstrip('
')) 89.29312212928599
line[2] -1291.2323086083798
float(line[2].rstrip('
')) -1291.2323086083798
line[2] -1293.8755834729386
float(line[2].rstrip('
')) -1293.8755834729386
line[2] -1077.3005052258698
float(line[2].rstrip('
')) -1077.3005052258698
line[2] -93.47316959760116
float(line[2].rstrip('
')) -93.47316959760116
line[2] -349.1667977317067
float(line[2].rstrip('
')) -349.1667977317067
line[2] -672.3277467814481
float(line[2].rstrip('
')) -672.3277467814481
line[2] -1143.7174255495095
float(line[2].rstrip('
')) -1143.7174255495095
line[2] -1078.084459875474
float(line[2].rstrip('
')) -1078.084459875474
line[2] -2030.775868056622
float(line[2].rstrip('
')) -2030.775868056622
line[2] -1078.084459875474
float(line[2].rstrip('
')) -1078.084459875474
line[2] 16.399999999865035
float(line[2].rstrip('
')) 16.399999999865035
line[2] -710.862322029855
float(line[2].rstrip('
')) -710.862322029855
line[2] -1189.121616347144
float(line[2].rstrip('
')) -1189.121616347144
line[2] -339.8307842472289
float(line[2].rstrip('
')) -339.8307842472289
line[2] -734.1247792774911
float(line[2].rstrip('
')) -734.1247792774911
line[2] -1056.0794480718703
float(line[2].rstrip('
')) -1056.0794480718703
line[2] -1214.0678353454375
float(line[2].rstrip('
')) -1214.0678353454375
line[2] -2057.961788555722
float(line[2].rstrip('
')) -2057.961788555722
line[2] -509.92900869798234
float(line[2].rstrip('
')) -509.92900869798234
line[2] -1215.322945313571
float(line[2].rstrip('
')) -1215.322945313571
Extension ThermoDGCheckerExtension loaded
Dumping reactions/literals dict...
{'HK': 2, 'PGI': 3, 'PFK': 4, 'PGK': 5, 'PK': 6, 'LDH': 7, 'LDH_rev': 8, 'G6PDH': 9, 'EP': 10, 'TK': 11, 'PDH': 12, 'CS': 13, 'CITS': 14, 'AKGDH': 15, 'SCOAS': 16, 'SDH': 17, 'MDH': 18, 'ME': 19, 'GLNT': 20, 'GLNT_rev': 21, 'GLNS': 22, 'GLNS_rev': 23, 'GLDH': 24, 'ALATA': 25, 'GLUT': 26, 'RESP': 27, 'LEAK': 28, 'ATPASE': 29, 'AK': 30, 'AK_rev': 31, 'CK': 32, 'CK_rev': 33, 'PPRIBP': 34, 'NADPHOX': 35, 'GROWTH': 36}
Solving...
lp support ['MDH', 'SCOAS', 'LEAK', 'CITS', 'PGI', 'AKGDH', 'PK', 'PFK', 'CS', 'HK', 'ATPASE', 'PGK', 'PDH', 'SDH']
lits for clause [18, 16, 28, 14, 3, 15, 6, 4, 13, 2, 29, 5, 12, 17]

LP Solver output
(0.0, {'flux("MDH")': 0.01, 'flux("G6PDH")': 0.0, 'flux("CK_rev")': 0.0, 'flux("SCOAS")': 0.01, 'flux("GLDH")': 0.0, 'flux("ALATA")': 0.0, 'flux("LDH_rev")': 0.01, 'flux("LEAK")': 0.0, 'flux("CITS")': 0.01, 'flux("PGI")': 0.0, 'flux("AKGDH")': 0.01, 'flux("GLNS_rev")': 0.0, 'flux("PK")': 0.0, 'flux("AK")': 0.0, 'flux("PFK")': 0.0, 'flux("CS")': 0.01, 'flux("HK")': 0.0, 'flux("PPRIBP")': 0.0, 'flux("GROWTH")': 0.0, 'flux("LDH")': 0.0, 'flux("ATPASE")': 0.0666, 'flux("NADPHOX")': 0.0, 'flux("AK_rev")': 0.0, 'flux("GLNS")': 0.0, 'flux("CK")': 0.0, 'flux("ME")': 0.0, 'flux("GLUT")': 0.0, 'flux("TK")': 0.0, 'flux("PGK")': 0.0, 'flux("EP")': 0.0, 'flux("GLNT_rev")': 0.0, 'flux("PDH")': 0.01, 'flux("GLNT")': 0.0, 'flux("SDH")': 0.01, 'flux("RESP")': 0.0283})
Answer: 1
support("LDH_rev") support("PDH") support("CS") support("CITS") support("AKGDH") support("SCOAS") support("SDH") support("MDH") support("RESP") support("ATPASE")

LP solver calls: 61   Time cplex :  0.01111459732055664



LP Solver output
(0.0, {'flux("MDH")': 0.01, 'flux("G6PDH")': 0.0, 'flux("CK_rev")': 0.0, 'flux("SCOAS")': 0.01, 'flux("GLDH")': 0.0, 'flux("ALATA")': 0.0, 'flux("LDH_rev")': 0.01, 'flux("LEAK")': 0.0283, 'flux("CITS")': 0.01, 'flux("PGI")': 0.0, 'flux("AKGDH")': 0.01, 'flux("GLNS_rev")': 0.0, 'flux("PK")': 0.0, 'flux("AK")': 0.0, 'flux("PFK")': 0.0, 'flux("CS")': 0.01, 'flux("HK")': 0.0, 'flux("PPRIBP")': 0.0, 'flux("GROWTH")': 0.0, 'flux("LDH")': 0.0, 'flux("ATPASE")': 0.01, 'flux("NADPHOX")': 0.0, 'flux("AK_rev")': 0.0, 'flux("GLNS")': 0.0, 'flux("CK")': 0.0, 'flux("ME")': 0.0, 'flux("GLUT")': 0.0, 'flux("TK")': 0.0, 'flux("PGK")': 0.0, 'flux("EP")': 0.0, 'flux("GLNT_rev")': 0.0, 'flux("PDH")': 0.01, 'flux("GLNT")': 0.0, 'flux("SDH")': 0.01, 'flux("RESP")': 0.0})
Answer: 2
support("LDH_rev") support("PDH") support("CS") support("CITS") support("AKGDH") support("SCOAS") support("SDH") support("MDH") support("LEAK") support("ATPASE")

LP solver calls: 65   Time cplex :  0.011916399002075195



LP Solver output
(0.0, {'flux("MDH")': 0.0, 'flux("G6PDH")': 0.0, 'flux("CK_rev")': 0.0, 'flux("SCOAS")': 0.0, 'flux("GLDH")': 0.0, 'flux("ALATA")': 0.0, 'flux("LDH_rev")': 0.0, 'flux("LEAK")': 0.0, 'flux("CITS")': 0.0, 'flux("PGI")': 0.0, 'flux("AKGDH")': 0.0, 'flux("GLNS_rev")': 0.0, 'flux("PK")': 0.0, 'flux("AK")': 0.0, 'flux("PFK")': 0.0, 'flux("CS")': 0.0, 'flux("HK")': 0.0, 'flux("PPRIBP")': 0.0, 'flux("GROWTH")': 0.0, 'flux("LDH")': 0.0, 'flux("ATPASE")': 0.01, 'flux("NADPHOX")': 0.0, 'flux("AK_rev")': 0.0, 'flux("GLNS")': 0.01, 'flux("CK")': 0.01, 'flux("ME")': 0.0, 'flux("GLUT")': 0.01, 'flux("TK")': 0.0, 'flux("PGK")': 0.0, 'flux("EP")': 0.0, 'flux("GLNT_rev")': 0.0, 'flux("PDH")': 0.0, 'flux("GLNT")': 0.01, 'flux("SDH")': 0.0, 'flux("RESP")': 0.0})
Answer: 3
support("GLNT") support("GLNS") support("GLUT") support("ATPASE") support("CK")

LP solver calls: 72   Time cplex :  0.01318359375


lp support ['PGI', 'PK', 'PFK', 'HK', 'LDH', 'ATPASE', 'PGK']
lits for clause [3, 6, 4, 2, 7, 29, 5]
lp support ['G6PDH', 'PK', 'PFK', 'HK', 'LDH', 'ATPASE', 'NADPHOX', 'TK', 'PGK', 'EP']
lits for clause [9, 6, 4, 2, 7, 29, 35, 11, 5, 10]
lp support ['MDH', 'SCOAS', 'CITS', 'PGI', 'AKGDH', 'PK', 'PFK', 'CS', 'HK', 'ATPASE', 'PGK', 'PDH', 'SDH', 'RESP']
lits for clause [18, 16, 14, 3, 15, 6, 4, 13, 2, 29, 5, 12, 17, 27]
lp support ['MDH', 'G6PDH', 'SCOAS', 'LEAK', 'CITS', 'AKGDH', 'PK', 'PFK', 'CS', 'HK', 'ATPASE', 'NADPHOX', 'TK', 'PGK', 'EP', 'PDH', 'SDH']
lits for clause [18, 9, 16, 28, 14, 15, 6, 4, 13, 2, 29, 35, 11, 5, 10, 12, 17]
lp support ['MDH', 'G6PDH', 'SCOAS', 'CITS', 'AKGDH', 'PK', 'PFK', 'CS', 'HK', 'ATPASE', 'NADPHOX', 'TK', 'PGK', 'EP', 'PDH', 'SDH', 'RESP']
lits for clause [18, 9, 16, 14, 15, 6, 4, 13, 2, 29, 35, 11, 5, 10, 12, 17, 27]

LP Solver output
(0.0, {'flux("MDH")': 0.0, 'flux("G6PDH")': 0.0, 'flux("CK_rev")': 0.0, 'flux("SCOAS")': 0.01, 'flux("GLDH")': 0.0, 'flux("ALATA")': 0.01, 'flux("LDH_rev")': 0.0, 'flux("LEAK")': 0.0133, 'flux("CITS")': 0.0, 'flux("PGI")': 0.0, 'flux("AKGDH")': 0.01, 'flux("GLNS_rev")': 0.0, 'flux("PK")': 0.0, 'flux("AK")': 0.0, 'flux("PFK")': 0.0, 'flux("CS")': 0.0, 'flux("HK")': 0.0, 'flux("PPRIBP")': 0.0, 'flux("GROWTH")': 0.0, 'flux("LDH")': 0.0, 'flux("ATPASE")': 0.01, 'flux("NADPHOX")': 0.0, 'flux("AK_rev")': 0.0, 'flux("GLNS")': 0.01, 'flux("CK")': 0.01, 'flux("ME")': 0.01, 'flux("GLUT")': 0.0, 'flux("TK")': 0.0, 'flux("PGK")': 0.0, 'flux("EP")': 0.0, 'flux("GLNT_rev")': 0.0, 'flux("PDH")': 0.0, 'flux("GLNT")': 0.01, 'flux("SDH")': 0.01, 'flux("RESP")': 0.0})
Answer: 4
support("AKGDH") support("SCOAS") support("SDH") support("ME") support("GLNT") support("GLNS") support("ALATA") support("LEAK") support("ATPASE") support("CK")

LP solver calls: 128   Time cplex :  0.025498628616333008



LP Solver output
(0.0, {'flux("MDH")': 0.0, 'flux("G6PDH")': 0.0, 'flux("CK_rev")': 0.0, 'flux("SCOAS")': 0.01, 'flux("GLDH")': 0.0, 'flux("ALATA")': 0.01, 'flux("LDH_rev")': 0.0, 'flux("LEAK")': 0.0, 'flux("CITS")': 0.0, 'flux("PGI")': 0.0, 'flux("AKGDH")': 0.01, 'flux("GLNS_rev")': 0.0, 'flux("PK")': 0.0, 'flux("AK")': 0.0, 'flux("PFK")': 0.0, 'flux("CS")': 0.0, 'flux("HK")': 0.0, 'flux("PPRIBP")': 0.0, 'flux("GROWTH")': 0.0, 'flux("LDH")': 0.0, 'flux("ATPASE")': 0.0366, 'flux("NADPHOX")': 0.0, 'flux("AK_rev")': 0.0, 'flux("GLNS")': 0.01, 'flux("CK")': 0.01, 'flux("ME")': 0.01, 'flux("GLUT")': 0.0, 'flux("TK")': 0.0, 'flux("PGK")': 0.0, 'flux("EP")': 0.0, 'flux("GLNT_rev")': 0.0, 'flux("PDH")': 0.0, 'flux("GLNT")': 0.01, 'flux("SDH")': 0.01, 'flux("RESP")': 0.0133})
Answer: 5
support("AKGDH") support("SCOAS") support("SDH") support("ME") support("GLNT") support("GLNS") support("ALATA") support("RESP") support("ATPASE") support("CK")

LP solver calls: 134   Time cplex :  0.026658058166503906


lp support ['MDH', 'G6PDH', 'SCOAS', 'GLDH', 'CITS', 'AKGDH', 'AK', 'CS', 'HK', 'PPRIBP', 'GROWTH', 'ATPASE', 'NADPHOX', 'CK', 'ME', 'PDH', 'GLNT', 'SDH', 'RESP']
lits for clause [18, 9, 16, 24, 14, 15, 30, 13, 2, 34, 36, 29, 35, 32, 19, 12, 20, 17, 27]
lp support ['MDH', 'G6PDH', 'SCOAS', 'CITS', 'AKGDH', 'AK', 'CS', 'HK', 'PPRIBP', 'GROWTH', 'ATPASE', 'NADPHOX', 'CK', 'ME', 'GLUT', 'PDH', 'GLNT', 'SDH', 'RESP']
lits for clause [18, 9, 16, 14, 15, 30, 13, 2, 34, 36, 29, 35, 32, 19, 26, 12, 20, 17, 27]

LP Solver output
(0.0, {'flux("MDH")': 0.01, 'flux("G6PDH")': 0.0, 'flux("CK_rev")': 0.0, 'flux("SCOAS")': 0.02, 'flux("GLDH")': 0.01, 'flux("ALATA")': 0.0, 'flux("LDH_rev")': 0.0, 'flux("LEAK")': 0.0, 'flux("CITS")': 0.01, 'flux("PGI")': 0.0, 'flux("AKGDH")': 0.02, 'flux("GLNS_rev")': 0.0, 'flux("PK")': 0.0, 'flux("AK")': 0.0, 'flux("PFK")': 0.0, 'flux("CS")': 0.01, 'flux("HK")': 0.0, 'flux("PPRIBP")': 0.0, 'flux("GROWTH")': 0.0, 'flux("LDH")': 0.0, 'flux("ATPASE")': 0.1032, 'flux("NADPHOX")': 0.0, 'flux("AK_rev")': 0.0, 'flux("GLNS")': 0.01, 'flux("CK")': 0.01, 'flux("ME")': 0.01, 'flux("GLUT")': 0.0, 'flux("TK")': 0.0, 'flux("PGK")': 0.0, 'flux("EP")': 0.0, 'flux("GLNT_rev")': 0.0, 'flux("PDH")': 0.01, 'flux("GLNT")': 0.01, 'flux("SDH")': 0.02, 'flux("RESP")': 0.0416})
Answer: 6
support("PDH") support("CS") support("CITS") support("AKGDH") support("SCOAS") support("SDH") support("MDH") support("ME") support("GLNT") support("GLNS") support("GLDH") support("RESP") support("ATPASE") support("CK")

LP solver calls: 158   Time cplex :  0.033008575439453125



LP Solver output
(0.0, {'flux("MDH")': 0.01, 'flux("G6PDH")': 0.0, 'flux("CK_rev")': 0.0, 'flux("SCOAS")': 0.02, 'flux("GLDH")': 0.01, 'flux("ALATA")': 0.0, 'flux("LDH_rev")': 0.0, 'flux("LEAK")': 0.0416, 'flux("CITS")': 0.01, 'flux("PGI")': 0.0, 'flux("AKGDH")': 0.02, 'flux("GLNS_rev")': 0.0, 'flux("PK")': 0.0, 'flux("AK")': 0.0, 'flux("PFK")': 0.0, 'flux("CS")': 0.01, 'flux("HK")': 0.0, 'flux("PPRIBP")': 0.0, 'flux("GROWTH")': 0.0, 'flux("LDH")': 0.0, 'flux("ATPASE")': 0.02, 'flux("NADPHOX")': 0.0, 'flux("AK_rev")': 0.0, 'flux("GLNS")': 0.01, 'flux("CK")': 0.01, 'flux("ME")': 0.01, 'flux("GLUT")': 0.0, 'flux("TK")': 0.0, 'flux("PGK")': 0.0, 'flux("EP")': 0.0, 'flux("GLNT_rev")': 0.0, 'flux("PDH")': 0.01, 'flux("GLNT")': 0.01, 'flux("SDH")': 0.02, 'flux("RESP")': 0.0})
Answer: 7
support("PDH") support("CS") support("CITS") support("AKGDH") support("SCOAS") support("SDH") support("MDH") support("ME") support("GLNT") support("GLNS") support("GLDH") support("LEAK") support("ATPASE") support("CK")

LP solver calls: 160   Time cplex :  0.03352785110473633


lp support ['MDH', 'G6PDH', 'SCOAS', 'CITS', 'AKGDH', 'GLNS_rev', 'AK', 'CS', 'HK', 'PPRIBP', 'GROWTH', 'ATPASE', 'NADPHOX', 'CK', 'ME', 'PDH', 'GLNT', 'SDH', 'RESP']
lits for clause [18, 9, 16, 14, 15, 23, 30, 13, 2, 34, 36, 29, 35, 32, 19, 12, 20, 17, 27]
lp support ['MDH', 'G6PDH', 'SCOAS', 'LEAK', 'CITS', 'AKGDH', 'AK', 'CS', 'HK', 'PPRIBP', 'GROWTH', 'ATPASE', 'NADPHOX', 'CK', 'ME', 'GLUT', 'PDH', 'GLNT', 'SDH']
lits for clause [18, 9, 16, 28, 14, 15, 30, 13, 2, 34, 36, 29, 35, 32, 19, 26, 12, 20, 17]
lp support ['MDH', 'G6PDH', 'SCOAS', 'LEAK', 'CITS', 'AKGDH', 'GLNS_rev', 'AK', 'CS', 'HK', 'PPRIBP', 'GROWTH', 'ATPASE', 'NADPHOX', 'CK', 'ME', 'PDH', 'GLNT', 'SDH']
lits for clause [18, 9, 16, 28, 14, 15, 23, 30, 13, 2, 34, 36, 29, 35, 32, 19, 12, 20, 17]
lp support ['MDH', 'G6PDH', 'SCOAS', 'GLDH', 'LEAK', 'CITS', 'AKGDH', 'AK', 'CS', 'HK', 'PPRIBP', 'GROWTH', 'ATPASE', 'NADPHOX', 'CK', 'ME', 'PDH', 'GLNT', 'SDH']
lits for clause [18, 9, 16, 24, 28, 14, 15, 30, 13, 2, 34, 36, 29, 35, 32, 19, 12, 20, 17]
lp support ['MDH', 'G6PDH', 'SCOAS', 'ALATA', 'CITS', 'AKGDH', 'AK', 'CS', 'HK', 'PPRIBP', 'GROWTH', 'ATPASE', 'NADPHOX', 'CK', 'ME', 'PDH', 'GLNT', 'SDH', 'RESP']
lits for clause [18, 9, 16, 25, 14, 15, 30, 13, 2, 34, 36, 29, 35, 32, 19, 12, 20, 17, 27]
lp support ['MDH', 'G6PDH', 'SCOAS', 'ALATA', 'LEAK', 'CITS', 'AKGDH', 'AK', 'CS', 'HK', 'PPRIBP', 'GROWTH', 'ATPASE', 'NADPHOX', 'CK', 'ME', 'PDH', 'GLNT', 'SDH']
lits for clause [18, 9, 16, 25, 28, 14, 15, 30, 13, 2, 34, 36, 29, 35, 32, 19, 12, 20, 17]

LP Solver output
(0.0, {'flux("MDH")': 0.0, 'flux("G6PDH")': 0.0, 'flux("CK_rev")': 0.0, 'flux("SCOAS")': 0.01, 'flux("GLDH")': 0.01, 'flux("ALATA")': 0.0, 'flux("LDH_rev")': 0.0, 'flux("LEAK")': 0.0, 'flux("CITS")': 0.0, 'flux("PGI")': 0.0, 'flux("AKGDH")': 0.01, 'flux("GLNS_rev")': 0.0, 'flux("PK")': 0.0, 'flux("AK")': 0.0, 'flux("PFK")': 0.0, 'flux("CS")': 0.0, 'flux("HK")': 0.0, 'flux("PPRIBP")': 0.0, 'flux("GROWTH")': 0.0, 'flux("LDH")': 0.01, 'flux("ATPASE")': 0.0366, 'flux("NADPHOX")': 0.0, 'flux("AK_rev")': 0.0, 'flux("GLNS")': 0.01, 'flux("CK")': 0.01, 'flux("ME")': 0.01, 'flux("GLUT")': 0.0, 'flux("TK")': 0.0, 'flux("PGK")': 0.0, 'flux("EP")': 0.0, 'flux("GLNT_rev")': 0.0, 'flux("PDH")': 0.0, 'flux("GLNT")': 0.01, 'flux("SDH")': 0.01, 'flux("RESP")': 0.0133})
Answer: 8
support("LDH") support("AKGDH") support("SCOAS") support("SDH") support("ME") support("GLNT") support("GLNS") support("GLDH") support("RESP") support("ATPASE") support("CK")

LP solver calls: 190   Time cplex :  0.04281210899353027



LP Solver output
(0.0, {'flux("MDH")': 0.0, 'flux("G6PDH")': 0.0, 'flux("CK_rev")': 0.0, 'flux("SCOAS")': 0.01, 'flux("GLDH")': 0.01, 'flux("ALATA")': 0.0, 'flux("LDH_rev")': 0.0, 'flux("LEAK")': 0.0133, 'flux("CITS")': 0.0, 'flux("PGI")': 0.0, 'flux("AKGDH")': 0.01, 'flux("GLNS_rev")': 0.0, 'flux("PK")': 0.0, 'flux("AK")': 0.0, 'flux("PFK")': 0.0, 'flux("CS")': 0.0, 'flux("HK")': 0.0, 'flux("PPRIBP")': 0.0, 'flux("GROWTH")': 0.0, 'flux("LDH")': 0.01, 'flux("ATPASE")': 0.01, 'flux("NADPHOX")': 0.0, 'flux("AK_rev")': 0.0, 'flux("GLNS")': 0.01, 'flux("CK")': 0.01, 'flux("ME")': 0.01, 'flux("GLUT")': 0.0, 'flux("TK")': 0.0, 'flux("PGK")': 0.0, 'flux("EP")': 0.0, 'flux("GLNT_rev")': 0.0, 'flux("PDH")': 0.0, 'flux("GLNT")': 0.01, 'flux("SDH")': 0.01, 'flux("RESP")': 0.0})
Answer: 9
support("LDH") support("AKGDH") support("SCOAS") support("SDH") support("ME") support("GLNT") support("GLNS") support("GLDH") support("LEAK") support("ATPASE") support("CK")

LP solver calls: 192   Time cplex :  0.04317355155944824


lp support ['MDH', 'G6PDH', 'LEAK', 'GLNS_rev', 'AK', 'CS', 'HK', 'PPRIBP', 'GROWTH', 'LDH', 'NADPHOX', 'CK', 'ME', 'PDH', 'GLNT']
lits for clause [18, 9, 28, 23, 30, 13, 2, 34, 36, 7, 35, 32, 19, 12, 20]
lp support ['MDH', 'G6PDH', 'LEAK', 'AK', 'CS', 'HK', 'PPRIBP', 'GROWTH', 'LDH', 'ATPASE', 'NADPHOX', 'CK', 'ME', 'GLUT', 'PDH', 'GLNT']
lits for clause [18, 9, 28, 30, 13, 2, 34, 36, 7, 29, 35, 32, 19, 26, 12, 20]
lp support ['MDH', 'G6PDH', 'SCOAS', 'GLDH', 'LEAK', 'AKGDH', 'AK', 'CS', 'HK', 'PPRIBP', 'GROWTH', 'LDH', 'ATPASE', 'NADPHOX', 'CK', 'ME', 'PDH', 'GLNT', 'SDH']
lits for clause [18, 9, 16, 24, 28, 15, 30, 13, 2, 34, 36, 7, 29, 35, 32, 19, 12, 20, 17]
lp support ['MDH', 'G6PDH', 'SCOAS', 'ALATA', 'LEAK', 'AKGDH', 'AK', 'CS', 'HK', 'PPRIBP', 'GROWTH', 'LDH', 'ATPASE', 'NADPHOX', 'CK', 'ME', 'PDH', 'GLNT', 'SDH']
lits for clause [18, 9, 16, 25, 28, 15, 30, 13, 2, 34, 36, 7, 29, 35, 32, 19, 12, 20, 17]
lp support ['MDH', 'G6PDH', 'GLNS_rev', 'AK', 'CS', 'HK', 'PPRIBP', 'GROWTH', 'LDH', 'ATPASE', 'NADPHOX', 'CK', 'ME', 'PDH', 'GLNT', 'RESP']
lits for clause [18, 9, 23, 30, 13, 2, 34, 36, 7, 29, 35, 32, 19, 12, 20, 27]
lp support ['MDH', 'G6PDH', 'AK', 'CS', 'HK', 'PPRIBP', 'GROWTH', 'LDH', 'ATPASE', 'NADPHOX', 'CK', 'ME', 'GLUT', 'PDH', 'GLNT', 'RESP']
lits for clause [18, 9, 30, 13, 2, 34, 36, 7, 29, 35, 32, 19, 26, 12, 20, 27]
lp support ['MDH', 'G6PDH', 'SCOAS', 'GLDH', 'AKGDH', 'AK', 'CS', 'HK', 'PPRIBP', 'GROWTH', 'LDH', 'ATPASE', 'NADPHOX', 'CK', 'ME', 'PDH', 'GLNT', 'SDH', 'RESP']
lits for clause [18, 9, 16, 24, 15, 30, 13, 2, 34, 36, 7, 29, 35, 32, 19, 12, 20, 17, 27]
lp support ['MDH', 'G6PDH', 'SCOAS', 'ALATA', 'AKGDH', 'AK', 'CS', 'HK', 'PPRIBP', 'GROWTH', 'LDH', 'ATPASE', 'NADPHOX', 'CK', 'ME', 'PDH', 'GLNT', 'SDH', 'RESP']
lits for clause [18, 9, 16, 25, 15, 30, 13, 2, 34, 36, 7, 29, 35, 32, 19, 12, 20, 17, 27]
SATISFIABLE

Models       : 9
Calls        : 1
Time         : 1.925s (Solving: 1.04s 1st Model: 0.37s Unsat: 0.15s)
CPU Time     : 4.145s

Choices      : 627      (Domain: 627)
Conflicts    : 147      (Analyzed: 146)
Restarts     : 2        (Average: 73.00 Last: 13)
Model-Level  : 10.9    
Problems     : 1        (Average Length: 1.00 Splits: 0)
Lemmas       : 146      (Deleted: 0)
  Binary     : 38       (Ratio:  26.03%)
  Ternary    : 21       (Ratio:  14.38%)
  Conflict   : 146      (Average Length:    4.8 Ratio: 100.00%) 
  Loop       : 0        (Average Length:    0.0 Ratio:   0.00%) 
  Other      : 0        (Average Length:    0.0 Ratio:   0.00%) 
Backjumps    : 146      (Average:  3.28 Max:  21 Sum:    479)
  Executed   : 145      (Average:  3.27 Max:  21 Sum:    478 Ratio:  99.79%)
  Bounded    : 1        (Average:  1.00 Max:   1 Sum:      1 Ratio:   0.21%)

Rules        : 372     
  Heuristic  : 35      
Atoms        : 361     
Bodies       : 42       (Original: 41)
Equivalences : 105      (Atom=Atom: 35 Body=Body: 0 Other: 70)
Tight        : Yes
Variables    : 35       (Eliminated:    0 Frozen:   35)
Constraints  : 6        (Binary:  83.3% Ternary:   0.0% Other:  16.7%)

Added nogoods for 13 out of 31 partial solutions checked
Total time used by ThermoDGChecker: 0.16858 s
