# Scripts use for the extension

## equilibrator_recup_data.py

Script used to retrieve the thermodynamic information required for the *DeltaGChecker* extension. For more information, go to [README Exemples and Data Inputs](../data/README.md), section Thermodynamics data

